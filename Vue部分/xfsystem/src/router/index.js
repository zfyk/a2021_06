import Vue from "vue";
import VueRouter from "vue-router";
import routes from './routes';
import store from '@/store';
import { Message, Loading } from 'element-ui';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'hash',
    routes
});

/* 全局前置守卫 */
let loading;
router.beforeEach(async (to, from, next) => {
    // 如果是跳转到登录，则直接跳转，无需做任何处理
    if (to.path === '/login') {
        next();
        return;
    }
    // 如果跳转到其他组件，则需要：校验当前用户是否登录
    let { info } = store.state;
    if (info === null) {
        // 开启Loading层
        loading = Loading.service({
            lock: true,
            text: '小主，奴家正在努力加载中，请稍后...'
        });
        let res = await store.dispatch('queryProfileAsync');
        if (!res) {
            // 没有登录
            Message.error('您当前还未登录，请您先登录!');
            next(`/login?target=${to.path}`);
            return;
        }
        info = res;
    }
    // 权限校验(访问拦截):根据meta获取目标路由的权限代码，再去登陆者拥有的权限中校验是否存在，存在则正常调，不存在直接劝返！！
    let power = (info.power || "").split('|'),
        code = to.meta.code;
    if (code && !power.includes(code)) {
        Message.error('您无权限访问此页面!');
        next(from.fullPath);
        return;
    }
    next();
});

/* 全局后置守卫 */
router.afterEach((to) => {
    // 如果之前开启了Loading层，此时我们把它关掉
    if (loading) loading.close();

    // 在路由跳转完毕后，修改页面的标题
    let { title } = to.meta;
    title = title ? '-' + title : '';
    document.title = `新蜂商城CMS系统${title}`;
});

export default router;