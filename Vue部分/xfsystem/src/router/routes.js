import Login from '@/views/Login.vue';
import Home from '@/views/Home.vue';

const routes = [{
    path: '/',
    redirect: '/home/controlpanel'
}, {
    path: '/login',
    name: 'login',
    component: Login,
    meta: { title: '用户登录' }
}, {
    path: '/home',
    name: 'home',
    component: Home,
    meta: { title: 'CMS首页' },
    // 二级路由
    children: [{
        path: '',
        redirect: '/home/controlpanel'
    }, {
        path: 'controlpanel',
        name: 'controlpanel',
        component: () => import(/* webpackChunkName:"controlpanel" */'@/views/ControlPanel.vue'),
        meta: { title: '控制面板', code: '0x100', icon: 'el-icon-menu' }
    }, {
        path: 'bannersetting',
        name: 'bannersetting',
        component: () => import(/* webpackChunkName:"home" */'@/views/Home/BannerSetting.vue'),
        meta: { title: '轮播图设置', code: '0x101', icon: 'el-icon-picture-outline' }
    }, {
        path: 'goodssetting',
        name: 'goodssetting',
        component: () => import(/* webpackChunkName:"home" */'@/views/Home/GoodsSetting.vue'),
        meta: { title: '热销商品设置', code: '0x101', icon: 'el-icon-wallet' }
    }, {
        path: 'productsetting',
        name: 'productsetting',
        component: () => import(/* webpackChunkName:"home" */'@/views/Home/ProductSetting.vue'),
        meta: { title: '新品上线设置', code: '0x101', icon: 'el-icon-position' }
    }, {
        path: 'recommendsetting',
        name: 'recommendsetting',
        component: () => import(/* webpackChunkName:"home" */'@/views/Home/RecommendSetting.vue'),
        meta: { title: '为你推荐设置', code: '0x101', icon: 'el-icon-thumb' }
    }, {
        path: 'classification',
        name: 'classification',
        component: () => import(/* webpackChunkName:"classification" */'@/views/ClassiFication.vue'),
        meta: { title: '分类管理', code: '0x102', icon: 'el-icon-tickets' }
    }, {
        path: 'goodsmanager',
        name: 'goodsmanager',
        component: () => import(/* webpackChunkName:"goodsmanager" */'@/views/GoodsManager.vue'),
        meta: { title: '商品管理', code: '0x103', icon: 'el-icon-shopping-cart-full' }
    }, {
        path: 'membermanager',
        name: 'membermanager',
        component: () => import(/* webpackChunkName:"membermanager" */'@/views/MemberManager.vue'),
        meta: { title: '会员管理', code: '0x104', icon: 'el-icon-user' }
    }, {
        path: 'ordermanager',
        name: 'ordermanager',
        component: () => import(/* webpackChunkName:"ordermanager" */'@/views/OrderManager.vue'),
        meta: { title: '订单管理', code: '0x105', icon: 'el-icon-s-order' }
    }]
}, {
    path: '*',
    redirect: '/home/controlpanel'
}];
export default routes;