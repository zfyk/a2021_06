/* 汇总各个版块的接口 */
import base from "./base";
import home from "./home";

export default {
    base,
    home
};