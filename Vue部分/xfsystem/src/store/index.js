import Vue from "vue";
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import home from './home';
import classifi from './classifi';
import api from '@/api';

Vue.use(Vuex);
const env = process.env.NODE_ENV;
export default new Vuex.Store({
    // 基础公共状态管理
    state: {
        info: null
    },
    mutations: {
        queryProfile(state, payload) {
            state.info = payload;
        }
    },
    actions: {
        async queryProfileAsync({ commit }) {
            try {
                let { resultCode, data } = await api.base.profile();
                if (+resultCode === 200) {
                    // 模拟给当前登录者设置对应的权限「真实项目中，从服务器获取的结果中就有」
                    data.power = "0x100|0x101|0x102|0x103|0x104|0x105";
                    commit('queryProfile', data);
                    return data;
                }
            } catch (_) { }
            return false;
        }
    },
    // 按模块管理的公共状态
    modules: {
        home,
        classifi
    },
    plugins: env === "production" ? [] : [createLogger()]
});

/*
 权限代码：
   控制面板 0x100
   主页配置 0x101
   分类管理 0x102
   商品管理 0x103
   会员管理 0x104
   订单管理 0x105
 */