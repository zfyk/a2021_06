/*
 对象常用的方法
   Object.prototype -> 实例.xxx()
    + hasOwnProperty 检测是否为私有属性（支持Symbol类型的属性名）
    + isPrototypeOf 检测一个对象是否会出现在其原型链上「prototype.isPrototypeOf(obj)：obj按照原型链查找，是否会找到prototype原型对象」
    + propertyIsEnumerable 检测当前属性是不是对象的可枚举(能被for in等遍历的)属性「obj.propertyIsEnumerable(attr)」
    + toString 检测数据类型，返回“[object ?]”
    + valueOf 获取对象的原始值
     
   Object静态私有属性方法 -> Object.xxx()
    + keys 获取当前对象“非Symbol类型、可枚举的、私有的”属性「结果是包含属性名的数组集合」
    + getOwnPropertySymbols 获取当前对象“Symbol类型、私有的”属性 「ES6」
    + getOwnPropertyNames 获取当前对象“非Symbol类型、私有的”属性

   Reflect ES6新增的一个对象，集成了很多对“对象”进行操作的方法
    + ownKeys 获取所有私有属性「含：各种类型、不管是否可枚举」
 */
let obj = { name: '珠峰培训', age: 13, [Symbol('AA')]: 100, 2: 20, 1: 10 };
Object.defineProperty(obj, 'test', {
  value: '测试',
  enumerable: false
});
Object.defineProperty(obj, Symbol('BB'), {
  value: 200,
  enumerable: false
});
Object.prototype.AA = 200;

/* 
// 真实项目中，我们迭代对象的需求一般都是：只迭代私有、而且不受Symbol和枚举的限定
// 获取所有私有的属性
// let keys = Object.getOwnPropertyNames(obj);
// keys = keys.concat(Object.getOwnPropertySymbols(obj));
let keys = Reflect.ownKeys(obj);
keys.forEach(key => {
  console.log('属性名：', key, '属性值：', obj[key]);
}); 
*/


/*
 数据的循环操作：
   + for循环 & while循环
   + 数组的迭代方法：forEach...
   + for of循环「ES6」
   + for in循环

 for in循环是所有循环中性能最差的{真实项目中不建议使用}
   + 不能迭代“Symbol类型”的属性、有不能迭代“不可枚举”的属性；
   + 但是它还会找对象的原型链，把所有“可枚举的非Symbol类型的”公有属性也拿来迭代；-> 因为其要查找所有私有和公有的属性，把符合要求的拿出来迭代，所以性能消耗比较大的！
   + 优先迭代数字属性

 // 如果只想解决：只查找私有属性这个问题，基于for in循环的时候，要做处理
 for (let key in obj) {
   if (!obj.hasOwnProperty(key)) break;
   console.log(key);
 }
 */