const data = [{
    id: 1,
    count: 2,
    price: 12.5
}, {
    id: 2,
    count: 0,
    price: 10.5
}, {
    id: 3,
    count: 3,
    price: 8.5
}, {
    id: 4,
    count: 0,
    price: 8
}, {
    id: 5,
    count: 0,
    price: 14.5
}];
const computedBox = document.querySelector('#computedBox'),
    topBox = computedBox.querySelector('.top'),
    [totalBox, pricesBox, maxBox] =
        Array.from(computedBox.querySelectorAll('.bottom span'));
let countsBox;

// 根据数据实现数据绑定(渲染视图)
const render = function render() {
    // 基于文档碎片实现批量新增{减少DOM重排/回流}
    let frag = document.createDocumentFragment();
    data.forEach(({ count, price }, index) => {
        let div = document.createElement('div');
        div.className = 'hang';
        div.index = index;
        div.innerHTML = `
            <i class="minus"></i>
            <span class="pronum">${count}</span>
            <i class="add"></i>
            <span class="info">
                单价：${price}元 &nbsp;&nbsp;
                小计：<em>${(count * price).toFixed(2)}</em>元
            </span>
        `;
        frag.appendChild(div);
    });
    topBox.appendChild(frag);
    frag = null;
    // 统计
    countsBox = Array.from(topBox.querySelectorAll('.pronum'));
    statis();
};

// 基于事件委托实现“+/-”的操作
const delegate = function delegate() {
    topBox.addEventListener('click', function (ev) {
        let target = ev.target,
            targetTag = target.tagName,
            targetClass = target.className,
            parent = target.parentNode;
        if (targetTag === 'I') {
            // 点击的是按钮:获取数量/小计盒子、获取商品单价
            let countBox = parent.querySelector('span.pronum'),
                subBox = parent.querySelector('em'),
                price = data[parent.index].price;
            if (targetClass === "minus") {
                countBox.innerHTML--;
                if (+countBox.innerHTML <= 0) countBox.innerHTML = 0;
            } else {
                countBox.innerHTML++;
            }
            subBox.innerHTML = (countBox.innerHTML * price).toFixed(2);
            statis();
        }
    });
};

// 实现统计
const statis = function statis() {
    let total = 0,
        prices = 0,
        max = [0];
    countsBox.forEach((countBox, index) => {
        let count = +countBox.innerHTML,
            price = data[index].price,
            sub = count * price;
        total += count;
        prices += sub;
        if (count > 0) max.push(price);
    });
    totalBox.innerHTML = total;
    pricesBox.innerHTML = prices;
    maxBox.innerHTML = Math.max(...max);
};

delegate();
render();