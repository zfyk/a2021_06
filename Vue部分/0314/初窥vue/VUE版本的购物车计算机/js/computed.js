/*
 Vue是前端框架「尤雨溪团队开发」：基于MVVM思想设计的；基于Vue框架开发项目，总体来讲是告别直接操作DOM，转向对数据的操作！
 */
const vm = new Vue({
    // 指定页面中的哪一个容器作为我们的视图
    el: '#computedBox',
    // 准备数据(Model层)
    data: {
        list: [{
            id: 1,
            count: 2,
            price: 12.5
        }, {
            id: 2,
            count: 0,
            price: 10.5
        }, {
            id: 3,
            count: 3,
            price: 8.5
        }, {
            id: 4,
            count: 0,
            price: 8
        }, {
            id: 5,
            count: 0,
            price: 14.5
        }],
        msg: '珠峰培训'
    },
    // 方法
    methods: {
        handle(type, index) {
            let item = this.list[index];
            if (type === 'add') {
                item.count++;
                return;
            }
            item.count--;
            if (item.count <= 0) item.count = 0;
        }
    },
    // 计算属性「依赖于其它状态的改变，自动计算出最新的值」
    computed: {
        sub() {
            let total = 0,
                prices = 0,
                max = [0];
            this.list.forEach(item => {
                total += item.count;
                prices += item.count * item.price;
                if (item.count > 0) max.push(item.price);
            });
            return {
                total,
                prices,
                max: Math.max(...max)
            };
        }
    }
});