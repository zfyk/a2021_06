/* 
 ES6的语法是不兼容IE浏览器，如果需要兼容：
 1. 可以基于 babel-preset 语法包，把ES6语法转换为ES5
 https://babeljs.io/
 2. 但是上述只能完成常规语法的转换，实现不了新增的那些内置API的转换，此时我们需要一个插件：@babel/polyfill「插件内部把常用的ES6+内置API都自己基于ES5的语法实现了一遍」
 3. 对于ES6中的一些特殊语法「例如：类的装饰器等」，还需要基于babel的一些插件才能进行转换！
 ============

 数组常用的方法
   Array.prototype 供其实例调用 -> arr.xxx()
     + push
     + pop
     + shift
     + unshift
     + splice
     + slice
     + concat
     + indexOf/lastIndexOf
     + includes 「ES7」
     + toString
     + join
     + sort 
     + reverse
     ----数组的迭代方法「都会依次迭代数组每一项(触发回调函数执行,传递item/index)」
     + forEach
     + map
     + filter 筛选
     + find 查找 「ES6」
     + findIndex 查找符合条件项的索引 「ES6」
     + reduce/reduceRight 可实现结果累计
     + some 数组有一项符合条件即可
     + every 数组所有项都需要符合条件
     ----其他方法
     + fill 数组填充「ES6」
     + flat 数组扁平化「ES10」 -> 掌握如何自己实现数组扁平化（经典算法）？
     + toLocaleString 在把数组转换为字符串的过程中，可以把日期格式的值，转换为想要的字符串格式
     ----不常用的
     + copyWithin
     + keys/values/entries

   Array 静态私有属性方法 -> Array.xxx()
     + from 把类数组(或可迭代的数据)转换为数组 「ES6」
     + isArray 检测是否为数组
     + of 创建一个新数组 「ES6」
     + ...
 */

/* 
const array1 = [1, 'a', new Date('21 Dec 1997 14:12:00 UTC')];
console.log(array1.toString()); //"1,a,Sun Dec 21 1997 22:12:00 GMT+0800 (中国标准时间)"
console.log(array1.toLocaleString('zh-cn', { hour12: false }));//"1,a,1997/12/21 22:12:00" */

/* 
let ary = new Array(10); //数组只有长度，但是每一项没有任何的值 “稀疏数组”「这样的数组无法使用数组的迭代方法进行遍历循环」
ary = ary.fill(null); //基于fill向数组中每一项进行填充，把“稀疏数组”变为“密集数组”
ary.forEach(_ => {
    console.log('OK');
}); 
----
// 循环10次
new Array(10).fill(null).forEach(_ => { 
    
});
*/

/* 
let arr = [
    0,
    [
        1,
        2,
        [3, 4],
        5,
        [
            6,
            7,
            [8, 9, [100, 200, [300, 400, [500, 600]]]]
        ]
    ],
    10
];
// arr = arr.flat(1); //降了一维
arr = arr.flat(Infinity); //直接降为一维
console.log(arr); 
*/


let data = [{
    id: 1,
    count: 2,
    price: 12.5
}, {
    id: 2,
    count: 0,
    price: 10.5
}, {
    id: 3,
    count: 3,
    price: 8.5
}, {
    id: 4,
    count: 0,
    price: 8
}, {
    id: 5,
    count: 0,
    price: 14.5
}];

/* 
// 依次迭代数组每一项，但是可以把上一次迭代的结果传递给下一次，从而实现结果的累积
// reduce：从左到右迭代  reduceRight：从右到左迭代
// arr.reduce((res, item, index) => { })：res初始值是数组第一项，从数组第二项依次向后迭代
// arr.reduce((res, item, index) => { },0)：res初始值是0，从数组第一项开始迭代
let result = data.reduce((res, item, index) => {
    return res + item.count;
}, 0);
console.log(result); 
*/

/* 
// some & every 返回的结果是true/false
// some:只要有一项符合条件，结果就是true
// every:需要所有项都符合条件，结果才是true
let result = data.every(item => item.price > 0);
console.log(result); 
*/

/* 
// find/findIndex：迭代数组每一项，找到符合条件这一项就停止迭代，把找到的这一项(或索引)返回；如果没找到，结果是undefined(或-1)；
let result = data.find(item => item.price > 10);
console.log(result); 
*/

/* 
// filter：迭代数组每一项，把符合条件「RETURN后设定的」的筛选出来，以新数组返回；如果一项都没筛选到，则返回空数组；原始数组不变！
let result = data.filter((item, index) => {
    return item.count > 0;
});
console.log(result); 
*/

