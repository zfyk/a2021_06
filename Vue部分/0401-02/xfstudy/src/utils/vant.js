import Vue from 'vue';

import {SubmitBar,Stepper,Checkbox, CheckboxGroup,Toast,GoodsAction, GoodsActionIcon, GoodsActionButton,Notify,Button,Tabbar, TabbarItem,NavBar,Icon,Form,Field, Swipe, SwipeItem, Grid, GridItem } from 'vant';

[SubmitBar,Stepper,Checkbox, CheckboxGroup,Toast,GoodsAction, GoodsActionIcon, GoodsActionButton,Notify,Button,Tabbar, TabbarItem,NavBar,Icon,Form,Field, Swipe, SwipeItem, Grid, GridItem ].forEach(item=>{
	Vue.use(item);
})
