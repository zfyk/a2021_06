import axios from "./http.js";
import md5 from 'js-md5';

export function test() {
  return axios.post('/shop-cart')
}

//注册功能
export function register(name,psw) {
  return axios.post('/user/register',{
	  loginName: name,
	  password: psw
  })
}

//登录功能
export function login(name,psw) {
  return axios.post('/user/login',{
	 loginName: name,
	 passwordMd5: md5(psw)
  })
}

//首页信息接口
export function getIndexInfo() {
  return axios.get('/index-infos')
}

//详情页面接口
export function getInfoData(id) {
  return axios.get('/goods/detail/' + id)
}

//添加购物车
export function addCart(options) {
  // options {goodsId,goodsCount}
  return axios.post('/shop-cart', options)
}

//获取添加到购物车的具体内容
export function getCartList() {
  return axios.get('/shop-cart')
}