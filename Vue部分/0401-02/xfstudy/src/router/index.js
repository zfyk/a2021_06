import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
	meta:{//路由元信息
	   isshow:true
	},
    component: () => import('../views/Home.vue')
  },
  {
    path: '/list',
    name: 'List',
	meta:{
	   isshow:true
	},
    component: () => import('../views/List.vue')
  },
  {
    path: '/cart',
    name: 'Cart',
	meta:{
	   isshow:true
	},
    component: () => import('../views/Cart.vue')
  },
  {
    path: '/my',
    name: 'My',
	meta:{
	   isshow:true
	},
    component: () => import('../views/My.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/detials/:id',
    name: 'Detials',
    component: () => import('../views/Detials.vue')
  },
  {
    path: '*',
    redirect:"/home"
  }
]

const router = new VueRouter({
  routes
})

export default router
