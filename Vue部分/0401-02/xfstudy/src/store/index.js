import Vue from 'vue'
import Vuex from 'vuex'
import {getCartList} from "../api/index.js"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
	  list:[]
  },
  getters:{
	  listnum:(state)=>{
		return state.list.reduce((result,item)=>{
			return result+item.goodsCount
		},0)
	  }
  },
  mutations: {
	  addCart(state,data){
		  //通过观察数据，少了一项，checked这个数据，补上
		  data=data.map(item=>{
			  item.checked=true;
			  return item;
		  })
		  state.list=data;
	  }
  },
  actions: {
	 asyncaddCart(context){
		//console.log(id);
		getCartList().then(data=>{
			console.log(data);
			if(+data.resultCode==200){
				context.commit("addCart",data.data)
			}
		})
	 }
  },
  modules: {
  }
})
