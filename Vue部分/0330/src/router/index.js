import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'hash',
    routes
});

/* 
全局前置守卫:每一次路由跳转，全局前置守卫一定会被执行
  to:跳转目标的路由信息 
  from:从哪来的路由信息 
  next:方法只有执行才能进入下一个环节；如果不执行，则本次路由跳转结束！ 
    + next('/login') 直接跳转到login这个路由组件中，和to就没关系了
真实项目中最常用的守卫函数：
  + 每一次路由跳转，我们可以根据meta中的信息，修改页面的title等
  + 登录或者权限校验
  + ...
*/
router.beforeEach((to, from, next) => {
    console.log('全局前置守卫：beforeEach');
    let { title } = to.meta;
    document.title = title;
    next();
});

router.beforeResolve((to, from, next) => {
    console.log('全局解析守卫：beforeResolve');
    next();
});

/*
 使用场景举例:给每一次路由跳转设置Loading层（或过渡效果）
   beforeEach中开启Loading
   afterEach中关闭Loading
 */
router.afterEach((to, from) => {
    console.log('全局后置守卫：afterEach');
});

export default router;