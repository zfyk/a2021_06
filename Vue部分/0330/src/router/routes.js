import Home from '@/views/Home.vue';

/*
 如果事先把所有组件导入进来，基于路由表进行规则匹配，匹配到谁就基于component渲染谁...这样在最后项目打包的时候，所有组件内容都打包到了一个js中「问题：打包后的js超级大」
   + 打包部署到服务器，当客户端进行访问，光加载这个js就需要N多时间，这样造成白屏等待时间过长！！
   + 我们期许：所有的组件按照大的功能板块（或者文件大小），打包的时候切割为多个js打包；访问项目的时候，最开始只把默认要展示的组件(例如:首页)对应的js加载出来！当跳转到指定路由，再把之前切割的js中的某一个单独加载！！
     例如：我们代码打包切割的时候，首页打包到主js中，这样页面第一次渲染，只渲染首页的内容，其余版块的js还没有加载，加快首次渲染时间！！把购物车和个人中心等单独切割为cart.js / person.js ... 当跳转到指定路由，再加载对应的js！！
   =====> 这套切割打包，按需加载的机制就是：“路由懒加载”
 */

const routes = [{
    //命名路由，就是给路由设置名字，目的：基于路由名字进行跳转
    name: 'home',
    path: '/',
    component: Home,
    //设置路由元信息：自己给每个路由设置一些基本信息，这些信息可以供以后使用
    meta: { title: "首页" }
}, {
    name: 'cart',
    path: '/cart',
    component: () => import(/* webpackChunkName: "cart" */'@/views/Cart.vue'),
    meta: { title: "购物车" },
    beforeEnter(to, from, next) {
        console.log('购物车路由独享守卫:beforeEnter「跳转到的路由地址和当前路由表匹配后，触发此路由独享守卫」');
        next();
    }
}, {
    name: 'person',
    path: '/person',
    component: () => import(/* webpackChunkName: "person" */'@/views/Person.vue'),
    meta: { title: "个人中心" },
    // 指定二级路由
    children: [{
        path: '',
        redirect: '/person/info'
    }, {
        name: 'person-info',
        path: 'info', // path="/person/info"
        component: () => import(/* webpackChunkName: "person" */'@/views/person/PersonInfo.vue'),
        meta: { title: '个人中心-我的信息' }
    }, {
        name: 'person-order',
        path: 'order',
        component: () => import(/* webpackChunkName: "person" */'@/views/person/PersonOrder.vue'),
        meta: { title: '个人中心-我的订单' }
    }, {
        name: 'person-address',
        path: 'address',
        component: () => import(/* webpackChunkName: "person" */'@/views/person/PersonAddress.vue'),
        meta: { title: '个人中心-我的地址' }
    }]
}, {
    name: '404',
    path: '/404',
    component: () => import(/* webpackChunkName: "other" */'@/views/Page404.vue'),
    meta: { title: "错误页面" }
}, {
    name: 'detail',
    // /detail 不匹配
    // /detail/100 匹配 ：id=100
    path: '/detail/:id',
    component: () => import(/* webpackChunkName: "detail" */'@/views/Detail.vue'),
    meta: { title: '商品详情' }
}, {
    path: '*',
    redirect: '/404'
}];
export default routes;