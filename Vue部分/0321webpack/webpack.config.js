/*
 Webpack本身是基于NodeJS开发和打包的，所以需要基于CommonJS规范来管理配置项 
   导入：require
   导出：module.exports
 */
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = {
    /* 基本配置 */
    // 设置打包模式:开发环境development 生产环境production
    mode: 'production',
    // 指定打包的入口
    entry: './src/main.js',
    // 指定打包的出口
    output: {
        filename: 'bundle.[hash].js',
        path: path.resolve(__dirname, './dist')
    },
    /* webpack插件 */
    plugins: [
        // 清空之前打包内容
        new CleanWebpackPlugin(),
        // 实现页面编译和导入打包的JS等
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './public/index.html'
        }),
        // 抽离CSS
        new MiniCssExtractPlugin({
            filename: 'bundle.[hash].css'
        })
    ],
    /* webpack加载器 */
    module: {
        // 加载器处理的规则顺序：从右到左，从下到上
        rules: [{
            test: /\.(css|less)$/i,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'postcss-loader',
                'less-loader'
            ]
        }, {
            test: /\.js$/i,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        "@babel/preset-env"
                    ]
                }
            }],
            include: path.resolve(__dirname, 'src'),
            exclude: /node_modules/
        }, {
            test: /\.(png|jpe?g|gif)$/i,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 40 * 1024,
                    outputPath: '/images'
                }
            }]
        }, {
            test: /\.(svg|eot|ttf|woff|woff2)$/i,
            use: "file-loader"
        }]
    },
    /* 优化项 */
    optimization: {
        minimizer: [
            new CssMinimizerWebpackPlugin(),
            new TerserWebpackPlugin()
        ]
    },
    /* DEV SERVER：本地启动web服务、打开浏览器、预览编译的结果、实现热更新、支持Proxy跨域代理等操作 */
    devServer: {
        host: '127.0.0.1',
        port: 8080,
        compress: true,
        open: true,
        hot: true
    }
};