const path=require("path");//path模块 是nodejs本身自带的模块

// production:生产环境（压缩）   development:开发环境（非压缩）
// module.exports={
// 	mode:"development",//模式
// 	entry:"./src/one.js",//入口
// 	output:{//出口
// 		filename:"main.[hash].js",//文件名称
// 		//获取根目录的绝对路径path.resolve(__dirname)
// 		path:path.resolve(__dirname,"dist")//出口的文件路径
// 	}
// }

//获取环境变量
let NODE_ENV= process.env.NODE_ENV || "development";

module.exports={
	mode:NODE_ENV,//模式
	entry:"./src/one.js",//入口
	output:{//出口
		filename:NODE_ENV==="development"?"devmain.[hash].js":"promain.[hash].min.js",//文件名称
		//获取根目录的绝对路径path.resolve(__dirname)
		path:path.resolve(__dirname,"dist")//出口的文件路径
	}
}