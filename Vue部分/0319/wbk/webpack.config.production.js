//生产环境
const path=require("path");//path模块 是nodejs本身自带的模块

// production:生产环境（压缩）   development:开发环境（非压缩）
module.exports={
	mode:"production",//模式
	entry:"./src/one.js",//入口
	output:{//出口
		filename:"promain.[hash].js",//文件名称
		//获取根目录的绝对路径path.resolve(__dirname)
		path:path.resolve(__dirname,"dist")//出口的文件路径
	}
}