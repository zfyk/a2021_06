const sum = function sum(...params){
	return params.reduce((result,item)=>{
		return result+item;
	})
}

const num=10;

//1. export default 只能导出一次----AAAA
//export default sum;

//2. 导出多个内容---》导出对象---BBBB
export default {
	sum,num
};

//导出底层
// {
// 	default:{
// 		sum,num
// 	}
// }
