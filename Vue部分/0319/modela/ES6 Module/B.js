import objdefault from "./A.js";
let {sum,num}=objdefault;

// 1. export可以导出多次
export function average(...params){
	params.sort((a,b)=>a-b);
	params.pop();
	params.shift();
	
	let total=sum(...params);
	return total/params.length;
}

export let msg="hello";
//底层
// {
// 	average:f,
// 	msg:"hello"
// }

//普通对象，必须用 export default
let obj={
	name:"lili",
	age:18
}
export default obj;