// AAAA
// import sum from "./A.js";
// console.log(sum(1,2,3,4,5));

// 3. 不能直接解构，会出错
// import {sum,num} from "./A.js";
// console.log(sum(1,2,3,4,5));
// console.log(num);

// BBBB
// import objdefault from "./A.js";
// let {sum,num}=objdefault;
// console.log(sum(1,2,3,4,5));
// console.log(num);

//1. 接收导出多个值----方法1
// import * as B from "./B.js";
// console.log(B.average(1,2,3,4,5));

//2. 接收导出多个值----方法1
import {average,msg} from "./B.js";
console.log(average(1,2,3,4,5));

import obj from "./B.js";
console.log(obj);
