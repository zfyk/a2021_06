define(function(){
	return{ 
		sum(...params){
			return params.reduce((result,item)=>{
				return result+item
			})
		}
	}
})