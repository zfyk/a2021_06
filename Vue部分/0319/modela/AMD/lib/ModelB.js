define(["ModelA"],function(ModelA){
	return{ 
		average(...params){
			params.sort((a,b)=>a-b);
			params.pop();
			params.shift();
			let total=ModelA.sum(...params);
			return total/params.length;
		}
	}
})