let sum=require("./A")

const average=function average(...params){
	params.sort((a,b)=>a-b);
	params.pop();
	params.shift();
	
	let total=sum(...params);
	
	return total/params.length;
}

let num=100;

module.exports={
	average,
	num
}