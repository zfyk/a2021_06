//nodejs----> 运行 命令提示符  node XXX.js
//解决：前置依赖， 实现了 按需导入

// 导入 require
let sum=require("./A");//可以省略.js,自定义的模块，必须加./
console.log(sum(1,2,3,4,5));

let {average,num}=require("./B");
console.log(average(1,2,3,4,5));

console.log(num);
//2000
