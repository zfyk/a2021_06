//求平均数 还用到了 modelA中的sum方法
const ModelB=(function(){
	const average=function(...params){
		params.sort((a,b)=>{return a-b});
		params.pop();
		params.shift();
		
		let total=ModelA.sum(...params);
		return total/params.length;
	}
	return {average}
})()