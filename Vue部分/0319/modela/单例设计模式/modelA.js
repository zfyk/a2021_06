//求和方法
const ModelA=(function(){
	const sum=function sum(...params){
		return params.reduce((result,item)=>{
			return result+item;
		})
	}
	return { sum }
})()