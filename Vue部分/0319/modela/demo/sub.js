//求和算法
const ModelA=(function(){
	const sum=function(...params){
		return params.reduce((result,item)=>{
			return result+item
		})
	}
	
	return {
		sum
	}
})()

