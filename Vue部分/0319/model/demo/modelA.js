let modelA=(function(){
	function sum(...params){
		return params.reduce((result,item)=>{
			return result+item;
		},0)
	}
	return {sum}
})()