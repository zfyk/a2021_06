import sum from "./A.js";
console.log(sum(10,20,30,40));

import B from "./B.js";
//B---->default: {
// 		average,
// 		num
// 	}

// let {average,num}=B;
// console.log(average(10,9,9,9,8,7,8));

console.log(B.num);
console.log(B.average(10,9,9,9,8,7,8));


//不能解构
// import {average,num} from "./B.js";
// console.log(num);

import * as C from "./C.js";
console.log(C.str);

//直接解构
import {apple,str,obj} from "./C.js";
console.log(apple);