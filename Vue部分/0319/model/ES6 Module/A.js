// 导出一次  导出单个值
// export default function sum(...params){
// 	return params.reduce((result,item)=>{
// 		return result + item;
// 	})
// }

const sum=function sum(...params){
	return params.reduce((result,item)=>{
		return result + item;
	})
}

export default sum;