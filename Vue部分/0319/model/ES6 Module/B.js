import sum from "./A.js";
//导出一次 导出对象
const average=function average(...params){
		params.sort((a,b)=>{
			return a-b;
		})
		params.pop();
		params.shift();
		
		let total=sum(...params);
		return (total/params.length).toFixed(2);
}

let num=10;

export default {
	average,
	num
}
// {
// 	default: {
// 		average,
// 		num
// 	}
// }