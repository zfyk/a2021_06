let ModelB=(function(){
	const average=function average(...params){
		params.sort((a,b)=>{return a-b});
		params.pop();
		params.shift();
		
		let total=ModelA.sum(...params);
		return (total/params.length).toFixed(2)
	}
	return {average}
})()