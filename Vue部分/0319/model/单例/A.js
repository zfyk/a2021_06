let ModelA=(function(){
	function sum(...params){
		return params.reduce((result,item)=>{
			return result+item;
		})
	}
	
	return {
		sum
	}
})()