let sum=require("./A.js");

const average=function average(...params){
		params.sort((a,b)=>{
			return a-b;
		})
		params.pop();
		params.shift();
		
		let total=sum(...params);
		return (total/params.length).toFixed(2);
}

//导出 对象
module.exports={
	average
}