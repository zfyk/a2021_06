import Home from '@/views/Home.vue';
import Cart from '@/views/Cart.vue';
import Person from '@/views/Person.vue';
import Page404 from '@/views/Page404.vue';

const routes = [{
    path: '/',
    component: Home
}, {
    path: '/cart',
    component: Cart
}, {
    path: '/person',
    component: Person
}, {
    path: '/404',
    component: Page404
}, {
    // 以上规则都不匹配：可以渲染404页面 或者 重定向到首页
    path: '*',
    redirect: '/404'
}];
export default routes;