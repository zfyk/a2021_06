import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;
new Vue({
  /*
   在每个组件中，可以基于 this.$router/$route 实现路由的管理
      $router：提供一系列的方法实现路由的跳转和传值
      $route：存储当前路由匹配到的相关信息
   */
  router,
  render: h => h(App),
}).$mount('#app');