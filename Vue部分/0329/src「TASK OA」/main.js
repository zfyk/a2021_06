import Vue from 'vue';
import App from './App.vue';
import './element';
import api from './api';
import store from './store';

Vue.prototype.$api = api;
Vue.config.productionTip = false;
new Vue({
  store, //在每个组件中可以基于 this.$store 获取容器「就可以使用disptach/commit/state...」
  render: h => h(App),
}).$mount('#app');