/* 按需导入ElementUI中需要使用的组件 */
import Vue from 'vue';
import { Button, Link, Input, DatePicker, Form, FormItem, Table, TableColumn, Tag, Loading, Message, Dialog, Popconfirm } from 'element-ui';

Vue.prototype.$message = Message;
Vue.use(Button)
    .use(Link)
    .use(Input)
    .use(DatePicker)
    .use(Form)
    .use(FormItem)
    .use(Table)
    .use(TableColumn)
    .use(Tag)
    .use(Loading.directive)
    .use(Dialog)
    .use(Popconfirm);