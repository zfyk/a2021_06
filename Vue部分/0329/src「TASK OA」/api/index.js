import axios from "./http";

// 获取指定状态的任务
const getTaskList = (state = 0) => {
    return axios.get('/api/getTaskList', {
        params: {
            state
        }
    });
};

// 删除指定的任务
const removeTask = id => {
    return axios.get('/api/removeTask', {
        params: {
            id
        }
    });
};

// 把指定的任务设置为完成
const completeTask = id => {
    return axios.get('/api/completeTask', {
        params: {
            id
        }
    });
};

// 新增任务
const addTask = data => {
    return axios.post('/api/addTask', data);
};

export default {
    getTaskList,
    removeTask,
    completeTask,
    addTask
};