import Vue from "vue";
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import api from '@/api';

Vue.use(Vuex);
const env = process.env.NODE_ENV,
    plugins = [];
if (env === 'development') {
    plugins.push(createLogger());
}

const store = new Vuex.Store({
    state: {
        taskList: null
    },
    mutations: {
        // 从服务器获取全部的任务信息，同步给taskList「payload从服务器获取的结果」
        queryAllTask(state, payload) {
            state.taskList = payload;
        },
        // 删除指定ID的任务
        removeTask(state, id) {
            if (!state.taskList) return;
            state.taskList = state.taskList.filter(item => {
                return +item.id !== +id;
            });
        },
        // 修改指定ID的任务
        updateTask(state, id) {
            if (!state.taskList) return;
            state.taskList = state.taskList.map(item => {
                if (+item.id === +id) {
                    item.state = 2;
                    item.complete = new Date().toLocaleString('zh-CN', { hour12: false });
                }
                return item;
            });
        }
    },
    actions: {
        // 从服务器获取全部的任务信息，同步给taskList
        async asyncQueryAllTask({ commit }) {
            let { code, list } = await api.getTaskList();
            if (+code !== 0) list = [];
            commit('queryAllTask', list);
        }
    },
    plugins
});
export default store;