import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

//创建一个全局的Eventbus
let Eventbus=new Vue();
//把创建的全局的Eventbus 挂在到 Vue.prototype
Vue.prototype.$bus=Eventbus;

new Vue({
  router,
  store,
  data(){
	 return {
		 msg:"我是根"
	 } 
  },
  render: h => h(App)
}).$mount('#app')

