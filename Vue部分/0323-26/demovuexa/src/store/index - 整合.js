import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger';


Vue.use(Vuex)

const moldeA={
	namespaced: true,
	state: {
	   anum:1314
	}
}

export default new Vuex.Store({
  state: {
	 num:10,
	 n:100,
	 arr:[10,20,30,40,450]
  },
  getters:{
	  donearr:(state)=>{
		  return state.arr.filter(item=>item>=30);
	  }
  },
  mutations: {
	 changeNum(state,n){
		 state.num+=n;
	 },
	 changeN(state,m){
	 	state.n+=m;
	 }
  },
  actions: {
	 asyncChangeN(context,val){
		 setTimeout(()=>{
			 context.commit("changeN",val)
		 },1000)
	 }
  },
  modules: {
	  moldeA:moldeA
  },
  plugins: process.env.NODE_ENV === "development" ? [createLogger()] : []
})
