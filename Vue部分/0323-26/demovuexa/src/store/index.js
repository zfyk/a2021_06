import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import createLogger from 'vuex/dist/logger';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
	  list:null
  },
  getters:{
  },
  mutations: {
	getlist(state,data){
		state.list=data;
	}
  },
  actions: {
	 asyncgetData(context){
		 axios.get("http://localhost:8888/api/swiper").then((data)=>{
			 context.commit("getlist",data);
		 })
	 }
  },
  modules: {
	
  },
  plugins: process.env.NODE_ENV === "development" ? [createLogger()] : []
})
