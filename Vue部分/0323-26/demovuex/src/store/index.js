import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store=new Vuex.Store({
	state:{
		supNum:5,
		oppNum:5
	},
	getters:{
		result:(state)=>{
			let total=state.supNum+state.oppNum;
			if(total<=0){
				return "0.00%"
			}
			let res=(state.supNum/total*100).toFixed(2)+"%"
			return res;
		}
	},
	mutations:{
		changeNum(state,type){
			if(type=="sup"){
				state.supNum++;
			}else if(type=="opp"){
				state.oppNum++;
			}
		}
	}
})

export default store;
