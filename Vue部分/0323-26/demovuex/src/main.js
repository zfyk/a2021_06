import Vue from 'vue'
import App from './App.vue'
import router from './router'

//导入 store 全写
//import store from './store/index.js'
//简写
import store from './store'


Vue.config.productionTip = false

let vm=new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

console.log(vm);
