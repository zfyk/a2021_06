import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store=new Vuex.Store({
	state:{
		num:10,
		n:100,
		msg:"hello",
		flag:true,
		arr:[10,20,30,40,50]
	},
	getters:{
		donearr:(state)=>{
			console.log(state);
			return state.arr.filter((item)=>item>=30)
		}
	},
	mutations:{//同步
		addnum(state,payload){
			//console.log(payload);
			//state.num++;
			state.num+=payload.n;
		},
		adda(state){
			state.n++;
		}
	}
})

export default store;
