import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import ModelA from "./modela.js";
import ModelB from "./modelb.js";


const store=new Vuex.Store({
	state:{
		n:100,
		arr:[10,20,30,40,50]
	},
	getters:{
		donearr:(state)=>{
			console.log(state);
			return state.arr.filter((item)=>item>=30)
		}
	},
	mutations:{//同步
		adda(state){
			state.n++;
			//函数调用了，数值并没有立刻同步过来
			// setTimeout(()=>{
			// 	state.n++;
			// },3000)
		}
	},
	actions:{//异步
		changeN(context){
			setTimeout(()=>{
				context.commit("adda")
			},3000)
		}
	},
	modules:{
		a:ModelA,
		b:ModelB
	}
})

export default store;
