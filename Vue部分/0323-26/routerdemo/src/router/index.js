import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from '../views/Home.vue'
import List from '../views/List.vue'
import Cart from '../views/Cart.vue'


//路由的配置信息
const routes=[
	{path:"/",redirect:"/home"},//默认打开/----->/home
	{path:"*",redirect:"/my"},//默认打开没有的页面-----》/my
	{path:"/home",name:"Home",component:Home},
	{path:"/list",component:List},
	{path:"/cart",component:Cart,alias:"/ct"},//起别名
	{path:"/my/:num",component:() => import('../views/My.vue')}
];

//创建一个路由
const router=new VueRouter({
	routes,//路由的配置信息
	linkActiveClass:"myactive",//路由高亮名称
	mode: 'history'
})

export default router;