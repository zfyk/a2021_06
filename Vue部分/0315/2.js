/* 
 版本：Vue2.6.14 「options api」
 
 Vue是一个自定义构造函数，我们使用vue开发就要创造它的一个实例
   const vm = new Vue([options]);
   + el(element缩写)：指定视图「注意不要把html和body作为视图，而是指定某个容器」
     + 也可以基于 vm.$mount('#app') 指定视图
     + 还可以基于template构建组件视图
     + ...
   + 构建数据是基于 data 配置项来处理的（值是对象，包含需要的所有数据）
     + 所有设定的数据(键值对)都会挂载到实例上 vm.msg/num
     + 而且对这些数据进行了GET/SET劫持「Object.defineProperty」：目的是当后期更改数据的时候，vue内部可以监测到，并通知视图重新渲染；我们把这样做了数据劫持的数据，称之为“响应式数据/状态”；
     + 默认会进行深度的数据劫持：只要是DATA中出现的，不论哪一级别，对象的属性都会做GET/SET劫持
     + 默认只会对DATA中“最开始就写好的”数据做劫持，后续加入进来的新内容没有做劫持：所以基于vue开发的时候，我们都会把需要数据提前写在data中(哪怕赋值的是null/undefined这样的初始值)，目的是让其做劫持！
     ----
     思考点：如果最开始没有写在DATA中，后续是否有办法，能够给新增的数据做劫持并且也可以通知视图渲染？
       vm.$set(对象,属性,属性值)  例如：
       vm.$set(vm.obj, 'age', 13)
       对象不能是vm实例本身！！
     ----
     思考点：如果是非响应式的数据更改，默认视图是不渲染的，有没有办法在不进行数据劫持的情况下，也可以控制视图强制更新渲染？
       vm.$forceUpdate()
     ----
     知识点：如果data中的某个数据是数组类型，和对象的处理不一样
       + 数组本身会做GET/SET劫持 ： vm.arr=xxx 视图更新
       + 数组中的每一个索引项不会进行劫持处理 ：vm.arr[0]=xxx 视图不更新
       + 但是，会让数组指向一个新的原型对象「数组.__proto__===新原型对象；新原型对象.__proto__===Array.prototype」；新的原型对象上提供了7个方法「push\pop\shift\unshift\splice\sort\reverse」，基于这7个方法操作数组某一项，可以通知视图的重新渲染！！
       如何实现修改数组某一项，也可以让视图更新呢？
         + 改完后直接 vm.$forceUpdate() 强制更新
         + vm.$set(vm.arr,0,100) 这样虽然还是没有把0做劫持，但是也会通知视图更新
         + vm.arr = [100, ...vm.arr.slice(1)] 利用arr做了劫持，更改其是可以让视图更新的
         + vm.arr.splice(0, 1, 100) 官方方案

 总结：Vue2内部会“在new Vue的时候”，基于“Object.defineProperty”对data中的数据进行“深度的监听劫持”「特殊：数组是基于重写原型对象上的7个方法来实现劫持」，这样就可以实现：更新数据，自动通知视图重新渲染，实现出数据驱动的思想，我们把这样的数据称之为“响应式数据/状态”！
   + 基于Vue.prototype.$set方法，可以为某个对象(排除vm)新增响应式的属性「会做数据劫持、也会通知视图渲染」
   + 基于Vue.prototype.$forceUpdate方法，可以实现强制通知视图渲染
   + 如果对象是被冻结的(Object.freeze)，则不在进行数据监听劫持！
   + 重写的数组7个方法为：push/pop/shift/unshift/splice/sort/reverse
 */
const vm = new Vue({
    data: {
        msg: 'hello world',
        num: 10,
        obj: {
            name: '珠峰'
        },
        arr: [10, 20, 30, { x: 100 }]
    }
});
vm.test = 100;
vm.$mount('#app');
console.log(vm);
setTimeout(() => {
    // vm.arr = [100, 200]; //可以重新渲染「因为arr本身做了GET/SET劫持」
    // vm.arr[0] = 100; //视图不会重新渲染，因为arr数组中的每一个索引项，并不会做数据劫持
    // vm.$set(vm.arr, 0, 100);
    // vm.arr = [100, ...vm.arr.slice(1)];
    // vm.arr.splice(0, 1, 100);
}, 1000);


/* 对数据进行深度的监听和劫持 */
// 渲染视图
const compile = function compile() {
    console.log('视图渲染');
};

// 数组重新指向的原型对象
const prototype = {};
Object.defineProperty(prototype, 'push', {
    value: function push(val) {
        // 调用数组的方法实现操作
        Array.prototype.push.call(this, val);
        // 通知视图渲染
        compile();
    }
});
// ...依次再加六个shift\pop\unshift\splice\sort\reverse
Object.setPrototypeOf(prototype, Array.prototype);

// 实现深度数据劫持
const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};
const defineReactive = function defineReactive(proxy, obj, key, val) {
    Object.defineProperty(obj, key, {
        get() {
            return proxy[key];
        },
        set(val) {
            if (data[key] === val) return;
            proxy[key] = val;
            // 通知视图重新渲染
            compile();
        }
    });
    // 除了设置劫持外，如果此项的值是对象或者数组，还需要深度劫持
    observe(val);
};
const observe = function observe(data) {
    let isObject = isPlainObject(data),
        isArray = Array.isArray(data);
    if (!isObject && !isArray) return data;
    if (isArray) {
        // 数组:重构原型对象；遍历数组每一项，如果其中某项值是对象或者数组，需要再次执行observe；
        Object.setPrototypeOf(data, prototype);
        data.forEach(item => observe(item));
        return data;
    }
    // 普通对象:基于Object.defineProperty对对象中的每一个属性做劫持
    // 如果当前对象是被冻结的，则停止劫持
    if (Object.isFrozen(data)) return data;
    let keys = Reflect.ownKeys(data),
        proxy = { ...data };
    keys.forEach(key => {
        defineReactive(proxy, data, key, data[key]);
    });
    return data;
};

let data = {
    msg: 'hello world',
    num: 10,
    obj: {
        name: '珠峰',
        x: Object.freeze({
            n: 100
        }),
        z: [10, 20, [30, 40]]
    },
    arr: [10, 20, 30, { y: 200 }]
};
observe(data);