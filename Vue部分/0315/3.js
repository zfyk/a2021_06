const vm = new Vue({
    data: {
        msg: '珠峰培训',
        link: "<a href='http://www.zhufengpeixun.cn/'>珠峰培训</a>"
    },
    /* 
     methods:定义方法
       + 方法会挂载到vm实例上  vm.func()
       + 方法执行，默认方法中的this，已经在vue内部被处理为：当前类的实例vm「无法改变为其它值，但是设定的函数绝不能是箭头函数」
    */
    methods: {
        func(ev, x, y) {
            console.log('button');
        },
        boxFunc() {
            console.log('boxFunc');
        }
    }
});
vm.$mount('#app');