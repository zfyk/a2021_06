/*
 对象常用的方法
   Object.prototype -> 实例.xxx()
    + hasOwnProperty 检测是否为私有属性（支持Symbol类型的属性名）
    + isPrototypeOf 检测一个对象是否会出现在其原型链上「prototype.isPrototypeOf(obj)：obj按照原型链查找，是否会找到prototype原型对象」
    + propertyIsEnumerable 检测当前属性是不是对象的可枚举(能被for in等遍历的)属性「obj.propertyIsEnumerable(attr)」
    + toString 检测数据类型，返回“[object ?]”
    + valueOf 获取对象的原始值

   Object静态私有属性方法 -> Object.xxx()
    + assign(obj1,obj2,...) 合并多个对象 「ES6」
    + create(prototype) 创建一个空对象，并且让其原型链指向prototype对象
    + fromEntries 可以把Map/数组转换为普通对象
    + getPrototypeOf 获取当前对象的原型对象
    + setPrototypeOf 设置对象的原型对象「ES6：但是兼容IE11」
    + is(val1,val2) 检测两个值是否相等「ES6」
    ------获取对象属性的
    + keys/values/entries 获取当前对象“非Symbol类型、可枚举的、私有的”属性/属性值/全部「结果是包含属性名的数组集合」
    + getOwnPropertySymbols 获取当前对象“Symbol类型、私有的”属性 「ES6」
    + getOwnPropertyNames 获取当前对象“非Symbol类型、私有的”属性
    ------针对的都是对象的属性规则
    + defineProperty(obj, prop, descriptor) 给当前对象的某个属性做“监听劫持” 「defineProperties可以对当前对象的多个属性同时做劫持」
    + freeze 冻结对象：被冻结的对象“不可修改属性值、不可删除属性、不可基于defineProperty设定规则和数据劫持、不可新增属性”，对可枚举型没有影响！
    + isFrozen 检测当前对象是否被冻结
    + preventExtensions 设定对象为不可扩展的：对既有的属性没有任何的影响，只是不允许添加新的属性了
    + isExtensible 检测对象是否是可扩展的
    + seal 密封一个对象：限定对象既有的属性不可删除、不能给对象新增属性，其余的规则没有做限制
    + isSealed 检测对象是否是被密封的
    + getOwnPropertyDescriptor(obj,prop) 获取当前对象某个属性的规则 「getOwnPropertyDescriptors(obj)获取当前对象所有属性的规则（ES8）」

   Reflect ES6新增的一个对象，集成了很多对“对象”进行操作的方法
    + ownKeys 获取所有私有属性「含：各种类型、不管是否可枚举」
    + deleteProperty 删除对象的某个属性，等价于delete操作符
    + get 获取对象的属性值
    + set 设置对象的属性值
    + has 校验对象是否有这个属性，等价于“in”操作符
    + ...
 */

/* let obj = { name: '珠峰', age: 13 };
Object.seal(obj);
console.log(Object.isSealed(obj)); //true */

//===============
/*
let obj = { name: '珠峰', age: 13 };
Object.preventExtensions(obj);
console.log(Object.isExtensible(obj)); //false
*/

//===============
/*
// 基于“===”进行绝对相等比较
// console.log(Object.is(10, '10')); //false
// 特殊处理：
// console.log(NaN === NaN); //false
// console.log(Object.is(NaN, NaN)); //true
// console.log(-0 === +0); //true
// console.log(Object.is(-0, +0)); //false
*/

//===============
/*
let obj = { name: '珠峰', age: 13 };
Object.freeze(obj);
console.log(Object.isFrozen(obj)); //true */


//===============
/* let obj = { name: '珠峰培训', age: 13, [Symbol('AA')]: 100, 2: 20, 1: 10 };
Object.defineProperty(obj, 'test', {
  value: '测试',
  enumerable: false
});
Object.defineProperty(obj, Symbol('BB'), {
  value: 200,
  enumerable: false
});
Object.prototype.AA = 200; */

//===============
/* let obj = { name: '珠峰', age: 13 };

// 作用二：对当前对象的某个属性做劫持「GETTER获取的劫持、SETTER设置的劫持」“Vue2中就是基于Object.defineProperty对响应式数据做监听劫持，从而实现修改数据，可以自动让视图重新渲染”
// 特殊：一但做了数据劫持，就不能再设定writable和value两个规则了，但是可以设置enumerable/configurable
Object.defineProperty(obj, 'age', {
  enumerable: false,
  configurable: false,
  // 当我们获取obj.age属性值的时候，则触发get函数：函数返回值就是我们获取的值
  get() {
    console.log('GETTER');
    return 100;
  },
  // 给obj.age设置属性值的时候，触发set函数：val就是我们新设置的值
  set(val) {
    console.log('SETTER', val);
  }
}); */

/*
// 作用一：给当前对象的某个属性设定相关的规则
Object.defineProperty(obj, 'name', {
  // 设定属性是否可以被删除
  configurable: false,
  // 设置属性是否为可枚举属性
  enumerable: false,
  // 设置属性值
  value: '哈哈哈',
  // 设置属性值是否可以被更改
  writable: false
});
*/

/*
// 作用一：在给对象设置新的属性时，设定一些“可删除、可修改、可枚举、默认值”等规则
Object.defineProperty(obj, 'x', {
  configurable: false,
  enumerable: false,
  writable: false,
  value: 100
});
Object.defineProperty(Object.prototype, 'AA', {
  //默认configurable、enumerable、writable都是false
  value: function () {

  }
});
*/

//===============
/* 
// 返回的是obj1的堆内存「修改的也是obj1」
//  + 先用obj2替换obj1：如果都具备的属性，以obj2为准；如果obj1有，但是obj2没有，则保持原来值即可；如果obj1没有，但是obj2有，则给obj1新设置一个！
//  + 再用obj3替换obj1
let obj1 = { name: 'obj1', age: 25, x: 100 },
  obj2 = { name: 'obj2', y: 200 },
  obj3 = { name: 'obj3', age: 30, z: 300 };
// console.log(Object.assign(obj1, obj2, obj3));
// 如果不想修改obj1，也不想返回obj1：第一个参数设置为一个新的空对象即可
console.log(Object.assign({}, obj1, obj2, obj3)); 
*/