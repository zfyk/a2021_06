/* 按需导入ElementUI中需要使用的组件 */
import Vue from 'vue';
import { Button, Link, Input, DatePicker, Form, FormItem, Table, TableColumn, Tag, Loading, Message, Dialog, Popconfirm } from 'element-ui';

Vue.prototype.$message = Message; //后期基于 this.$message() 做提示
Vue.use(Button) //创建一个全局组件，后期视图中可以基于 el-button 调用组件
    .use(Link)
    .use(Input)
    .use(DatePicker)
    .use(Form)
    .use(FormItem)
    .use(Table)
    .use(TableColumn)
    .use(Tag)
    .use(Loading.directive) //设置一个叫做 v-loading 的自定义指令
    .use(Dialog)
    .use(Popconfirm);