import Vue from 'vue';
import App from './App.vue';
import './element';
import api from './api';

Vue.prototype.$api = api; //把发送数据请求的方法挂载到Vue的原型上，在各个组件中基于 this.$api.xxx() 完成数据请求的发送，无需再单独导入api文件了
Vue.config.productionTip = false;
new Vue({
  render: h => h(App),
}).$mount('#app');