import axios from "axios";
import qs from 'qs';
import { isPlainObject } from '@/assets/utils';
import { Message } from 'element-ui';
axios.defaults.baseURL = '';
axios.defaults.timeout = 60000;
axios.defaults.transformRequest = data => {
    if (isPlainObject(data)) return qs.stringify(data);
    return data;
};
axios.interceptors.response.use(response => {
    return response.data;
}, reason => {
    Message.error('小主，当前网络繁忙，请您稍后再试~');
    return Promise.reject(reason);
});
export default axios;