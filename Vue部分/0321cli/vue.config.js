const { defineConfig } = require('@vue/cli-service');
// 获取环境变量
//   + $ npm run serve  env=development 开发环境
//   + $ npm run build  env=production 生产环境
const env = process.env.NODE_ENV;
module.exports = defineConfig({
  // 默认情况下（false），不会对node_modules中的文件进行ES6转ES5的编译；如果我们安装使用的某个模块，是基于ES6开发的，需要进行代码转换，则我们可以设置一个数组，把node_modules中需要编译的模块放入数组中；不建议设置为true，这样不管需要不需要，node_modules中的模块也都会基于babel-loader编译一次，减慢打包的速度！
  transpileDependencies: false,
  // 控制打包后导入资源的前缀
  publicPath: './',
  // 配置多页面应用
  // pages:{},
  // 控制ESLint词法检测规则的「生产环境关闭词法检测，有助于提高打包的速度」
  lintOnSave: env !== 'production',
  // 开发环境下打包编译的时候，取消map文件的生成，这样可以加快打包速度
  productionSourceMap: false,
  //============下述的配置需要对webpack的语法有了解
  // 向现有的webpack配置项中，加入新的配置项，以此实现自己需要的功能
  /* configureWebpack: {
    plugins: [

    ],
    module: {
      rules: []
    }
  }, */
  // 修改现有的webpack配置项
  /* chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap(options => {
        // 修改配置项
        return options;
      })
  } */
  // 修改CSS各个加载器的配置项
  /* css: {
    loaderOptions: {
      css: {
        // 这里的选项会传递给 css-loader
      },
      postcss: {
        // 这里的选项会传递给 postcss-loader
      },
      less: {
        // 这里的选项会传递给 less-loader
      }
    }
  } */
  //============修改webpack-dev-server的配置项「开发环境」
  devServer: {
    // ... 完全按照dev-server的语法进行修改
    open: true,
    host: '127.0.0.1',
    // 配置proxy跨域代理
    // proxy: 'https://www.jianshu.com', //这样写：项目中所有请求都代理到简书服务器
    proxy: {
      // 配置多个代理的地址「基于请求前缀来区分」
      '/api': {
        target: 'https://www.jianshu.com',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      },
      '/zhihu': {
        target: 'https://news-at.zhihu.com/api/4',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/zhihu': ''
        }
      }
    }
  }
});