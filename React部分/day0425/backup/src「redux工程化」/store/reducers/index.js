import { combineReducers } from 'redux';
import voteReducer from './vote';
import personalReducer from './personal';

/* 
把各模块的REDUCER合并为一个 
  公共状态:会按照指定的模块名进行状态的单独管理
  state = {
      vote:{
        supNum:10,
        oppNum:5,
        info:null
      },
      personal:{
        info:null,
        power:''
      }
  }
*/
const reducer = combineReducers({
    vote: voteReducer,
    personal: personalReducer
});
export default reducer;