import { Component } from 'react';
import VoteFooter from './VoteFooter';
import VoteMain from './VoteMain';
import Theme from '../Theme';

export default class Vote extends Component {
    static contextType = Theme;

    render() {
        const { store } = this.context;
        let { supNum, oppNum } = store.getState().vote;

        return <div className="vote-box">
            <div className="header">
                <h3 className="title">React比Vue好学!</h3>
                <span className="num">{supNum + oppNum}</span>
            </div>
            <VoteMain />
            <VoteFooter />
        </div>;
    }

    // 组件第一次渲染完成：把让组件更新的办法插入到事件池中
    componentDidMount() {
        this.context.store.subscribe(() => {
            this.forceUpdate();
        });
    }
};