import { Component } from 'react';
import Theme from '../Theme';
import actions from '../store/actions';

export default class VoteFooter extends Component {
    static contextType = Theme;
    render() {
        const { store } = this.context;
        return <div className="footer">
            <button onClick={() => {
                store.dispatch(actions.vote.support());
            }}>支持</button>

            <button onClick={() => {
                store.dispatch(actions.vote.oppose());
            }}>反对</button>
        </div>;
    }
};