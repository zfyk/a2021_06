import React from 'react';
import ReactDOM from 'react-dom';

import './assets/reset.min.css';
import './views/Vote.less';
import Vote from './views/Vote';

// 把创建的store导入且放在上下文中：供后代获取到store进行相关操作
import store from './store';
import Theme from './Theme';

ReactDOM.render(
  <Theme.Provider
    value={{
      store
    }}>
    <Vote />
  </Theme.Provider>,
  document.getElementById('root')
);