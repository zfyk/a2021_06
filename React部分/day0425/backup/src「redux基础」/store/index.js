import { createStore } from 'redux';

// 创建reducer管理员
const initial = {
    supNum: 10,
    oppNum: 5
};
const reducer = function reducer(state = initial, action) {
    // 把state克隆：防止操作state直接修改了容器中的状态
    state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'sup':
            state.supNum++;
            break;
        case 'opp':
            state.oppNum++;
            break;
        default:
    }
    // 期望return的内容是替换容器中状态的
    return state;
};


// 创建容器
const store = createStore(reducer);
export default store;