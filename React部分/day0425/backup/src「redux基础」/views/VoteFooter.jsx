import { Component } from 'react';
import Theme from '../Theme';

export default class VoteFooter extends Component {
    static contextType = Theme;

    render() {
        const { store } = this.context;

        return <div className="footer">
            <button onClick={() => {
                store.dispatch({
                    type: 'sup'
                });
            }}>支持</button>

            <button onClick={() => {
                store.dispatch({
                    type: 'opp'
                });
            }}>反对</button>
        </div>;
    }
};