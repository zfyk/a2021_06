import { Component } from 'react';
import VoteFooter from './VoteFooter';
import VoteMain from './VoteMain';
import Theme from '../Theme';

export default class Vote extends Component {
    static contextType = Theme;

    render() {
        const { store } = this.context;
        let { supNum, oppNum } = store.getState();

        return <div className="vote-box">
            <div className="header">
                <h3 className="title">React比Vue好学!</h3>
                <span className="num">{supNum + oppNum}</span>
            </div>
            <VoteMain />
            <VoteFooter />
        </div>;
    }

    // 组件第一次渲染完成：把让组件更新的办法插入到事件池中
    componentDidMount() {
        const unsubscribe = this.context.store.subscribe(() => {
            // 方法执行,让当前组件更新：强制更新forceUpdate、修改某个状态setState...
            this.forceUpdate();
        });
        // 返回的unsubscribe方法执行，就是把刚才加入到事件池中的方法移除
    }
};