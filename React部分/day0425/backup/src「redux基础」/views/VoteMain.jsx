import { useContext, useEffect, useState } from 'react';
import Theme from '../Theme';

export default function VoteMain() {
    const { store } = useContext(Theme);
    let { supNum, oppNum } = store.getState();

    // 第一次渲染完:向事件池中加入让组件更新的方法
    let [num, setNum] = useState(0);
    useEffect(() => {
        store.subscribe(() => {
            setNum(+new Date());
        });
    }, []);

    return <div className="main">
        <p>支持人数：{supNum}</p>
        <p>反对人数：{oppNum}</p>
    </div>;
};