import React from 'react';
import ReactDOM from 'react-dom';

import './assets/reset.min.css';
import './views/Vote.less';
import Vote from './views/Vote';

import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
  <Provider store={store}>
    <Vote />
  </Provider>,
  document.getElementById('root')
);