import { connect } from 'react-redux';
const VoteMain = function VoteMain(props) {
    let { supNum, oppNum } = props;
    return <div className="main">
        <p>支持人数：{supNum}</p>
        <p>反对人数：{oppNum}</p>
    </div>;
};
export default connect(state => state.vote)(VoteMain);