import { Component } from 'react';
import VoteFooter from './VoteFooter';
import VoteMain from './VoteMain';
import { connect } from 'react-redux';

class Vote extends Component {
    render() {
        let { supNum, oppNum } = this.props;
        return <div className="vote-box">
            <div className="header">
                <h3 className="title">React比Vue好学!</h3>
                <span className="num">{supNum + oppNum}</span>
            </div>
            <VoteMain />
            <VoteFooter />
        </div>;
    }
};
export default connect(state => state.vote)(Vote);


/* export default connect(
    state => {
        return {
            supNum: state.vote.supNum,
            oppNum: state.vote.oppNum
        };
    }
)(Vote); */

/* 
 connect([mapStateToProps],[mapDispatchToProps])([渲染的组件])
   [mapStateToProps]:函数，把redux容器中的公共状态，基于属性传递给组件
      connect(state=>{
        //state存储的就是redux容器中的状态 {vote:{...},personal:{...}}
        //return对象中有哪些值，就把这些值作为属性传递给组件
        return {
           a:state.vote.info,
           supNum:state.vote.supNum
        };
      })(Vote)
      -----
      Vote : this.props.a存储的是state.vote.info
      ...
 */