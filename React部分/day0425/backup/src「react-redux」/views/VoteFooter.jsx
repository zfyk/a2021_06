import { Component } from 'react';
import actions from '../store/actions';
import { connect } from 'react-redux';

class VoteFooter extends Component {
    render() {
        let { support, oppose } = this.props;
        return <div className="footer">
            <button onClick={support}>支持</button>
            <button onClick={oppose}>反对</button>
        </div>;
    }
};
export default connect(null, actions.vote)(VoteFooter);

/* export default connect(
    null,
    dispatch => {
        return {
            support() {
                dispatch(actions.vote.support());
            },
            oppose() {
                dispatch(actions.vote.oppose());
            }
        };
    }
)(VoteFooter); */

/* 
 [mapDispatchToProps]函数，把需要派发的方法，基于属性传递给组件
    dispatch => {
        // dispatch:它就是store.dispatch这个方法,基于这个方法派发任务 "dispatch([ACTION对象])"
        // 返回啥，就把啥当做属性传递给组件
        return {
            support(){
                dispatch({
                    type:'xxx',
                    ...
                })
            }
        };
    }
    ------
    Vote: this.props.support()
    ...
 */