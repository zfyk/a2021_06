import voteAction from "./vote";
import personalAction from "./personal";

/*
合并各版块的ACTION 
  actions = {
      vote:{
          support(){...},
          oppose(){...}
      },
      personal:{
          
      }
  }
 */
const actions = {
    vote: voteAction,
    personal: personalAction
};
export default actions;