import React from 'react';
import ReactDOM from 'react-dom';
import Task from './views/Task';
/* ANTD */
import { ConfigProvider } from 'antd';
import zhCN from 'antd/lib/locale/zh_CN';
import './index.less';
/* REDUX */
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <Task />
    </Provider>
  </ConfigProvider>,
  document.getElementById('root')
);