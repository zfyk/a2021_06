import { combineReducers } from 'redux';
import taskReducer from './task';

const reducer = combineReducers({
    task: taskReducer
});
export default reducer; 