import * as TYPES from '../action-types';

const initial = {
    list: null
};
export default function taskReducer(state = initial, action) {
    state = JSON.parse(JSON.stringify(state));
    let { type, payload, id } = action;
    if (type === TYPES.TASK_QUERY) {
        // 同步全部任务
        state.list = payload;
    }
    if (type === TYPES.TASK_DELETE && state.list) {
        // 删除指定任务
        state.list = state.list.filter(item => {
            return +item.id !== +id;
        });
    }
    if (type === TYPES.TASK_UPDATE && state.list) {
        // 把指定任务设置为已完成
        state.list = state.list.map(item => {
            if (+id === +item.id) {
                item.state = 2;
                item.complete = new Date().toLocaleString('zh-CN', { hour12: false });
            }
            return item;
        });
    }
    return state;
};