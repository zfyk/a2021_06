import * as TYPES from '../action-types';
import api from '../../api';

const taskAction = {
    // 派发:获取全部任务
    async queryAllDispatch() {
        let { code, list } = await api.queryList();
        if (+code !== 0) list = [];
        return {
            type: TYPES.TASK_QUERY,
            payload: list
        };
    },
    // 派发:删除指定任务
    delTaskDispatch(id) {
        return {
            type: TYPES.TASK_DELETE,
            id
        };
    },
    // 派发:完成指定任务
    updateTaskDispatch(id) {
        return {
            type: TYPES.TASK_UPDATE,
            id
        };
    }
};
export default taskAction;