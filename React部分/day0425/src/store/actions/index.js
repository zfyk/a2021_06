import taskAction from "./task";

const actions = {
    task: taskAction
};
export default actions;