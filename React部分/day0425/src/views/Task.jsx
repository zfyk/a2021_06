import { useState, useEffect } from "react";
import { connect } from 'react-redux';
import { Button, Tag, Table, Modal, Form, Input, DatePicker, Popconfirm, message } from 'antd';
import { formatTime } from '../assets/utils';
import './Task.less';
import actions from '../store/actions';
import api from '../api';

const Task = function Task(props) {
    /* 表格列 */
    const columns = [{
        title: '编号',
        dataIndex: 'id',
        align: 'center',
        width: '8%'
    }, {
        title: '任务描述',
        dataIndex: 'task',
        width: '50%'
    }, {
        title: '状态',
        dataIndex: 'state',
        align: 'center',
        width: '10%',
        render: text => +text === 1 ? '未完成' : '已完成'
    }, {
        title: '完成时间',
        dataIndex: 'time',
        align: 'center',
        width: '15%',
        render: (_, record) => {
            let { state, time, complete } = record;
            if (+state === 2) time = complete;
            return formatTime(time, '{1}-{2} {3}:{4}');
        }
    }, {
        title: '操作',
        render: (_, record) => {
            let { id, state } = record;
            return <>
                <Popconfirm title="您确定要删除此任务吗?"
                    onConfirm={deleteTask.bind(null, id)}>
                    <Button type="link">删除</Button>
                </Popconfirm>
                {+state !== 2 ? <Popconfirm title="您确定要修改此任务吗?"
                    onConfirm={updateTask.bind(null, id)}>
                    <Button type="link">完成</Button>
                </Popconfirm> : null}
            </>;
        }
    }];

    /* 属性&状态 */
    let { list, queryAllDispatch, delTaskDispatch, updateTaskDispatch } = props;
    let [tableData, setTableData] = useState([]),
        [select, setSelect] = useState(0),
        [loading, setLoading] = useState(false);
    let [visible, setVisible] = useState(false),
        [confirmLoading, setConfirmLoading] = useState(false),
        [form] = Form.useForm();

    /*  第一次渲染完 */
    useEffect(async () => {
        // 如果redux中没有全部任务，则进行派发
        if (!list) {
            setLoading(true);
            await queryAllDispatch();
            setLoading(false);
        }
    }, []);

    /* 计算tableData的值 */
    useEffect(() => {
        if (!list) list = [];
        if (select !== 0) {
            // 选中的非"全部"
            list = list.filter(item => +item.state === select);
        }
        setTableData(list);
    }, [list, select]);

    /* 普通方法 */
    // 新增任务
    const submit = async () => {
        let ruleForm = await form.validateFields();
        ruleForm.time = ruleForm.time.format('YYYY-MM-DD HH:mm:ss');
        // 表单校验通过
        setConfirmLoading(true);
        let { code } = await api.addTask(ruleForm);
        if (+code !== 0) {
            message.error('当前新增任务失败，请稍后再试~');
            setConfirmLoading(false);
            return;
        }
        message.success('恭喜您，任务新增成功~');
        cancel();
        // 重新派发任务,同步所有任务信息到redux容器中
        setLoading(true);
        await queryAllDispatch();
        setLoading(false);
    };

    // 取消弹框
    const cancel = () => {
        form.resetFields();
        setConfirmLoading(false);
        setVisible(false);
    };

    // 删除任务
    const deleteTask = async (id) => {
        let { code } = await api.removeTask(id);
        if (+code !== 0) {
            message.error('当前操作失败，请稍后重试~');
            return;
        }
        message.success('恭喜您，操作成功啦~');
        // 派发任务,删除redux容器中的信息
        delTaskDispatch(id);
    };

    // 完成任务
    const updateTask = async (id) => {
        let { code } = await api.completeTask(id);
        if (+code !== 0) {
            message.error('当前操作失败，请稍后重试~');
            return;
        }
        message.success('恭喜您，操作成功啦~');
        // 派发任务,修改redux容器中的信息
        updateTaskDispatch(id);
    };


    return <div className="task-box">
        <header className="head-box">
            <h2 className="title">TASK OA 任务管理系统</h2>
            <Button type="primary"
                onClick={() => {
                    setVisible(true);
                }}>
                新增任务
            </Button>
        </header>

        <section className="tag-box">
            {['全部', '未完成', '已完成'].map((item, index) => {
                return <Tag key={index}
                    color={index === select ? '#108ee9' : ''}
                    onClick={() => {
                        if (select === index) return;
                        setSelect(index);
                    }}>
                    {item}
                </Tag>;
            })}
        </section>

        <Table pagination={false}
            rowKey="id"
            columns={columns}
            dataSource={tableData}
            loading={loading} />

        <Modal keyboard={false} maskClosable={false} okText="提交信息" title="新增任务窗口"
            confirmLoading={confirmLoading}
            visible={visible}
            onOk={submit}
            onCancel={cancel}>
            <Form layout="vertical"
                initialValues={{ task: '', time: '' }}
                form={form}>
                <Form.Item label="任务描述" name="task" validateTrigger="onBlur"
                    rules={[{ required: true, pattern: /^[\w\W]{6,}$/, message: '内容为必填且不少于6位!' }]}>
                    <Input.TextArea rows={4} />
                </Form.Item>
                <Form.Item label="任务预期完成时间" name="time" validateTrigger="onBlur"
                    rules={[{ required: true, message: '完成时间是必填项!' }]}>
                    <DatePicker showTime />
                </Form.Item>
            </Form>
        </Modal>
    </div>;
};

export default connect(
    state => state.task,
    actions.task
)(Task);