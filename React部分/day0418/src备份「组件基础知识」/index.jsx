import React from 'react';
import ReactDOM from 'react-dom';
import './assets/reset.min.css';
import TestPure from './views/TestPure';
import Vote from './views/VoteHooks';

ReactDOM.render(
  <>
    {/* <Vote title="React比Vue好学">
      <div name="bottom">哈哈哈</div>
      <div name="top">呵呵呵</div>
    </Vote> */}

    <Vote title="React比Vue好学" />

    {/* <TestPure /> */}
  </>,
  document.getElementById('root')
);