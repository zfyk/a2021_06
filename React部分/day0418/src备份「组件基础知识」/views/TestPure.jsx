import { Component, PureComponent } from 'react';

/*
 PureComponent 相对于 Component 来讲，自动设置了 shouldComponentUpdate 周期函数
   + 在此周期函数中，对属性和状态做了一个“浅比较”
   + 如果发现没有任何值的变化，则不进行更新
 */
class TestPure extends PureComponent {
    state = {
        arr: [10, 20] //0x001
    };

    change = () => {
        let { arr } = this.state; //0x001
        arr.push(100); //已经把state中arr的值改了：堆内存末尾放个100
        // console.log(this.state.arr); //[10,20,100] 
        this.setState({
            arr: [...arr] //0x002
        });
    };

    /* shouldComponentUpdate(nextProps, nextState) {
        // nextState:即将修改的状态  this.state:修改之前的状态
        let flag = false,
            nextKeys = Reflect.ownKeys(nextState),
            prevKeys = Reflect.ownKeys(this.state);
        if (nextKeys.length !== prevKeys.length) return true;
        for (let i = 0; i < nextKeys.length; i++) {
            let key = nextKeys[i];
            if (nextState[key] !== this.state[key]) {
                flag = true;
                break;
            }
        }
        return flag;
    } */

    render() {
        console.log('RENDER', this);
        return <>
            {this.state.arr.toString()}
            <button onClick={this.change}>哈哈哈</button>
        </>;
    }
}
export default TestPure;