/*
 函数组件：创建一个函数，返回对应的JSX
   + 形参props：用来接收传递的属性和插槽「只读的 -> 传递的属性对象是被冻结的」
     + 包含了传递的属性值
     + children存储的是传递的进来的子节点信息(类似于插槽):不存在/一个对象/一个数组
     + React.Children对象包含了对children操作的相关方法，内部忽略了children的值类型，什么类型值都不会报错：forEach/map/toArray...

   + 我们可以基于官方插件 prop-types 设置属性规则：给函数/类设置静态私有属性
     + defaultProps 用来给属性设置默认值的
     + propTypes 用来给属性设置规则的，对于不符合规则的情况，控制台会给予提示，但是不影响渲染 
       https://github.com/facebook/prop-types

  ----------
  函数组件是“静态组件”
    + 调用组件<Vote .../>：把Vote函数执行，把解析出来的“属性和children”基于实参传递给函数「特殊：如果设置了属性的校验规则，会把获取的属性先去进行校验」，最后把函数执行返回的JSX元素进行解析渲染！
    + 在这个过程中，状态、周期函数...等操作都没有，默认也没有提供“修改数据可以让视图更新”的办法，所以函数组件也叫“无状态组件”，意思是第一次渲染完，以后就不会再更新了，视图也不会再变化了（除非重新调用）！！
    + 相比较于类组件来讲，渲染速度快~~
 */
import React from 'react';
import PropTypes from 'prop-types';
import './Vote.less';

const Vote = function Vote(props) {
    let { title, children } = props,
        slotTop,
        slotBottom;
    children = React.Children.toArray(children);
    children.forEach(item => {
        let name = item.props.name;
        if (name === 'top') slotTop = item;
        if (name === 'bottom') slotBottom = item;
    });

    let supNum = 10,
        oppNum = 5;
    const change = type => {
        if (type === 'sup') {
            supNum++;
        } else {
            oppNum++;
        }
        console.log(supNum, oppNum);
    };

    return <div className="vote-box">
        {slotTop}
        <div className="header">
            <h3 className="title">{title}</h3>
            <span className="num">{supNum + oppNum}</span>
        </div>
        <div className="main">
            <p>支持人数：{supNum}</p>
            <p>反对人数：{oppNum}</p>
        </div>
        <div className="footer">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>
        {slotBottom}
    </div>;
};

// 设置属性规则
Vote.defaultProps = {
    title: '我是默认的title属性值'
};
Vote.propTypes = {
    title: PropTypes.string
};

export default Vote;