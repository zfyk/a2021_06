/*
 Hooks组件：在函数组件的基础上，使用React提供的hooks函数，让静态组件动态化 
   + useState:在函数组件中应用状态
     useState(状态初始值) -> 返回值是个数组 [第一项：获取的状态值，第二项：修改状态的方法]
       let [num,setNum]=useState(10);
       console.log(num); //10
       setNum(100); //一方面把状态改为100，并且通知视图更新
   + useEffect:在函数组件中应用周期函数
   + useRef:在函数组件中获取DOM元素

 函数组件的视图更新：就是把函数再执行一遍，返回最新的JSX
 */
import { useState, useEffect, useRef } from 'react';
import './Vote.less';

const Vote = function Vote(props) {
    let { title } = props;
    let [supNum, setSupNum] = useState(10),
        [oppNum, setOppNum] = useState(5);
    const change = type => {
        if (type === 'sup') {
            setSupNum(supNum + 1);
            return;
        }
        setOppNum(oppNum + 1);
    };

    /* 使用周期函数 */
    /* useEffect(() => {
        console.log('第一次渲染完成');
        setTimeout(() => {
            //useEffect的副作用：获取的是第一次组件渲染的状态值
            //获取的是第一次渲染的值10
            console.log(supNum);
        }, 5000);
    }, []); */
    /* useEffect(() => {
        console.log('第一次渲染完和每一次更新都触发');
        setTimeout(() => {
            // 副作用：获取的值是当前那一次更新时，存储的状态值
            console.log(supNum);
        }, 5000);
    }); */
    /* useEffect(() => {
        console.log('第一次渲染完会执行，后期只有依赖的状态值supNum改变，才会触发执行！');
    }, [supNum]); */
    /* useEffect(() => {
        return () => { 
            // 会在组件释放的时候执行，类似于 componentWillUnmount
        };
    }); */

    /* 使用REF */
    const box = useRef(null);
    useEffect(() => {
        console.log(box.current);
    }, []);

    return <div className="vote-box" ref={box}>
        <div className="header">
            <h3 className="title">{title}</h3>
            <span className="num">{supNum + oppNum}</span>
        </div>
        <div className="main">
            <p>支持人数：{supNum}</p>
            <p>反对人数：{oppNum}</p>
        </div>
        <div className="footer">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>
    </div>;
};
export default Vote;


/* 
const Vote = function Vote(props) {
    let { title } = props;
    let [state, setState] = useState({
        supNum: 10,
        oppNum: 5
    });
    const change = type => {
        if (type === 'sup') {
            // 不支持部分状态更改，传递啥值，就把状态整体改为啥
            setState({
                ...state,
                supNum: state.supNum + 1
            });
            return;
        }
        setState({
            ...state,
            oppNum: state.oppNum + 1
        });
    };
    let { supNum, oppNum } = state;
    return <div className="vote-box">
        <div className="header">
            <h3 className="title">{title}</h3>
            <span className="num">{supNum + oppNum}</span>
        </div>
        <div className="main">
            <p>支持人数：{supNum}</p>
            <p>反对人数：{oppNum}</p>
        </div>
        <div className="footer">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>
    </div>;
}; 
*/