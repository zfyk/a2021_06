/*
 类组件：创建一个类，并且让其继承React.Component/PureComponent父类，并且需要在render这个生命周期函数中返回JSX元素（构建的视图）！！

 在类组件中，实例(this)是非常重要的，因为属性/上下文/状态/refs/setState/forceUpdate...都和它有关，所以我们要确保各个方法中，都可以正常的获取到this实例
   + vue：内部做了处理，保证 computed/watch/methods... 方法中的this都是实例
   + react：只保证周期函数中的this是实例，但是自己写的其它方法并不给予特殊处理，所以需要我们手动去保证这个事情 “一般我们都把函数写成箭头函数，箭头函数没有自己的this，用到的this都是所处上下文中的，也就是都是当前实例了”

 -------
 调用类组件的时候：首先创建这个类的一个实例「实例.__proto__ -> Vote.prototype -> Component.prototype -> Object.prototype 就可以调用实例私有的属性方法和所属类们原型上公共属性方法」
   私有属性：context、props、refs、state...
   公共方法：生命周期函数、setState、forceUpdate、isReactComponent...

 初始化属性(含属性规则校验)、初始化上下文对象
 执行constructor「如果不设置，则默认自己加一个constructor」
    constructor(props, context) {
        super(); //等价于 Component.call(this) 
        //=> this.props/context=undefined  this.refs={}  this.updater=...
    }
 把初始化的属性和上下文都挂载到实例上 -> this.props / this.context
 初始化状态并且挂载到实例上 -> this.state={...}

 触发 UNSAFE_componentWillMount「不安全」: 第一次渲染之前
 触发 render：第一次渲染「render函数必须要有，且返回JSX，这里构建的是组件的视图」
 触发 componentDidMount ：第一次渲染完成
   + 发送数据请求，请求回来后，基于修改状态，控制视图更新
   + 获取DOM元素
   + 手动实现定时器、事件绑定、监听器
   + ...
 
 当基于this.setState(...)修改状态的时候(通知视图更新)：
   + 触发 shouldComponentUpdate ：是否允许本次更新「一般用于性能优化」
     shouldComponentUpdate(nextProps, nextState) {
        // this.props/state 存储还是之前的值
        // nextProps/nextState 存储的是即将要更改的值
        return true; //返回true是允许,继续下面的步骤；返回false则结束更新流程；
     }
   + 触发 UNSAFE_componentWillUpdate : 更新之前
   + 触发 render : 更新 「到这个阶段，this.props/state存储的是最新修改的值」
     + 根据最新的值生成新的虚拟DOM对象
     + DOM-DIFF
     + 差异化渲染
   + 触发 componentDidUpdate ：更新完成
   + 如果之前在 this.setState({...},[callback]) 设置了callback回调函数，则把此回调函数执行，类似于Vue中的$nextTick：状态视图更新后，触发的回调函数

 当基于 this.forceUpdate([callback]) 强制通知视图更新：
   + 跳过shouldComponentUpdate，直接进入下一步：UNSAFE_componentWillUpdate -> render -> componentDidUpdate
   + 最后也会把[callback]执行（前提是设置了）

 当父组件重新调用Vote组件，并且传递了不同的属性值：
   + 触发 UNSAFE_componentWillReceiveProps 
   + 触发 shouldComponentUpdate
   + ...
 */
import { Component } from 'react';
import PropTypes from 'prop-types';
import './Vote.less';

class Vote extends Component {
  /* 设置属性规则 */
  static defaultProps = {};
  static propTypes = {
    title: PropTypes.string.isRequired
  };

  /* 初始状态 */
  state = {
    supNum: 10,
    oppNum: 5,
    num: 0
  };

  /* 普通方法 */
  change = (type) => {
    let { supNum, oppNum } = this.state;
    if (type === 'sup') {
      this.setState({
        supNum: supNum + 1
      });
      return;
    }
    this.setState({
      oppNum: oppNum + 1
    });
  };

  render() {
    let { title } = this.props,
      { supNum, oppNum } = this.state,
      total = supNum + oppNum,
      ratio = '--';
    if (total > 0) ratio = (supNum / total * 100).toFixed(2) + '%';

    return <div className="vote-box">
      <div className="header">
        <h3 className="title">{title}</h3>
        <span className="num">{total}</span>
      </div>
      <div className="main">
        <p>支持人数：{supNum}</p>
        <p>反对人数：{oppNum}</p>
        <p>支持比例：{ratio}</p>
      </div>
      <div className="footer">
        <button onClick={this.change.bind(null, 'sup')}>支持</button>
        <button onClick={this.change.bind(null, 'opp')}>反对</button>
      </div>

      {/* 测试按钮 */}
      {this.state.num}
      <button onClick={this.test}>测试一下</button>
      <button ref={x => {
        // x:是DOM元素
        this.btn2 = x;
      }}>测试一下2</button>
    </div>;
  }

  componentDidMount() {
    this.btn2.addEventListener('click', () => {
      let { num } = this.state;
      this.setState({ num: num + 1 });
      console.log(this.state.num); //1
      /* 
       此处的setState是同步编程：同步修改状态、立即通知视图更新，更新完毕，代码才会继续向下执行
       ---
       总结：setState即可能是同步的，也可能是异步的
         + 出现在周期函数、合成事件绑定中...本身是异步的「这样才好」
         + 出现在其他的异步编程中（例如：原生事件绑定、定时器...），setState就变为同步的了
       */
    });
  }
  componentDidUpdate() {
    console.log('修改完');
  }

  test = () => {
    let { num } = this.state;
    this.setState({
      num: num + 1
    });
    console.log(this.state.num); //0  
    /* 
     在此处，setState的操作是“异步”的：
       + 执行setState并没有立即修改状态，而是把操作加入到队列中，当前上下文中代码执行完毕，再把队列重要做的事“统一”去执行
       优势/原因:
       + 如果连续出现多个setState操作，也不会让视图更新多次，而是最后统一更新一次「性能好」
       + 也是为了更好的管理代码执行“上一个方法中代码都处理完，再去下一个函数/钩子函数中执行”
       + ...
     大部分情况下，setState操作都是异步的，例如：在周期函数中、在合成事件绑定中...
    */
  };
}
export default Vote;