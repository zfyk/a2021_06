import React from 'react';
import ReactDOM from 'react-dom';

import './assets/reset.min.css';
import './views/Vote.less';
import Vote from './views/Vote';

ReactDOM.render(
  <>
    <Vote title="React比Vue好学" />
  </>,
  document.getElementById('root')
);