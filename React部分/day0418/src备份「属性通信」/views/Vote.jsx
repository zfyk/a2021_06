import { Component } from 'react';
import VoteFooter from './VoteFooter';
import VoteMain from './VoteMain';

export default class Vote extends Component {
    state = {
        supNum: 10,
        oppNum: 5
    };

    change = type => {
        let { supNum, oppNum } = this.state;
        if (type === 'sup') {
            this.setState({ supNum: supNum + 1 });
            return;
        }
        this.setState({ oppNum: oppNum + 1 });
    };

    render() {
        let { title } = this.props,
            { supNum, oppNum } = this.state;
        return <div className="vote-box">
            <div className="header">
                <h3 className="title">{title}</h3>
                <span className="num">{supNum + oppNum}</span>
            </div>
            <VoteMain supNum={supNum} oppNum={oppNum} />
            <VoteFooter change={this.change} />
        </div>;
    }
};