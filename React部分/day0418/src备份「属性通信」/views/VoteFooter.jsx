import { Component } from 'react';
export default class VoteFooter extends Component {
    render() {
        let { change } = this.props;
        return <div className="footer">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>;
    }
};