import React from 'react';
import ReactDOM from 'react-dom';

import './assets/reset.min.css';
import './views/Vote.less';
import Vote from './views/Vote';

ReactDOM.render(
  <>
    <Vote />
  </>,
  document.getElementById('root')
);