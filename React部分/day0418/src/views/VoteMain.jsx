import { useContext } from 'react';
import Theme from '../Theme';

export default function VoteMain() {
    let context = useContext(Theme),
        { supNum, oppNum } = context;
    return <div className="main">
        <p>支持人数：{supNum}</p>
        <p>反对人数：{oppNum}</p>
    </div>;
};