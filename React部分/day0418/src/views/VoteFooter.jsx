import { Component } from 'react';
import Theme from '../Theme';

/* export default class VoteFooter extends Component {
    static contextType = Theme;

    render() {
        let { change } = this.context;
        return <div className="footer">
            <button onClick={change.bind(null, 'sup')}>支持</button>
            <button onClick={change.bind(null, 'opp')}>反对</button>
        </div>;
    }
}; */

export default class VoteFooter extends Component {
    render() {
        return <Theme.Consumer>
            {context => {
                let { change } = context;
                return <div className="footer">
                    <button onClick={change.bind(null, 'sup')}>支持</button>
                    <button onClick={change.bind(null, 'opp')}>反对</button>
                </div>;
            }}
        </Theme.Consumer>;
    }
};