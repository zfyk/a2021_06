import { Component } from 'react';
import VoteFooter from './VoteFooter';
import VoteMain from './VoteMain';
import Theme from '../Theme';

export default class Vote extends Component {
    state = {
        supNum: 10,
        oppNum: 5
    };

    change = type => {
        let { supNum, oppNum } = this.state;
        if (type === 'sup') {
            this.setState({ supNum: supNum + 1 });
            return;
        }
        this.setState({ oppNum: oppNum + 1 });
    };

    render() {
        let { supNum, oppNum } = this.state;
        return <Theme.Provider
            value={{
                supNum,
                oppNum,
                change: this.change
            }}>
            <div className="vote-box">
                <div className="header">
                    <h3 className="title">React比Vue好学!</h3>
                    <span className="num">{supNum + oppNum}</span>
                </div>
                <VoteMain />
                <VoteFooter />
            </div>
        </Theme.Provider>;
    }
};