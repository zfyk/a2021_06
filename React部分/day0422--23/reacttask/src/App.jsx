import React, { useState,useEffect } from 'react';
import './assets/reset.min.css';
import './App.less';
import API from "./api/index";
import moment from 'moment';
import {Button,PageHeader,Table,Modal,Input,DatePicker,message} from "antd";
const { TextArea } = Input;
const { confirm } = Modal;

const addZero=function addZero(n){
  return n.length>=2?n:"0"+n;
}
let mylist=[];
 // // 表格数据
  // const dataSource = [
  //   { 
  //     id: '0001',
  //     task: "task管理要完成",
  //     state: 1,  //1 未完成 2 已完成
  //     time:'2019-6-15 :39:00',// 预完成时间   state--->1
  //     complete:'2019-6-15 8:39:00' //真实完成时间  state--->2
  //   },
  //   { 
  //     id: '0002',
  //     task: "task管理要完成2222",
  //     state: 2,  //1 未完成 2 已完成
  //     time:'2019-6-15 :39:00',// 预完成时间   state--->1
  //     complete:'2019-6-15 8:39:00' //真实完成时间  state--->2
  //   }
  // ];

//函数组件---》HOOK组件(useState useEffect)
const App = function App() {
  //定义了一个弹框是否显示的初始值  false
  const [isModalVisible, setIsModalVisible] = useState(false);
 //定义一个表格的初始值, 为 []
  const [dataSource, setdataSource] = useState([]);
  //定义一个按钮选中的初始值, 0
  const [seletced, setseletced] = useState(0);
  //定义一个任务描述的初始值 ""
  const [textValue,settextValue]=useState("");
  //定义一个任务完成时间的初始值 null
  const [timeValue,settimeValue]=useState(null);
  //定义一个参数n
  const [n,setn]=useState("");

  //删除功能
  function delTask(id){
    confirm({
      title: `你确定要删除${id}这项吗？`,
      onOk:async ()=>{//确定
         let {code} =await API.removeTask(id);
         if(+code!==0){
           message.error("删除失败，稍后重试！");
           return;
         }
         message.success("恭喜你，删除成功！");
         setn(+new Date());//更新页面
      }
    });
  }

  //完成任务
  function comTask(id){
      confirm({
        title:`你确定要完成${id}这项吗？`,
        onOk:async ()=>{
            let {code}=await API.completeTask(id);
            if(+code!==0){
              message.error("完成任务失败，请稍后再试！");
              return;
            }
            message.success("恭喜你，操作成功！");
            setn(+new Date());
        }
      })
  }

  const columns = [
    {
      title: '编号',
      dataIndex: 'id'
    },
    {
      title: '任务描述',
      dataIndex: 'task'
    },
    {
      title: '状态',
      dataIndex: 'state',
      render:(text,record,index)=>{
        return +text===1?"未完成":"已完成"
      }
    },
    {
      title: '完成时间',
      dataIndex: 'time',
      render:(_,record)=>{
        let {state,time,complete}=record;
        let mytime= +state===1?time:complete;
        let[,month="00",day="00",hours="00",mins="00"]=mytime.match(/\d+/g)||new Date().toLocaleString()
        let result=`${addZero(month)}-${addZero(day)} ${addZero(hours)}:${addZero(mins)}`;
        return result;
      }
    },
    {
      title: '操作',
      dataIndex: 'todo',
      render:(_,record)=>{
          let {state,id}=record;
          return (
            <div>
              <Button type="link" onClick={delTask.bind(null,id)}>删除</Button>
              {+state===1?<Button type="link" onClick={comTask.bind(null,id)}>完成</Button>:null} 
            </div>
          )
      }
    },
  ];

  //获取表格数据 渲染表格数据  相当于componentDidMount
  //[]数组里面没有参数的时候，相当于componentDidMount
  //[n] 数组里面有参数的时候，(n)参数修改，这方法就会重新执行
  useEffect(()=>{
    API.getTaskList(0).then(value=>{
      //console.log(value);
      let {code,list}=value;
      if(+code!==0){return}
      setdataSource(list);
      mylist=list;
    })
  },[n])

  //点击按钮组，修改表格数据
  function changeTable(index){
    setseletced(index);//切换按钮样式
    if(+index===+seletced){return}
    if(+index!==0){//要么是1，要么是2
       let newarr=mylist.filter(item=>item.state===index)
       setdataSource(newarr);
    }else{
       setdataSource(mylist);
    }
  }

  //弹框数据
  //点击按钮显示弹框
  const showModal = () => {
    setIsModalVisible(true);
  };
  //点击确定回调
  const handleOk = async () => {
    //console.log(textValue,timeValue);
    let {code} = await API.addTask(textValue,timeValue);
    if(+code!==0){
      message.error('添加失败，请重试！');
      return;
    }
    message.success('恭喜你，添加成功！');
    setn(+new Date());//更新页面
    //关闭窗口
    setIsModalVisible(false);
    //清空数据
    settextValue("");
    settimeValue("");
  };
  //点击遮罩层或右上角叉或取消按钮的回调
  const handleCancel = () => {
    setIsModalVisible(false);
    //清空数据
    settextValue("");
    settimeValue("");
  };

  return <div className="App">
        {/* 导航区域  */}
        <PageHeader
            title="TASK OA"
            subTitle="任务管理系统"
            extra={[
              <Button key="1" type="primary" onClick={showModal}>
                新增任务
              </Button>,
            ]}
        ></PageHeader>

        {/* 按钮组 */}
        <div className="btngroup">
          {
              ["全部","未完成","已完成"].map((item,index)=>{
                   return <Button type={+index===+seletced?"primary":""} 
                            key={index}
                            onClick={changeTable.bind(null,index)}
                            >{item}</Button>
              })
          }
        </div>

        {/* 表格 */}
        <Table
        rowKey="id"
        dataSource={dataSource} 
        columns={columns} 
        pagination={false}/>

        {/* 弹框 */}
        <Modal title="新增任务窗口"
         visible={isModalVisible} 
         onOk={handleOk} 
         onCancel={handleCancel}>
           {/* 任务描述 */}
          <div>
            <span>任务描述：</span>
            <TextArea rows={3} 
            value={textValue}
            onChange={(e)=>{
              //console.log(e.target.value);
              settextValue(e.target.value);
            }}></TextArea>
          </div>
          {/* 预期完成时间 */}
          <div className="Date">
            <span>预期完成时间：</span>
            <DatePicker 
               showTime
               onChange={(_,dateString)=>{
                //console.log(date,dateString)
                settimeValue(dateString);
                //时间发生变化的回调
            }} />
          </div>
      </Modal>

  </div>
};

export default App;

