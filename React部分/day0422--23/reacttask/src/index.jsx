import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {ConfigProvider} from 'antd';
// 由于 antd 组件的默认文案是英文，所以需要修改为中文
import zhCN from 'antd/lib/locale/zh_CN';
import 'moment/locale/zh-cn';
import 'antd/dist/antd.css';
//import API from "./api/index"

//测试发送请求-----》对fetch进行二次封装
// fetch("/api/getTaskList?limit=100&page=1&state=0").then((response)=>{
//   return response.json();//返回json格式
// }).then(value=>{
//   console.log(value);
// }).catch(error=>{
//   console.log(error);
// })

//测试fetch二次封装，发送请求
// API.getTaskList(0).then(value=>{
//   console.log(value);
// }).catch(err=>{
//   console.log(err);
// })


ReactDOM.render(
    <ConfigProvider locale={zhCN}>
      <App/>
    </ConfigProvider>,
  document.getElementById('root')
);
