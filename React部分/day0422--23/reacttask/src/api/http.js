import {
    isPlainObject
} from '../assets/utils';
import qs from 'qs';
import {
    message
} from 'antd';
let baseURL = "";
export default function request(url, config) {
    if (typeof url !== "string") throw new TypeError("url must be a string");
    if (!isPlainObject(config)) config = {};
    let {
        method,
        params,
        body,
        responseType,
        headers,
        cache,
        credentials
    } = Object.assign({
        method: 'GET',
        params: null,
        body: null,
        responseType: 'json',
        headers: null,
        cache: 'no-cache',
        credentials: 'include'
    }, config);
    if (!/^https?:\/\//i.test(url)) url = baseURL + url;
    if (!isPlainObject(headers)) headers = {};
    if (params) {
        if (isPlainObject(params)) params = qs.stringify(params);
        url += `${url.includes('?')?'&':'?'}${params}`;
    }
    if (body && isPlainObject(body)) {
        body = qs.stringify(body);
        headers["Content-Type"] = "application/x-www-form-urlencoded";
    }
    method = method.toUpperCase();
    config = {
        method,
        headers,
        cache,
        credentials
    };
    if (/^(POST|PUT|PATCH)$/i.test(method) && body) config.body = body;
    return fetch(url, config).then(response => {
        let {
            status,
            statusText
        } = response;
        if (status >= 200 && status < 300) {
            let result;
            switch (responseType.toUpperCase()) {
                case 'JSON':
                    result = response.json();
                    break;
                case 'TEXT':
                    result = response.text();
                    break;
                case 'BLOB':
                    result = response.blob();
                    break;
                default:
                    result = response.arrayBuffer();
            }
            return result;
        }
        return Promise.reject({
            code: "HTTP ERROR",
            status,
            statusText
        });
    }).catch(reason => {
        if (reason && reason.code === "HTTP ERROR") {
            switch (reason.status) {
                case 403:
                    message.error(`小主，当前请求参数错误或者无权限~`);
                    break;
                case 404:
                    message.error(`小主，当前请求地址不存在~`);
                    break;
                case 500:
                    message.error(`小主，服务器端开小差了~`);
                    break;
                default:
                    message.error(`小主，当前网络繁忙，请稍后再试~`);
            }
        } else {
            message.error(`小主，当前网络繁忙，请稍后再试~`);
        }
        return Promise.reject(reason);
    });
};