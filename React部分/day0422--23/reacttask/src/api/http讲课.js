// Request(url,config)
// config:
// {
//     method:"GET",//请求方式
//     params:{},//get请求方式传递的参数放在这里
//     body:{},//post请求方式传递的参数放在这里
//     responseText:'json',//json/blob/arrayBuffer/text
//     headers:{},//headers
//     cache: 'no-cache',// 是否使用缓存 不用
//     credentials: 'include',//是否允许携带跨域资源凭证 允许
// }

//判断是不是纯对象
import {isPlainObject} from "../assets/utils";
import qs from "qs";
import {message} from "antd";
let baseURL="";
export default function request(url,config){
    if(typeof url!=="string"){
        throw new TypeError("url must be string");
    }
    if(!isPlainObject(config)){
        config={}
    }
    let { 
    method,
    params,
    body,
    responseText,
    headers,
    cache,
    credentials}=Object.assign({
       method:"GET",
       params:null,
       body:null,
       responseText:"json",
       headers:null,
       cache:"no-cache",
       credentials:"include"
     },config)
     //处理url是否加基础路径(baseURL)
     if(!/^https?\/\/$/i.test(url)){url=baseURL+url;}
     //处理headers
     if(!isPlainObject(headers)){headers={}}
     //处理params,params最后要拼到url后面
     if(params){
         if(isPlainObject(params)){
            params=qs.stringify(params)//"name=lili&age=10"
         }
         url+=`${url.includes("?")?"&":"?"}${params}`;
     }
     //处理 body
     if(body&&isPlainObject(body)){
         body=qs.stringify(body);//urlencoded格式
         //'Content-Type': 'application/x-www-form-urlencoded'
         headers["Content-Type"]="application/x-www-form-urlencoded";
     }
     //处理method--->转化为大写
     method=method.toUpperCase();

    config={
      method,
      headers,
      cache,
      credentials
    }
    //body --->POST/PUT/PATCH 其中的一个请求，config就要含有body
    if(/^(POST|PUT|PATCH)$/i.test(method)&&body){
       config.body=body;
    }

    return fetch(url,config).then(response=>{
        let {status,statusText}=response;
        if(status>=200&&status<300){//成功
          let result;
          switch (responseText.toUpperCase()) {
              case 'JSON':
                  result=response.json();
                  break;
              case 'BLOB':
                  result=response.blob();
                  break;
              case 'TEXT':
                  result=response.text();
                  break;
              default:
                  result=response.arrayBuffer();
                  break;
          }
          return result;
        }
        //失败
        return Promise.reject({
            status,
            statusText,
            code:"HTTP REEOR"
        })
    }).catch(error=>{//统一处理错误
       if(error&&error.code==="HTTP REEOR"){//状态码的错误
           switch (error.status) {
               case 403:
                   message.error("请求参数信息错误，权限不足");
                   break;
               case 404:
                    message.error("请求地址不存在");
                    break;
               case 500:
                    message.error("服务器开小差");
                    break;
               default:
                   message.error("网速不行，请稍后再试");
                   break;
           }

       }else{//网络问题，请求超时
            message.error("网速不行，请稍后再试");
       }
       return Promise.reject(error);
    })
}