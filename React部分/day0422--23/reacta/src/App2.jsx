import './assets/reset.min.css';
import './App2.less';
import Nav from "./components/Nav";
import Home from './pages/Home';
import System from './pages/System';
import {HashRouter,Route,Switch,Redirect} from "react-router-dom";
let flag=true;
const App = function App() {
    return <HashRouter>
        <div>  
            <Nav/>
            <section className="content">
               {/* path的路径，不是精准匹配 */}
               {/* /home---> /  /home*/}
               {/* /system---> /  /system*/}
               
               {/* Switch  找到了第一个匹配的路径就不会往下找了 */}
               <Switch>
                    {/* exact 精准匹配 */}
                    {/* <Route path="/" exact component={Home}></Route> */}

                    <Route path="/" exact render={()=>{
                        //相当于vue里面的路由钩子函数
                        if(flag){
                            return <Redirect to="/home"></Redirect>
                        }else{
                            return <Redirect to="/system"></Redirect>
                        }
                    }}></Route>
                    <Route path="/home" component={Home}></Route>
                    <Route path="/system" component={System}></Route>
                    {/* 没有的路径 */}
                    <Redirect to="/home"></Redirect>
               </Switch>
            </section>
        </div> 
    </HashRouter>
};
export default App;