import { NavLink,Link,withRouter} from "react-router-dom";

// NavLink 会自动添加一个 class="active" 属性
// LINK 不会自动添加一个 class="active" 属性
const Nav = function Nav(props) {

    //默认只要不是配置的路由页面，都不会含有路由的信息
    //通过withRouter来获取当前路由页面的信息
    console.log(props);

    return <section className="nav-box">
        <h2>OA管理系统</h2>
        <nav>
            {/* NavLink 相当于a 标签，解析为a标签 */}
            <NavLink to="/home">首页</NavLink>
            <NavLink to={{
              pathname:"/system"
            }}>员工管理</NavLink>

            {/* <Link to="/home">首页</Link>
            <Link to="/system">员工管理</Link> */}
        </nav>
    </section>;
};
export default withRouter(Nav);