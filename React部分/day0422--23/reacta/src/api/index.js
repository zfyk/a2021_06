import request from './http';

const API = {
    // 获取任务列表信息
    getTaskList(state = 0) {
        return request('/api/getTaskList', {
            params: {
                limit: 1000,
                page: 1,
                state
            }
        });
    },
    // 新增任务
    addTask(task, time) {
        return request('/api/addTask', {
            method: 'POST',
            body: {
                task,
                time
            }
        });
    },
    // 删除任务
    removeTask(id) {
        return request('/api/removeTask', {
            params: {
                id
            }
        });
    },
    // 完成任务
    completeTask(id) {
        return request('/api/completeTask', {
            params: {
                id
            }
        });
    }
};
export default API;