import {Route,Switch,Link,Redirect} from "react-router-dom";

import UserAdd from "./system/UserAdd";
import UserList from "./system/UserList";

const System = function System() {
    return <>
        <div className="menu-box">
            {/* query 传参  不需要修改路由*/}
            <Link to="/system/userlist?name=lili&age=18">员工列表</Link>
            <Link to="/system/useradd">新增员工</Link>
        </div>
        <div className="form-box">
           <Switch>
               <Redirect path="/system" exact to="/system/userlist"></Redirect>
               <Route path="/system/useradd" component={UserAdd}></Route>
               <Route path="/system/userlist" component={UserList}></Route>
           </Switch>
        </div>
    </>;
};
export default System;