import { Route,Switch,Link,Redirect} from "react-router-dom";

import CustomAdd from "./home/CustomAdd";
import CustomList from "./home/CustomList";
const Home = function Home() {
    return <>
        <div className="menu-box">
            {/* params 传参，需要修改路由配置 */}
            <Link to="/home/customlist/100">客户列表</Link>
            <Link to="/home/customadd">新增客户</Link>
        </div>
        <div className="form-box">
             <Switch>
                 {/* 二级路由默认展示的页面 */}
                 <Redirect path="/home" exact to="/home/customlist"></Redirect>
                 <Route path="/home/customadd" component={CustomAdd}></Route>
                 <Route path="/home/customlist/:id" component={CustomList}></Route>
             </Switch>
        </div>
    </>;
};
export default Home;