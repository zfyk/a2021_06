import { Component } from "react";
//生命周期 渲染阶段 更新阶段 销毁阶段
export default class Life extends Component{
    state={
        num:100
    }
    UNSAFE_componentWillMount(){//不建议使用 废弃的
       console.log("将要渲染,没有DOM,有props,state");
    }
    render(){
        let {n}=this.props;
        console.log(this.state);//{num:200}
        console.log("渲染");
        let {num}=this.state;
        return <div>
            life---{num}---{n}
        </div>
    }
    componentDidMount(){//发送 ajax,在componentDidMount这个钩子里发送
        console.log("渲染完成,有DOM");

        this.setState({
            num:200
        },()=>{
            
            //异步----》保证整个生命周期的完整性，
            // 整个更新周期都完成后，才最后执行
            console.log("数据已经变成200了");
        })
        console.log("1111");
        //先执行生命周期钩子函数中的代码，最后才执行 setState里面的回调

        // 想要 1111 后执行, 让1111也变成异步，而且要比他晚
        // setTimeout(()=>{
        //     console.log("1111");
        // },1000)

        //不推荐 强制更新---》不会走shouldComponentUpdate这个钩子
        // this.state.num=1000;
        // this.forceUpdate();
    }
    shouldComponentUpdate(newProps,newState){
        console.log(this.state);//{num:100}
        console.log(newState);//{num:200}
        console.log("组件是否更新");
        //返回值 false 不更新
        //返回值 true  更新,  默认true
        return true;
    }
    UNSAFE_componentWillUpdate(){
        console.log(this.state);//{num:100}
        console.log("更新之前");
    }
    componentDidUpdate(){
        console.log(this.state);//{num:200}
        console.log("更新之后");
    }
    componentWillUnmount(){
        console.log("销毁之前");
    }
    UNSAFE_componentWillReceiveProps(){
        console.log("修改props");
    }
    
}