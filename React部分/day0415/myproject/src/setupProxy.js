const {createProxyMiddleware}=require("http-proxy-middleware");

//配置多源跨域
module.exports=function(app){
   app.use(createProxyMiddleware("/jianshu",{
       target:"https://www.jianshu.com",//跨域的网址
       changeOrigin:true,//host源
       pathRewrite:{//重写 /jianshu--->""
          "/jianshu":""
       }
   }))
   app.use(createProxyMiddleware("/zhihu",{
        target:"https://news-at.zhihu.com",//跨域的网址
        changeOrigin:true,//host源
        pathRewrite:{//重写 /zhihu--->""
        "/zhihu":""
        }
    }))
}