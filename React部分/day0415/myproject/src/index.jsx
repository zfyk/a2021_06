import React from 'react';
import ReactDOM from 'react-dom/client';
//导入类组件
//import One from "./one";

// import Two from "./two"

import LifeParent from "./lifeParent"


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
 <div>
    {/* 使用类组件 */}
    {/* <One name="lili" num={num}></One> */}
    {/* <Two></Two> */}
   <LifeParent></LifeParent>
 </div>
);



