import { Component } from "react";
import PropTypes from "prop-types"
// PropTypes 名字可以随意接收

//类组件 生命周期 state
//通过 export default 导出类组件,类的首字母一定要大写
export default class One extends Component{
    // constructor(props){
    //     super(props);
    //     //底层代码，自动挂载了props
    //     //this.props=props
    //     //this.context=context
    //     //.......

    //     this.state={
    //         classname:"06",
    //         year:"2021"
    //     }
    // }
    static defaultProps={//取默认值   defaultProps名称固定
        name:'糯米团子',
        num:20
    }
    static propTypes={//校验你传递过来的参数 propTypes名称固定
        name:PropTypes.string.isRequired, //name字符串类型，不能为空
        num:PropTypes.oneOf([10,20,30]) 
        // 值必须是10，20，30 中的一个
    }
    state={
        classname:"88",
        year:"2022"
    }
    changeClassname(){
        console.log(this);
        //react 没有双向数据绑定,修改值必须通过方法
        this.setState({
            classname:"6688"
        });
    }
    changeYear=()=>{//箭头函数没有this,指向上下文
        console.log(this);
        this.state.year="202205";
        console.log(this.state);
        this.forceUpdate();//强制更新,页面更新
    }
    render(){//render() 生命周期函数，渲染结构
        //console.log(this.props);
        let{classname,year}=this.state;
        let {name,num}=this.props;
        console.log(this);
        return(
            <div>
                <h1>{name}</h1>
                <h2>{num}</h2>
                <h1>{classname}--{year}</h1>
                <button onClick={this.changeYear}>修改year</button>
                <button onClick={this.changeClassname.bind(this)}>修改classname</button>
            </div>
        )
    }
}

//静态属性
// One.propTypes={//校验你传递过来的参数 propTypes名称固定
//     name:PropTypes.string.isRequired, //name字符串类型，不能为空
//     num:PropTypes.number.isRequired //num 值类型，不能为空
// }
