import { Component } from "react";
import Life from "./life";

export default class LifeParent extends Component{
    state={
        flag:true,
        n:520
    }
    delLife(){
        this.setState({
            flag:false
        })
    }
    changen(){
        this.setState({
            n:123456
        })
    }
    render(){
        let {flag,n}=this.state;
        return (<div>
            {flag?<Life n={n}></Life>:null}
            <button onClick={this.delLife.bind(this)}>销毁life</button>
            <button onClick={this.changen.bind(this)}>修改n</button>
        </div>)
    }
}