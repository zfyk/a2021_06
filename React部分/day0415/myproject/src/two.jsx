import { Component } from "react";
//类组件      受控组件  使用了state
// export default class Two extends Component{
//     state={
//         time:new Date().toLocaleString()
//     }
//     changeTime(){
//         setInterval(() => {
//             this.setState({
//                 time:new Date().toLocaleString()
//             })
//         }, 1000);
//     }
//     render(){
//         let {time}=this.state;
//         return <div>
//             <h1>{time}</h1>
//             <button onClick={this.changeTime.bind(this)}>点击按钮修改时间</button>
//         </div>
//     }
// }

//非受控组件----不是state,直接操作DOM
export default class Two extends Component{
    changeTime(){
        //这种用法已经被废弃
       //console.log(this.refs.one);
       //console.log(this.h1Box);
       setInterval(()=>{
            this.h1Box.innerHTML=new Date().toLocaleString();
       },1000)
    }
    render(){
        return <div>
            {/* <h1 ref="one">{new Date().toLocaleString()}</h1> */}
            <h1 ref={(element)=>{
                this.h1Box=element;
            }}>{new Date().toLocaleString()}</h1>
            <button onClick={this.changeTime.bind(this)}>点击按钮修改时间</button>
        </div>
    }
}