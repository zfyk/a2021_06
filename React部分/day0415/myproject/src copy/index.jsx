import React from 'react';
import ReactDOM from 'react-dom/client';
import Fn from "./fn"
//react 组件  
//1. 函数组件
//  1.1 先创建一个函数组件
//  1.2 导入函数组件 import Fn from "./fn"
//  1.3 使用函数组件  单标签和双标签都可以 <Fn></Fn>   <Fn/>

//2. 类组件
//3. hooks组件

let num=100;

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
 <div>
   <Fn num={num} name="lili">
     <span>111</span>
     <span>222</span>
     <span>333</span>
   </Fn>
   {/* <Fn num="200"/> */}
 </div>
);



