import React from 'react';
import ReactDOM from 'react-dom/client';
//导入 react+react-dom 开发 PC端页面
//导入 react+react-native 开发 移动端 
//渐进式 redux--》dva-->umi(相当于vuex)  react-router-dom
// andt   react没有一个特别好的移动端框架

//react使用css
import "./index.css";
//react 使用less
import "./one.less";

// fetch 默认请求的方式为 get---->es6新增
// fetch(url).then().then()
// 需要配置跨域
// 1. 配置一个网址跨域 
//  package.json 直接配置   "proxy":"https://www.jianshu.com"

//简书的完整网址：https://www.jianshu.com/asimov/subscriptions/recommended_collections
// fetch("/asimov/subscriptions/recommended_collections")
// .then(response=>{//需要一个then过渡
//   return response.json();//返回的结果是json
// }).then(value=>{//接收请求成功的结果
//   console.log(value);
// })

//2. 配置多源跨域(多个网址跨域)
//2.1  第一步 安装 npm i http-proxy-middleware --save
//2.2 第二步 在src目录下面 创建一个 setupProxy.js (名字定死)
//2.3 在 setupProxy.js 里面编写内容

//https://www.jianshu.com/asimov/subscriptions/recommended_collections
fetch("/jianshu/asimov/subscriptions/recommended_collections")
.then(response=>{//需要一个then过渡
  return response.json();//返回的结果是json
}).then(value=>{//接收请求成功的结果
  console.log(value);
})

//https://news-at.zhihu.com/api/4/news/latest
fetch("/zhihu/api/4/news/latest")
.then(response=>{
  return response.json()
}).then(value=>{
  console.log(value);
})


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
    <h1>1111</h1>
    <h2>222</h2>
    <h3>333</h3>
  </div>
);


