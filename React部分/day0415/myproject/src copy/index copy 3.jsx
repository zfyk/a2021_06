import React from 'react';
import ReactDOM from 'react-dom/client';
import "./index.css";
//1. <></> 相当于 vue中的<template></template>  不渲染的标签
//2. 写样式的时候 class--->className
//3. 写style的时候 值是一个对象  
//4. {}不能放{}，但是style 除外
//5. React.createElement(标签名,属性,内容)

// n:3----h3 ？ 1-6
//v-if="n==1"   h1
//v-else-if="n==2"  h2
//v-else-if="n==3"  h3 ...

let styleobj = {
  "color": "red",
  "fontSize": "50px"
}

let n = 1;

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <h1 className="one">1111</h1>
    <h2 style={styleobj}>222</h2>
    <h3 style={{ color: "blue" }}>3333</h3>
    <ul>
      <li>111</li>
      <li>222</li>
      <li>333</li>
    </ul>
    {React.createElement("h" + n, { "title": "123" }, "hahaha")}
  </>
);

console.log(React.createElement("ul", null, 
React.createElement("li", null, "111"), 
React.createElement("li", { className: "two"}, "222"), 
React.createElement("li", null, "333")));


