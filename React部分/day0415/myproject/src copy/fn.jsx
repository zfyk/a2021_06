import React from "react";

//1. 函数组件(静态组件)，名称首字母一定要大写
//   没有 state, 没有生命周期，主要用于展示静态的页面结构
export default function Fn(props){
    //插槽的内容，存放在props.children里面，数组(2个及2个以上是数组) 1个是对象
   // console.log(props);//是一个对象，里面存放着传递过来的参数
//    let name=props.name||"小糯米";//取默认值
   let {num,name}=props
   name=name||"菜包";

   let time=new Date().toLocaleString();
   function changeTime(){
      // console.log(this);
      setInterval(()=>{
        time=new Date().toLocaleString()
        console.log(time);
      },1000)
   }

   return (<div>
       fn---{num}---{name}
       {/* {props.children} */}

       {React.Children.map(props.children,(item,index)=>{
           return <h1 key={index} style={{color:"red"}}>{item}</h1>
       })}

       <h1>{time}</h1>
       <button onClick={changeTime.bind(this)}>点击按钮修改时间</button>
    </div>)
}