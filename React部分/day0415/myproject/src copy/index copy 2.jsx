import React from 'react';
import ReactDOM from 'react-dom/client';

//jsx 语法规则 js+标签混用
// 1.用 {} 包裹值  计算  三元运算
// 2. undefined/null 不展示内容
// 3. 不能用 if else  for  forEach
// 4. 循环数组---》map  循环对象---》Object.keys()/Object.values()  
//    key唯一值
// 5. on事件名（大写）  onClick={show}  不能加show(),函数会自动执行
//    传参 onClick={show.bind(null,10,20)}  bind() 不会自动执行，点击执行
//    可以直接写函数

let num=10,numa=200;
let arr=["aa","bb","cc"];
let obj={
  name:"lili",
  age:18
}

function show(n,m,e){
  console.log(n,m,e);
  //console.log(this);
  //console.log("1111");
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
     <button onClick={show.bind(null,10,20)}>点击事件1</button>

     <button onClick={()=>{
         console.log("222");
     }}>点击事件2</button>

     <h1>{num}</h1>
     <h1>{num+numa}</h1>
     { num>50 ? <h1>大于50</h1> : <h1>小于等于50</h1> }
     { num>50 ? <h1>大于50</h1> : undefined }
     <h2>{arr}</h2>

      {
        arr.map((item,index)=>{
          return <h2 key={index}>{item}</h2>
        })
      }
      {
        Object.keys(obj).map(item=>{
          return item;
        })
      }
      {
        Object.values(obj).map(item=>{
          return item;
        })
      }
  </div>
);


