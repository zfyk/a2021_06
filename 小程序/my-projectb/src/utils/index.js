const BASE_URL = 'http://localhost:8088' //域名或选取所有接口不变的那一部分
export const myRequest = (options) => { //暴露一个function：myRequest，使用options接收页面传过来的参数
        return new Promise((resolve, reject) => { //异步封装接口，使用Promise处理异步请求
            uni.request({ //发送请求
                url: BASE_URL + options.url, //接收请求的API
                method: options.method || 'GET', //接收请求的方式,如果不传默认为GET
                data: options.data || {}, //接收请求的data,不传默认为空
                success: (res) => { //数据获取成功
                    resolve(res.data) //成功,将数据返回
                },
                fail: (err) => { //失败操作
                    reject(err)
                }
            })
        })
    }