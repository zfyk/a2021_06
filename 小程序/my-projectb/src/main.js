import Vue from 'vue'
import App from './App'

//导入
import {myRequest} from "./utils/index.js";
//放到vue的原型上
Vue.prototype.$myRequest=myRequest;

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
