import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
App.mpType = 'app'

//导入封装的ajax
import { myRequest } from './utils/index'
//挂载到Vue的原型上---->使用 this.$myRequest
Vue.prototype.$myRequest=myRequest;

const app = new Vue({
  ...App
})
app.$mount()
