// 完整路径 ： http://localhost:8088/api/xiaomi/swiper
export function myRequest(options){
   let baseURL="http://localhost:8088";
   options.method=options.method&&options.method.toUpperCase();
   return new Promise((resolve,reject)=>{
        uni.request({
            url: baseURL+options.url,
            method:options.method||"GET",
            data: options.data||{},
            header: options.header||{},
            success: (res) => {
                resolve(res.data);
            },
            fail:(error)=>{
                reject(error);
            }
        });
   })
}