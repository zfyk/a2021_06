// components/coma/coma.js
Component({
  /**
   * 组件的属性列表
   * 
   * 子组件接收参数
   */
  properties: {
      user:String,//接收的数据类型
      money:{
        type:String,//接收的数据类型
        value:"30w"//默认值---不传参
      }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //接收父页面方法, 通过 this.triggerEvent 去执行，父页面自定义的方法（fn)
    gethandle(){
        //传递的参数是多个的时候，只能传过去一个 
        //传递的参数是对象最好  
        this.triggerEvent("fn",{a:10,b:20})
    }
  }
})
