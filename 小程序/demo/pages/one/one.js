// pages/one/one.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      arr:[100,200,300],
      myname:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     //通过getApp().globalData 获取公共数据
     
      //console.log(getApp().globalData.name)
      this.setData({
        myname:getApp().globalData.name
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
     console.log("下拉刷新");
     //修改data里面的数值，必须通过 this.setData
     this.setData({
       arr:[400,500,600,100,200,300]
     })

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("上拉触底");
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          title: '自定义转发标题bbb'
        })
      }, 2000)
    })

    return {
      title: '自定义转发标题aaa',
      path: '/page/user?id=123',
      promise 
    }
  },
  //页面滚动触发的事件
  onPageScroll(options){
    //距离顶部滚动的距离  scrollTop
     console.log(options.scrollTop);
  }
})