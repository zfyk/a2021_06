// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
     msg:"哈哈哈",
     arr:["aa","bb","cc"],
     num:88
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     console.log("cart页面初始化，只执行一次");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log("cart页面初次渲染，只执行一次");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("cart页面初次展示");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("cart页面隐藏");
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log("cart页面卸载");
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})