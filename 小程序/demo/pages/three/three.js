
let util=require("../../utils/util");
// pages/three/three.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      imgurl:"",
      videourl:""
  },
  getdom(){
    //1.创建一个query
    let query=wx.createSelectorQuery();
    //2.通过 select / selectAll 查询
    //3.获取元素的各种信息   boundingClientRect  / context/ scrollOffset
    //4.执行 exec()
    query.select(".one").boundingClientRect(rect=>{
        console.log(rect)
    }).exec()
  },
  choosevideo(){
    wx.chooseVideo({
      sourceType: ['album','camera'],
      maxDuration: 60,
      camera: 'back',
      success:(res)=>{
        console.log(res);
        this.setData({
          videourl:res.tempFilePath
        })
      }
    })
  },
  choseimg(){
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success:(res)=>{
        console.log(res);

        this.setData({
          imgurl:res.tempFilePaths[0]
        })
        // tempFilePath可以作为img标签的src属性显示图片
        //const tempFilePaths = res.tempFilePaths
      }
    })
  },
  storage(){
    //Sync 同步缓存   async异步
    //设置缓存 setStorageSync 同步
    //设置缓存 setStorage 异步
    wx.setStorageSync('token', "110212939293203");
  },
  getstorage(){
    //获取缓存的数据
    console.log(wx.getStorageSync("token"));

    //删除某一个缓存  removeStorageSync(key)
    //清空所有缓存  clearStorageSync()

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     //一打开页面就loading 加载    灵力
      wx.showLoading({
        title: '加载中',
      })

      util.myRequest({
        url:"/api/xiaomi/swiper"
      }).then(res=>{
        console.log(res);
        //获取数据成功后取消 loading 加载
        if(res){
          wx.hideLoading();
        }
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})