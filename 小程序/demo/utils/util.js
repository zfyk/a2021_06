const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const myRequest=options=>{

   let baseURL="http://localhost:8088";

   //与  必须传递了options.method有参数，我才让他变成大写
   options.method=options.method&&options.method.toUpperCase()

   return new Promise((resolve,reject)=>{
      wx.request({
        url: baseURL+options.url, 
        method:options.method||"GET",
        data: options.data||{},
        header: options.header||{
          'content-type': 'application/json'
        },
        success (res) {
          resolve(res.data)
        },
        fail(err){
          reject(err);
        }
      })
   })
}

module.exports = {
  formatTime,
  myRequest
}
