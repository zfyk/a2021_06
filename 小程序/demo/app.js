// app.js
//应用生命周期---主入口的app.js（整个小程而言）
// onLaunch 初始化 只执行一次
// onShow 小程展示 （前后台切换）
// onHide 小程隐藏  （前后台切换）
// onError 小程序有错误的时候
// onPageNotFound  第一次渲染页面,找不到页面入口

//页面生命周期---每个页面的js里面
// onLoad  页面初始化 只执行一次
// onShow  页面展示
// onReady 页面渲染   只执行一次
// onHide  页面隐藏
// onUnload  页面销毁  （跳转页面，关闭当前页面，跳转到其他页面的时候触发（没有历史记录））

App({
  onLaunch() {
    console.log("小程序初始化，只执行一次");
    // // 展示本地存储能力
    // const logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    // // 登录
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
  },
  onShow(){
    console.log("小程序展示");
  },
  onHide(){
    console.log("小程序隐藏");
  },
  onError(error){
     console.log(error);
  },
  onPageNotFound(){
    console.log("第一次渲染页面找不到页面入口")
  },
  //所有页面的公共数据
  globalData: {
    userInfo: null,
    name:"王宝宝"
  }
})
