const source = axios.CancelToken.source();
const query = function query() {
    return axios.post('https://xxx.com/students', null, { cancelToken: source.token })
        .then(response => response.data);
};
const fetchStudents = function fetchStudents() {
    return new Promise(resolve => {
        // 创建6个工作区同时进行
        let works = new Array(6).fill(null),
            values = [],
            flag = false;
        works.forEach(() => {
            // 每个工作区都是获取学生信息；当此任务执行完（不论成功还是失败），继续去获取学生信息..
            const next = async () => {
                if (flag) return;
                // 当values存储的结果够100个后，就无需再发请求(取消正在发送的请求)
                if (values.length >= 100) {
                    resolve(values.slice(0, 100));
                    source.cancel();
                    flag = true;
                    return;
                }
                try {
                    let value = await query();
                    value = value.filter(item => {
                        return item.score >= 90 && (new Date(item.time) - new Date('2021-12-03')) > 0;
                    });
                    values = values.concat(value);
                } catch (_) { }
                next();
            };
            next();
        });
    });
};