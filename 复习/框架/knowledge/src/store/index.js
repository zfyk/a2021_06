import Vue from "vue";
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);
const env = process.env.NODE_ENV;
export default new Vuex.Store({
    state: {

    },
    mutations: {

    },
    actions: {

    },
    plugins: env === "production" ? [] : [createLogger()]
});