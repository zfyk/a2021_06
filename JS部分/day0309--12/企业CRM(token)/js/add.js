(function(){
	let customerId=location.href.queryParams("customerId")||0;
	let username=document.querySelector(".username");
	let useremail=document.querySelector(".useremail");
	let userphone=document.querySelector(".userphone");
	let userqq=document.querySelector(".userqq");
	let userweixin=document.querySelector(".userweixin");
	let usertype=document.querySelector(".usertype");
	let useraddress=document.querySelector(".address");
	let man=document.getElementById("man");
	let woman=document.getElementById("woman");
	let submit=document.querySelector(".submit");
	let usernameSpan=document.querySelector(".usernameSpan");
	
	async function render(){
		let result=await axios.get("/customer/info",{
			params:{customerId}
		})

		if(+result.code!==0){
			return;
		}
		let {id,name,sex,email,phone,QQ,weixin,type,address,userId,userName}=result.info;
		username.value=name;
		useremail.value=email;
		userphone.value=phone;
		sex==0? man.checked=true : woman.checked=true;
		userqq.value=QQ;
		userweixin.value=weixin;
		usertype.value=type;
		useraddress.value=address;
	}
	
	if(customerId){//编辑
		render();
	}else{//新增
		console.log("新增");
	}
	const validateName=function validateName(){
		let name=username.value.trim();
		let reg=/^[\u4e00-\u9fa5]{2,}(·[\u4e00-\u9fa5]{2,})?$/;
		if(name.length==0){
			usernameSpan.innerHTML="用户名不能为空！";
			return false;
		}
		if(reg.test(name)){
			usernameSpan.innerHTML="";
			return true;
		}else{
			usernameSpan.innerHTML="必须是中文姓名！";
			return false;
		}
	}
	submit.onclick= async function(){
		//自己验证格式
		if(!validateName()){
			return;
		}
		let obj={
			name:username.value.trim(),
			sex:man.checked?0:1,
			email:useremail.value.trim(),
			phone:userphone.value.trim(),
			QQ:userqq.value.trim(),
			weixin:userweixin.value.trim(),
			type:usertype.value.trim(),
			address:useraddress.value.trim()
		}
		if(customerId){//修改
		    obj["customerId"]=customerId;
			let result=await axios.post("/customer/update",obj)
			if(+result.code!==0){
				alert("修改失败！");
				return;
			}else{
				alert("修改成功！");
				location.href="customerlist.html";
			}
		}else{//新增
		    let resultadd=await axios.post("/customer/add",obj);
			if(+resultadd.code!==0){
				alert("无法新增！");
			}else{
				alert("新增成功！");
				location.href="customerlist.html";
			}
		}
	}
})()