(function(){
	let lx=location.href.queryParams("lx")||"my";
	let limit=10;
	let page=1;
	let selectBox=document.querySelector(".selectBox");
	let searchInp=document.querySelector(".searchInp");
	let tbody=document.querySelector("tbody");
	let pageBox=document.querySelector(".pageBox");
	
	const render=async function render(){
		let type=selectBox.value;
		let search=searchInp.value.trim();
		let obj={lx,limit,page,type,search}
		
		let {data,code,total,totalPage}=await axios.get("/customer/list",{
			params:obj
		});
		
		if(+code!=0){
			return;
		}
		
		let str="";
		data.forEach(item=>{
			let {
				id,
				name,
				sex,
				email,
				phone,
				QQ,
				weixin,
				type,
				address,
				userId,
				userName,
				departmentId,
				department
			}=item;
			str+=`<tr>
				<td class="w8">${name}</td>
				<td class="w5">${+sex===0?"男":"女"}</td>
				<td class="w10">${email}</td>
				<td class="w10">${phone}</td>
				<td class="w10">${weixin}</td>
				<td class="w10">${QQ}</td>
				<td class="w5">${type}</td>
				<td class="w8">${userName}</td>
				<td class="w20">${address}</td>
				<td class="w14">
					<a href="customeradd.html?customerId=${id}">编辑</a>
					<a href="javascript:;" data-id="${id}">删除</a>
					<a href="visit.html?customerId=${id}">回访记录</a>
				</td>
			</tr> `
		})
		tbody.innerHTML=str;
		
		let str2="";
		if(page>1){
			str2+=`<a href="javascript:;">上一页</a>`
		}
		if(totalPage>1){
			str2+=`<ul class="pageNum">`
			for(let i=1;i<=totalPage;i++){
				str2+=`<li class=${i==page?"active":""}>${i}</li>`
			}
			str2+=`</ul>`
		}
		if(page<totalPage){
			str2+=`<a href="javascript:;">下一页</a>`
		}
		pageBox.innerHTML=str2;
	}
	render()
	//翻页
	pageBox.onclick=function(e){
		//上一页
		if(e.target.tagName=="A"&&e.target.innerHTML=="上一页"){
			page--;
		}
		//下一页
		if(e.target.tagName=="A"&&e.target.innerHTML=="下一页"){
			page++;
		}
		if(e.target.tagName=="LI"){
			let newpage=+e.target.innerHTML;
			page=newpage;
		}
		render();
	}
	//下拉
	selectBox.onchange=function(e){
		page=1;
		render();
	}
	//搜索
	searchInp.onkeydown=function(e){
		if(e.keyCode==13){
			page=1;
			render();
		}
	}
	//删除
	tbody.onclick= async function(e){
		if(e.target.tagName=="A"&&e.target.innerHTML=="删除"){
		   let id=+e.target.getAttribute("data-id");
		   if(confirm(`你确定要删除${id}?`)){
			   let result=await axios.get("/customer/delete",{
				   params:{
					   customerId:id
				   }
			   })
			   if(+result.code===0){
			   	alert("恭喜你，删除成功！");
			   	render(page);
			   }else{
			   	alert("删除失败请重试！");
			   }
			   
		   }
		}
	}
})()