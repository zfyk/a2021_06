(function(){
	let userName=document.querySelector(".userName");
	let userPass=document.querySelector(".userPass");
	let submit=document.querySelector(".submit");
	let account,password;
	
	const validateName=function validateName(){
		let reg1=/^[\u4e00-\u9fa5]{2,}(·[\u4e00-\u9fa5]{2,})?$/;
		let reg2=/^1[3-9]\d{9}$/;
		let reg3=/^\w+((-\w+)|(\.\w+))*@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
		
		if(account.length===0){
			alert("用户名不能为空！");
			return false;
		}
		if(reg1.test(account)||reg2.test(account)||reg3.test(account)){
			return true;
		}else{
			alert("用户名格式错误！");
			return false;
		}
	}
	const validatepassword=function validatepassword(){
		let reg1=/^\w{6,16}$/;
		
		if(password.length===0){
			alert("密码不能为空！");
			return false;
		}
		if(reg1.test(password)){
			return true;
		}else{
			alert("密码格式错误！");
			return false;
		}
	}
	
	const login=async function login(){
		account=userName.value.trim();
		password=userPass.value.trim();
		if(!validateName()){
			return;
		}
		if(!validatepassword()){
			return;
		}
		let result=await axios.post("/user/login",{
			account,
			password:md5(password)
		})
		let {code,info,token}=result;
		if(code===0){
			//进入首页
			location.href="index.html";
			localStorage.setItem("info",JSON.stringify(info));
			localStorage.setItem("token",token);
		}else{
			alert("用户名或密码错误！");
		}
	}
	
	submit.onclick=login;
	userPass.onkeydown=function(e){
		if(e.keyCode===13){
			login();
		}
	}
})()