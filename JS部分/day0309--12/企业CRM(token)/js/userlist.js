(function() {
	let selectBox = document.querySelector(".selectBox");
	let searchInp = document.querySelector(".searchInp");
	let tbody = document.querySelector("tbody");
	let checkAll=document.getElementById("checkAll");
	let deleteAll=document.querySelector(".deleteAll");
	let departmentId, search;
	let newdata=[];
	const getData=async function getData(){ 
		departmentId = selectBox.value;
		search = searchInp.value.trim();
		let result = await axios.get("/user/list", {
			params: {
				departmentId,
				search
			}
		})
		if (+result.code !== 0) {
			return
		}
		let {data}=result;
		//处理数据，设置一个selected=false，默认全部都不选中
		newdata=data.map(item=>{
			item.selected=false;
			return item;
		})
		render();
	}
	getData();
	
	//获取表格数据，渲染表格
	const render = function render() {
		let str = "";
		newdata.forEach(item => {
			let {
				id,
				name,
				sex,
				email,
				phone,
				departmentId,
				department,
				jobId,
				job,
				desc,
				selected
			} = item;
			str +=
				`<tr>
				<td class="w3">
				   ${id==1?"":`<input type="checkbox" ${selected?"checked":""} data-id="${id}">`}
				</td>
				<td class="w10">${name}</td>
				<td class="w5">${sex==0?"男":"女"}</td>
				<td class="w10">${department}</td>
				<td class="w10">${job}</td>
				<td class="w15">${email}</td>
				<td class="w15">${phone}</td>
				<td class="w20">${desc}</td>
				<td class="w12">
					<a href="useradd.html?departmentId=${id}" >编辑</a>
					<a href="javascript:;" data-id="${id}">删除</a>
					<a href="javascript:;" data-id="${id}">重置密码</a>
				</td>
			</tr>`
		})
		tbody.innerHTML = str;

	}

	//渲染下拉列表
	const renderselect=async function renderselect(){
		//本地存储
		let data=sessionStorage.getItem("department");
		if(data){
			data=JSON.parse(data);
		}else{
			let result= await axios.get("/department/list");
			if(+result.code!==0){return}
			sessionStorage.setItem("department",JSON.stringify(result.data));
			data=result.data;
		}
		
		let str="";
		data.forEach(item=>{
			let {id,name}=item;
			str+=`<option value="${id}">${name}</option>`
		})
		selectBox.innerHTML+=str;
	}
	 renderselect();
	 
	//下拉筛选
	selectBox.onchange=function(){
		getData();
	} 
	
	//搜索
	searchInp.onkeydown=function(e){
		if(e.keyCode==13){
			getData();
		}
	}
	
	//全选和全不选
	checkAll.onclick=function(){
		//console.log(checkAll.checked);
		newdata.map(item=>{
			if(item.id==1){
				item.selected=false;
			}
			item.selected=checkAll.checked;
			return item;
		})
		render();
	}
	
	//事件委托
	tbody.onclick=async function(e){
		//单个元素的选中
		if(e.target.tagName=="INPUT"){
			let flag=e.target.checked;
			let aid=e.target.getAttribute("data-id");
		    newdata.map(item=>{
				if(aid==item.id){
					item.selected=flag;
				}
				return item;
			})
			
			//some:只要一个为真，结果真
			//every：只要一个为假，结果为假
			let bol=newdata.every(item=>{
				if(item.id==1){
					return true;
				}
				return item.selected==true;
			})
			checkAll.checked=bol;
			return;
		}
	   
	   //单个删除   
	   if(e.target.tagName=="A"&&e.target.innerHTML=="删除"){
		   let id=e.target.getAttribute("data-id");
		   if(confirm(`你确定要删除${id}吗？`)){
			   let result=await axios.get("/user/delete",{
				   params:{
					  userId:id
				   }
			   })
			   if(+result.code===0){
				   alert("恭喜你，删除成功");
				   getData();
			   }else{
				   alert("删除失败，请重试！");
			   }
			   return;
		   }
	   }
	   
	   //重置密码
	   if(e.target.tagName=="A"&&e.target.innerHTML=="重置密码"){
		   let resetid=e.target.getAttribute("data-id");
		   if(confirm(`用户${resetid},你确定重置密码吗？`)){
			   let resetResult=await axios.post("/user/resetpassword",{
				   userId:resetid
			   })
			   if(+resetResult.code===0){
				   alert("重置密码成功！");
				   getData();
			   }else{
				   alert("重置密码失败，请重试！");
			   }
		   }
		   return;
	   }
	}
	
    //批量删除
	deleteAll.onclick=function(){
		let arr=newdata.filter(item=>{
			return item.selected==true;
		})
		if(arr.length==0){
			alert("请先选中！");
			return;
		}
		if(confirm(`你确定要删除${arr.length}个吗？`)){
			del();
		}
		async function del(){
			//什么时候停止
			if(arr.length==0){
				alert("批量删除成功！");
				getData();
				return;
			}
			//怎么求每个元素的id, 数组顶部出栈
			let singleid=arr.shift().id;
			let result=await axios.get("/user/delete",{
				params:{
					userId:singleid
				}
			})
			if(+result.code!==0){
				alert("批量删除失败！")
				return;
			}
			del();//递归
		}
	}
})()
