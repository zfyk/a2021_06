/*
下⾯代码是否可以，每隔1000MS依次输出 0 1 2 3 4  ? 
如果不可以，说明为啥？以及如何解决？「三种⽅案处理」 
--------
不可以，因为：
  + 基于var声明变量并不会产生“块级私有上下文”，所用到的i都是全局上下文中的，包括定时器设定的回调函数中的i也是用全局上下文中的！
  + 定时器是异步编程，而循环是同步编程，只有等同步结束，才会执行异步的操作，此时全局上下文中的i已经是5了！

解决方案
  方案一：利用闭包的机制，每一轮循环，都产生一个闭包，分别存储后续需要的i的值「0~4」，这样定时器触发执行，回调函数中用的i，让其去访问对应闭包中的i即可！！
  方案二：利用定时第三个(及以后)参数，提前给回调函数预设需要的实参值！
*/
/* for (let i = 0; i < 5; i++) {
    setTimeout(function () {
        console.log(i);
    }, (i + 1) * 1000);
} */
/* for (var i = 0; i < 5; i++) {
    (function (i) {
        setTimeout(function () {
            console.log(i);
        }, (i + 1) * 1000);
    })(i);
} */

/* for (var i = 0; i < 5; i++) {
    setTimeout(function (i) {
        console.log(i);
    }, (i + 1) * 1000, i);
} */

//=====================
/* let a = 0,
    b = 0;
function A(a) {
    A = function (b) {
        alert(a + b++);
    };
    alert(a++);
}
A(1);
A(2); */

//====================
console.log(foo);
{
    console.log(foo);
    function foo() { }
    foo = 1;
    console.log(foo);
}
console.log(foo);