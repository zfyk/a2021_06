/*
 JS核心几部分
   1. 堆栈内存&闭包作用域「数据类型」
      + 数据类型
      + 数据类型检测
      + 数据类型转换
      + 堆栈内存、EC、VO、AO、GO...
      + 作用域&作用链{闭包}
      + ...
   2. 面向对象
      + 对象、类、实例
      + new操作符
      + prototype&__proto__
      + this
      + ...
   3. 同步异步编程「EventLoop」
      + 定时器
      + requestAnimationFrame
      + 事件循环机制
      + 浏览器渲染机制：DOM的回流重绘
      + Promise
      + 动画：CSS3动画、JS动画
      + ...
   4. DOM操作「核心:事件处理」
      + 事件和事件对象
      + 事件传播机制
      + 事件委托
      + 拖拽&放大镜
      + ...
   5. HTTP网络&以ajax为主的前后端数据通信
      + 网络和页面渲染的流程
      + Ajax的基础知识
      + Axios库的应用
      + 跨域解决方案
      + 前端性能优化方案
      + 综合实战:CRM客户管理系统
      + ...
   6. ES6+
      + let/const/class
      + 箭头函数
      + 解构赋值
      + 模板字符串
      + “...”展开、拓展、剩余运算符
      + 形参赋值默认值、对象键值对的快速赋值
      + Object/Array/String上提供的一些新的方法
      + Promise
      + async await
      + Reflect
      + 迭代器和生成器
      + ES6 Module规范
      + Fetch
      + IntersectionObserver
      + ...
  ------------推荐的学习方式
  @1 规律性、周期性总结和复习「巩固基础知识、让所有的知识在脑海中串联为知识图谱」
     例如：我每周四/周日，每天抽出两个小时，从头到尾的过一遍笔记
  @2 实战方面
     课堂案例：不要对照老师代码抄N遍，首先对照代码写一遍，写的时候把中文全写了；第二遍关掉老师代码，找中文一步步写代码（遇到不会的说明知识点不会）；第三遍...直到对照着中文自己可以流畅的写下来；把中文不看了，再写一遍进行巩固！！...过两天后再来一遍！！
     课外案例：也是先想思路，可以写成中文，一步步去实现代码！！
  @3 费曼学习法：给别人讲
 */

/* 
// parseFloat/parseInt([string])：传参必须是字符串(如不是则转换为字符串)，从字符串左侧第一个字符开始进行查找，把找到的有效数字字符转为数字(十进制数字)，遇到一个非有效数字字符则结束查找，如果一个有效数字字符都没找到，则返回NaN！
// NaN和自己以及任何值都不相等  NaN===NaN=>false   Object.is(NaN,NaN)=>true(内部做了特殊处理)
let num = parseFloat('width:100px');
if (num == 100) { alert(1); }
else if (num == NaN) { alert(2); }
else if (typeof num == 'number') { alert(3); }
else { alert(4); }
// 答案：“3”  alert会把所有值变为字符串再输出

console.log(parseInt(null)); //parseInt('null') -> NaN
console.log(Number(null)); //0 

============
parseInt VS parseFloat
  + parseFloat可以多识别一个小数点
  + parseInt支持第二个参数“[radix]进制”，parseFloat不支持

parseInt([string],[radix])
  + 在左侧字符串中，找到所有符合[radix]进制的数字字符（遇到一个非[radix]进制的，则结束查找），把找到的内容([radix]进制)转为十进制的数字！！
  + [radix]取值范围 2~36之间，如果设置的值不在这个范围内(排除0)，则直接返回NaN
  + 如果不设置或者设置为零：一般默认都是10「如果字符串是以0x开始，则默认是16」
*/
// console.log(parseInt('12px')); //12  -> parseInt('12px',10) -> '12'转为十进制数字 -> 12
// console.log(parseInt('120px', 2)); // -> '1' -> 1*2^0 -> 1
// console.log(parseInt('120px', 1)); //NaN

/* let arr = [27.2, 0, '0013', '14px', 123];
arr = arr.map(parseInt);
console.log(arr);
// parseInt(27.2,0) -> parseInt('27.2',10) -> '27' -> 27
// parseInt(0,1) -> NaN
// parseInt('0013',2) -> '001' -> 0*2^2+0*2^1+1*2^0 -> 1
// parseInt('14px',3) -> '1' -> 1*3^0 -> 1
// parseInt(123,4) -> parseInt('123',4) -> '123' -> 1*4^2+2*4^1+3*4^0 -> 16+8+3 -> 27
// [27,NaN,1,1,27] */



/*
 “+”在JS中，不仅仅有数学运算的能力，还有字符串拼接的作用
   数学运算：会存在把其它数据类型值“隐式”转换为数字 Number([val])
     + 字符串转数字：空字符串变为零；只要出现非有效数字字符，结果都是NaN；
     + 布尔转数字：true->1 false->0
     + null->0  undefined->NaN
     + symbol不能转换为数字
     + bigint类型可以转换为数字「去除末尾n、以科学计数法表示」
     + 把对象转换为数字
       + 首先检测对象的Symbol.toPrimitive属性，如果存在(属性值是个函数)，把函数执行的结果就是我们转换的值；如果没有这个属性则继续...
       + 其次再调用对象的valueOf这个方法，看返回的结果是否为原始值，如果是则获取的就是转换结果；如果不是，则继续执行后面的步骤...
       + 然后调用对象的toString方法，把其转换为字符串
       + 最后把字符串转换为数字
   字符串拼接：如果“+”左右两边有一边出现 字符串/部分对象 则按照字符串拼接处理
 */
// let result = 100 + true + 21.2 + null + undefined + "Tencent" + [] + null + 9 + false; 
// // 'NaNTencentnull9false'
// console.log(result);


/*
 “==”进行比较，两边类型不一致，浏览器会隐式转换为一致的类型，再进行比较 
   1. 对象==对象  比较的是堆内存地址
   2. NaN!==NaN 
   3. 两个等号下null和undefined相当，在三个等号下是不想当的，而且他们和任何其他值都不相等
      if(a==null) { a是null/undefined }
   4. 对象==字符串  对象转字符串
   5. 剩余情况都要转换为数字
 */
// 方案一：利用“==”比较会存在数据类型转换、而把对象转数字要经历四个步骤，此时我们只需要重写其中的某个步骤，让其每次转换的返回值分别是1/2/3，就可以实现对应的效果了！！
/* var a = {
    i: 0,
    [Symbol.toPrimitive]() {
        // this->a
        return ++this.i;
    }
};
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
} */
/* 
var a = [1, 2, 3];
a.toString = a.shift;
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
} 
*/

/* 
// 方案二：利用Object.defineProperty对window对象中的a属性做获取劫持
var i = 0;
Object.defineProperty(window, 'a', {
    get() {
        return ++i;
    }
});
if (a == 1 && a == 2 && a == 3) {
    console.log('OK');
} 
*/


/* var a = { n: 1 };
var b = a;
a.x = a = { n: 2 };
console.log(a.x);
console.log(b); */