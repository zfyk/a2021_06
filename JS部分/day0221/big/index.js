let mark=document.getElementById("mark");
let leftbox=document.getElementById("boxleft");
let rightbox=document.getElementById("boxright");
let rightimg=document.querySelector("#boxright img");

//console.log(mark.offsetWidth);//如果元素 display:none; 求不到高宽
function getCss(ele,attr){//获取隐藏盒子的高度宽度的方法
	return parseInt(window.getComputedStyle(ele)[attr]);
}

let markW=getCss(mark,"width");//遮罩的宽度
let markH=getCss(mark,"height");//遮罩的高度

let rightW=getCss(rightbox,"width");//右盒子的宽度
let rightH=getCss(rightbox,"height");//右盒子的高度

rightimg.style.width=(leftbox.offsetWidth/markW*rightW)+"px";//大图片宽度
rightimg.style.height=(leftbox.offsetHeight/markH*rightH)+"px";//大图片的高度

function computed(e){
	let maxW=leftbox.offsetWidth-markW;//最大宽度
	let maxH=leftbox.offsetHeight-markH;//最大高度
	
	let px=e.pageX;//鼠标进入左侧小盒子，page的坐标
	let py=e.pageY;
	
	let l=leftbox.offsetLeft;//左侧小盒子距离左边的距离
	let t=leftbox.offsetTop;//左侧小盒子距离上边的距离
	
	let valuex=px-l-(markW/2);//mark的left距离
	let valuey=py-t-(markH/2);//mark的top距离
	
	valuex=valuex<0?0:(valuex>maxW?maxW:valuex);//left最大范围值
	valuey=valuey<0?0:(valuey>maxH?maxH:valuey);//top 最大范围值
	
	mark.style.left=valuex+"px";
	mark.style.top=valuey+"px";
	
	rightimg.style.left=`-${rightW/markW*valuex}px`;
	rightimg.style.top=`-${rightH/markH*valuey}px`;
	
}

//鼠标移入左侧小盒子， 遮罩层显示，右侧盒子显示
leftbox.onmouseenter=function(e){
	mark.style.display="block";
	rightbox.style.display="block";
	computed(e);
}
//鼠标移出左侧小盒子， 遮罩层隐藏，右侧盒子隐藏
leftbox.onmouseleave=function(){
	mark.style.display="none";
	rightbox.style.display="none";
}
//鼠标移动，坐标修改
leftbox.onmousemove=computed;