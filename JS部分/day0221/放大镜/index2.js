let leftbox=document.querySelector(".leftbox");
let rightbox=document.querySelector(".rightbox");
let mark=document.querySelector(".mark");
let bigimg=document.getElementById("bigimg");

function getCss(ele,attr){
	return parseInt(window.getComputedStyle(ele)[attr]);
}

let mw=getCss(mark,"width");
let mh=getCss(mark,"height");
let rightboxh=getCss(rightbox,"height");
let rightboxw=getCss(rightbox,"width");

bigimg.style.height=(leftbox.offsetHeight/mh*rightboxh)+"px";
bigimg.style.width=(leftbox.offsetWidth/mw*rightboxw)+"px";

function computed(e){
	let maxW=leftbox.offsetWidth-mw;
	let maxH=leftbox.offsetHeight-mh;
	
	let px=e.pageX;
	let py=e.pageY;
	
	let l=leftbox.offsetLeft;
	let t=leftbox.offsetTop;
	
	let valuex=px-l-(mw/2);
	let valuey=py-t-(mh/2);
	
	valuex=valuex<0?0:(valuex>maxW?maxW:valuex);
	valuey=valuey<0?0:(valuey>maxH?maxH:valuey);
	
	mark.style.left=valuex+"px";
	mark.style.top=valuey+"px";
	
	bigimg.style.left=`-${valuex*(rightboxw/mw)}px`;
	bigimg.style.top=`-${valuey*(rightboxh/mh)}px`;
}
//移入 遮罩，右边盒子显示
leftbox.onmouseenter=function(e){
	mark.style.display="block";
	rightbox.style.display="block";
	computed(e);
}
//移出 遮罩，右边盒子隐藏
leftbox.onmouseleave=function(){
	mark.style.display="none";
	rightbox.style.display="none";
}
//鼠标进入左侧小盒子移动
leftbox.onmousemove=computed;