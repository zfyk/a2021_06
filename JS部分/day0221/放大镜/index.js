let mark=document.querySelector(".mark");
let rightbox=document.querySelector(".right");
let leftbox=document.querySelector(".left");
let rightimg=document.querySelector(".right img");

let maxW=leftbox.offsetWidth-100;
let maxH=leftbox.offsetHeight-100;

function computed(e){
	let startx=e.clientX;
	let starty=e.clientY;
	
	let markw=mark.offsetWidth;
	let markh=mark.offsetHeight;
	
	let l=leftbox.offsetLeft;
	let t=leftbox.offsetTop;
	
	let valuex=startx-l-(markw/2);
	let valuey=starty-t-(markh/2);
	
	valuex=valuex<0?0:(valuex>maxW?maxW:valuex);
	valuey=valuey<0?0:(valuey>maxH?maxH:valuey);

	mark.style.left=valuex+"px";
	mark.style.top=valuey+"px";
	
	rightimg.style.left=`-${valuex*4}px`;
	rightimg.style.top=`-${valuey*4}px`;
}

leftbox.onmouseenter=function(e){
	mark.style.display="block";
	rightbox.style.display="block";
}
leftbox.onmousemove=computed;

leftbox.onmouseleave=function(){
	mark.style.display="none";
	rightbox.style.display="none";
}