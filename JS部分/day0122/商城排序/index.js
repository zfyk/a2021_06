let product = (function() {
	let data = null;
	//2.1.获取con元素
	let con = document.getElementById("con");
	//3.1 找到三个li
	let lilist=document.querySelectorAll("#lilist li");
	//1.准备数据
	function getData() {
		//1.创建XMLHttpRequest核心对象
		let xhr = new XMLHttpRequest();
		//2.建立连接
		xhr.open("GET", "product.json", false);
		//3.注册监听
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4 && xhr.status === 200) {
				//console.log(xhr.responseText);
				data = JSON.parse(xhr.responseText);
				//console.log(data);
			}
		}
		//4.发送数据
		xhr.send(null);
	}

	//2.循环渲染
	function render() {
		// console.log(con);
		// console.log(data);
		let str="";
		//3.循环
		data.forEach((item,index)=>{
			//str+=index;//""+0="0"  //"0"+1="01"  //"01"+2="012" //...
			//console.log(item);
			let {id,hot,img,title,time,price}=item;
			str+=`<div class="col-md-3 col-sm-6" id="${id}">
					<div class="thumbnail">
						<img src="${img}">
						<div class="caption">
							<h3>${title}</h3>
							<p>价格: ￥${price}</p>
							<p>时间: ${time}</p>
							<p>热度: ${hot}</p>
						</div>
					</div>
				</div>`
		})
		//2.2通过innerHTML给con添加内容
		con.innerHTML=str;
	}
	function clear(n){
		//console.log(n);
		lilist.forEach((item)=>{
			if(item!=n){
				item.flag=-1;
			}
		})
	}
    //3.功能部分
    function handle(){
		//把一个函数当做另一个函数的参数,回调函数
		
		//console.log(lilist);
		lilist.forEach((item,index)=>{
			item.flag=-1;
			//给每个li元素绑定一个点击事件
			item.onclick=function(){
				clear(item);//除点击的元素外，flag恢复初始
				this.flag*=-1;
				let title=this.getAttribute("data-title");
				console.log(title);
				//数据排序
				data.sort((a,b)=>{
					if(title=="time"){
						a=new Date(a.time)*1;
						b=new Date(b.time)*1;
						return (a-b)*this.flag;
					}
					return (a[title]-b[title])*this.flag;
				})
				render();
			}
		})
	}
    
	return {
		init() {
			getData();
			render();
			handle();
		}
	}
})()
product.init();
