let product = (function() {
	let data = null;
	//2.1.获取con元素
	let con = document.getElementById("con");
	//3.1 获取三个 li
	let lilist=document.querySelectorAll("#lilist li");
	//1.准备数据
	function getData() {
		//1.创建XMLHttpRequest核心对象
		let xhr = new XMLHttpRequest();
		//2.建立连接
		xhr.open("GET", "product.json", false);
		//3.注册监听
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4 && xhr.status === 200) {
				//console.log(xhr.responseText);
				data = JSON.parse(xhr.responseText);
				//console.log(data);
			}
		}
		//4.发送数据
		xhr.send(null);
	}

	//2.循环渲染
	function render() {
		// console.log(con);
		// console.log(data);
		let str="";
		//3.循环
		data.forEach((item,index)=>{
			//str+=index;//""+0="0"  //"0"+1="01"  //"01"+2="012" //...
			//console.log(item);
			let {id,hot,img,title,time,price}=item;
			str+=`<div class="col-md-3 col-sm-6" id="${id}">
					<div class="thumbnail">
						<img src="${img}">
						<div class="caption">
							<h3>${title}</h3>
							<p>价格: ￥${price}</p>
							<p>时间: ${time}</p>
							<p>热度: ${hot}</p>
						</div>
					</div>
				</div>`
		})
		//2.2通过innerHTML给con添加内容
		con.innerHTML=str;
	}
    //3.功能部分
    function handle(){
		//console.log(lilist);
		//3.2 添加点击事件
		let flag=-1;
		lilist[0].onclick=function(){
			flag*=-1;//1  1*-1===>-1  1
			//点击---》修改数据data--->点击价格---》价格排序
			data.sort(function(a,b){
				return (b.price-a.price)*flag;//(a.p-b.p)
			})
			render();
		}
		
		let flag2=-1;
		lilist[1].onclick=function(){
			flag2*=-1;//1  1*-1===>-1  1
			//点击---》修改数据data--->点击价格---》价格排序
			data.sort(function(a,b){
				a=new Date(a.time).getTime();
				b=new Date(b.time).getTime();
				return (b-a)*flag2;//(a.p-b.p)
			})
			render();
		}
	}
    
	return {
		init() {
			getData();
			render();
			handle();
		}
	}
})()
product.init();
