$(function(){
    //返回首页
    $(".icon-fanhui").click(function(){
        location.href="../index.html";
    })
    //显示弹框
    $("footer i:not(.icon-fanhui)").click(function(){
        $(".mask").show();
    })
    //隐藏弹窗
    $(".mask").click(function(){
        $(".mask").hide();
    })

    let aid=location.search.split("=")[1];
    //获取数据
    $.get("http://localhost:8888/api/article?id="+aid,function(data){
       console.log(data);
       render(data);
    })
    //渲染页面
    function render(data){
      $("header img").attr("src",data.img);
      $(".title").html(data.title);
      $(".author").html(data.author);

      let dataarr=data.content;
      let str="";
      dataarr.forEach((item)=>{
          if(item.type=="text"){
             str+=`<p>${item.text}</p>`
          }else{
            str+=`<img src="${item.img}" alt="${item.des}" style="width:100%">`
          }
      })
      $(".topcon").html(str);
    }

})