$(function(){
    let i=0,str="",gdate=null,dateobj=null;
    //日期添加0
    function addZero(n){
       return n>9?n:"0"+n;
    }
    //头部时间处理
    function changeheader(){
       let date=new Date();
       let day=date.getDate();//今天日期  1--->01
       let month=date.getMonth();//今天的月份 6
       let hour=date.getHours();
       let arrmonth=["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"];
       $(".h_left span").text(addZero(day));
       $(".h_left i").html(arrmonth[month]);//查找数组中的月份值
       if(hour>=12){
           $(".h_left h2").html("知乎日报");
       }else{
           $(".h_left h2").html("早上好");
       }
    }
    changeheader();

    //获取轮播图数据
    $.get("http://localhost:8888/api/swiper",function(data){
        //console.log(data);
        renderlun(data);
    })
    //渲染轮播图
    function renderlun(data){
       //console.log(data);
       let str='';
       data.forEach((item)=>{
           let{author,title,img}=item;
           str+=`<div class="swiper-slide">
                    <img src="${img}" alt="">
                    <div class="text">
                        <h2>${title}</h2>
                        <p>${author}</p>
                    </div>
                </div>`;
       })
       $(".swiper-wrapper").html(str);
       lunactive();//启动轮播图
    }
    //启动轮播图
    function lunactive(){
        var mySwiper = new Swiper ('.swiper-container', {
            loop: true, // 循环模式选项
            autoplay: {
                delay: 1000,
                stopOnLastSlide: false,
                disableOnInteraction: true,
             },
            // 如果需要分页器
            pagination: {
              el: '.swiper-pagination',
            },
      
          })   
    }

   //获取列表数据
   function getlistdata(){
       if(i>5){
           $(".loadmore").html("全部加载完成");
           return;
       }
        let date=new Date();
       let year=date.getFullYear();
       let month=date.getMonth()+1;//6
       let day=date.getDate();

       let dateValue=`${year}-${addZero(month)}-${addZero(day)}`;
      // console.log(dateValue);
       //console.log(date.toLocaleDateString().replace(/\//g,"-"));
       $.get(`http://localhost:8888/api/articleList?date=${dateValue}`,function(data){
            //console.log(data);
            renderlist(data);
       })
   }
   getlistdata();
   //处理一下日期
   function changeDate(){
      gdate=new Date().getTime()-(24*60*60*1000*i);
      dateobj={
          month:new Date(gdate).getMonth()+1,
          day:new Date(gdate).getDate()
      }
      i++;
	  console.log(i);
   }
   //渲染列表
   function renderlist(data){
        changeDate();
        if(i>1){
            str+=`<p><span>${dateobj.month}月${addZero(dateobj.day)}日</span><strong></strong></p>`
        }
      data.forEach((item)=>{
          let {author,des,title,id,img} = item;
          str+=` <div class="item" data-id=${id}>
                    <div class="item_left">
                        <h2>${title}</h2>
                        <p>${author}·${des}分钟阅读</p>
                    </div>
                    <img src="${img}" alt="">
                </div>`
      })
      $(".list").html(str);
      loadmore();
   }
   //加载更多
   function loadmore(){
       let ob=new IntersectionObserver((changes)=>{
           if(changes[0].isIntersecting){
                //console.log("加载更多");
                getlistdata();
           }
       })
       ob.observe($(".loadmore")[0]);
   }

   //跳转到登录页
   $(".h_right").click(function(){
       location.href="pages/login.html";
   })
   //跳转到详情页---事件委托
   $(".list").delegate(".item","click",function(){
       //console.log(this);
       let id=$(this).attr("data-id");
       location.href="pages/article.html?id="+id;
   })
})