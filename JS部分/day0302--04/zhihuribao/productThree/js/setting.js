$(function(){
    let url="login.html";
    
    if(location.search.includes("setdark")){
        $(".box").addClass("dark");
        url="login.html?id=dark";
        $("#ye").prop("checked",true);
    }else{
        $(".box").removeClass("dark");
        url="login.html";
        $("#ye").prop("checked",false);
    }
    //夜间模式
    $("#ye").change(function(){
        if($(".box").hasClass("dark")){
            $(".box").removeClass("dark");
            url="login.html";
        }else{
            $(".box").addClass("dark");
            url="login.html?id=dark"
        }
    })
     //点击按钮返回登录
     $(".icon-fanhui").click(function(){
        location.href=url;
    })
})