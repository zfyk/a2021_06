$(function(){
    //点击返回按钮--->首页
    $(".icon-fanhui").click(function(){
        location.href="../index.html";
    })

    //渲染页面
     let query=location.search;//?id=9736137
    // let index=query.indexOf("=");//= -->3  3+1
    // let aid=query.slice(index+1);
     //console.log(aid)

    //  let aid=/([^=]+)=(\d+)/g.exec(query)[2];
    //  console.log(aid);
    
    
    //1.获取数据
    // $.get(`http://localhost:8888/api/article?id=${aid}`,function(data){
    //     console.log(data);
    // })

    $.get(`http://localhost:8888/api/article${query}`,function(data){
        //console.log(data);
        render(data);
    }) 

    function render(data){
       let {author,title,img,content}=data;
       $(".top h1").html(title);
       $(".top img").attr("src",img);
       $(".title span").html(author);
       
       let str="";
       content.forEach((item)=>{
           if(item.type=="text"){
              str+=`<p class="context">${item.text}</p>`
           }else{
               str+=`<p class="conimg">
                    <img src="${item.img}" alt="">
                    <span>${item.des}</span>
                </p>`
           }
       })
       $(".con").html(str);
    }


    //显示遮罩层
    $(".footer i:not(.icon-fanhui)").click(function(){
        $(".mask").show();
    })
    //点击遮罩隐藏
    $(".mask").click(function(){
        $(".mask").hide();
    })

})