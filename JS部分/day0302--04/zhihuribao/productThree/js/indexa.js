$(function() {
	//日期补零
	function addZero(n) {
		return n > 9 ? n : "0" + n;
	}
	//处理导航栏的时间
	function changeTime() {
		let date = new Date();
		let day = addZero(date.getDate());
		let month = date.getMonth();
		let arrmonth = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];

		$(".hleft strong").html(day);
		$(".hleft span").html(arrmonth[month]);

		let hour = date.getHours();
		if (hour > 12) {
			$(".hleft h3").html("知乎日报");
		} else {
			$(".hleft h3").html("早上好好");
		}
	}
	changeTime();

	//轮播图
	//1.获取轮播图数据
	$.get("http://localhost:8888/api/swiper", function(data) {
		//console.log(data);
		lunrender(data);
	}) //异步

	//2.循环渲染
	function lunrender(data) {
		let str = "";
		data.forEach((item, index) => {
			let {
				author,
				id,
				img,
				title
			} = item;
			str +=
				`<div class="swiper-slide" id="${id}">
							<img src="${img}" alt="">
						    <div class="title">
                                <h1>${title}</h1>
								<p>${author}</p>
							</div>
						</div>`
		})
		$(".swiper-wrapper").html(str);
		lunstart();
	}
	//启动轮播图-----》数据渲染完后再启动
	function lunstart() {
		var swiper = new Swiper('.swiper-container', {
			autoplay: {
				delay: 1000,
				stopOnLastSlide: false,
				disableOnInteraction: true,
			},
			loop: true,
			pagination: {
				el: '.swiper-pagination',
			},
		});
	}

	//列表
	//1.获取数据
	let i = 0,count=0;

	function getlistdata() {
		let date = new Date(new Date() - i * 24 * 60 * 60 * 1000);
		let year = date.getFullYear();
		let month = addZero(date.getMonth() + 1);
		let day = addZero(date.getDate());
		let dateValue = `${year}-${month}-${day}`;
		//console.log(dateValue);
		$.get(`http://localhost:8888/api/articleList?date=${dateValue}`, function(data) {
			//console.log(data);
			listrender(data);
		})
	}
	getlistdata();

	//列表渲染
	function listrender(data) {
		//console.log(data);
		let str = `<div id="everylist">`;
		if(i>0){
			let date=new Date(new Date()-i*24*60*60*1000);
			let Month=addZero(date.getMonth()+1);
			let day=addZero(date.getDate());
			str+=`<p class="time">${Month}月${day}日 <span></span></p>`;
		}			
		str+=`<ul id="ullist">`;

		data.forEach((item, index) => {
			let {id,author,des,title,img}=item;
			str +=
				`<li id=${id}>
					<div class="leftlist">
						<h2>${title}</h2>
						<p>${author}·${des}分钟阅读</p>
					</div>
					<img src="${img}" alt="">
				</li>`
		})

		str += `</ul>
		</div>`;
		$(".list")[0].innerHTML+=str;
		i++;
		loadmore();
	}
   
   //加载更多
   function loadmore(){
	   count++;
	   let ob=new IntersectionObserver((changes)=>{
		   
		   if(changes[0].isIntersecting){
			   if(count>5){
				   ob.unobserve($(".loadmore")[0]);
				   $(".loadmore").html("已经加载完毕");
				   return;
			   }
			  // console.log("加载更多");
			   getlistdata();
		   }
	   })
	   ob.observe($(".loadmore")[0]);
   }

   //点击进入详情页--->不行 li渲染---->事件委托
    $(".list").delegate("li","click",function(){
		//console.log(this);//DOM对象 点击li
		let aid=$(this).attr("id");
		//console.log(aid);
		location.href=`pages/detials.html?id=${aid}`;
	})
	//点击跳转到登录页面
	$(".hright").click(function(){
		location.href="pages/login.html";
	})
})
