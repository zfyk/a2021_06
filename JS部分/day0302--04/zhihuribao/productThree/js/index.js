$(function(){
    function addZero(n){
      return n<10?"0"+n:n;
    }
    //处理导航栏日期
    function changeDate(){
      let date=new Date();
      let montharr=['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月', '十二月'];
      let month=date.getMonth();
      let day=date.getDate();
      let hour=date.getHours();
 
      $(".hleft strong").html(addZero(day));
      $(".hleft span").html(montharr[month]);
      if(hour>=12){
         $(".hleft h3").html("知乎日报");
      }else{
        $(".hleft h3").html("早上好！");
      }
    }
    changeDate()

    //轮播图
    //1.获取轮播图数据
    $.get("http://localhost:8888/api/swiper",function(data){
        lunrender(data);
    })
    //2.循环渲染
    function lunrender(data){
       let str="";
       data.forEach((item,index)=>{
           let {author,id,img,title}=item;
            str+=`<div class="swiper-slide" id=${id}>
                    <img src="${img}" alt="">
                    <div class="title">
                        <h1>${title}</h1>
                        <p>${author}</p>
                    </div>
                </div>`
       })
      $(".swiper-wrapper").html(str);
      startlun();
    }
    //3.渲染完后，启动轮播图
    function startlun(){
        var swiper = new Swiper('.swiper-container', {
            autoplay: {
                delay: 1000,
                stopOnLastSlide: false,
                disableOnInteraction: true,
            },
            loop:true,
            pagination: {
            el: '.swiper-pagination',
            },
      });
    }

    //列表
    //1.获取数据
    let count=0,i=0;
    function getlistdata(){
        let date=new Date();
        let year=date.getFullYear();
        let month=addZero(date.getMonth()+1);
        let day=addZero(date.getDate());
        let dateValue=`${year}-${month}-${day}`;
        $.get("http://localhost:8888/api/articleList?date=${dateValue}",function(data){
            //console.log(data);
            listrender(data);
        })
    }
    getlistdata();
    //2.渲染列表
    function listrender(data){
      let str=`<div id="everylist">`;
      if(i>0){
          let date=new Date()-i*24*60*60*1000;
          let newdate=new Date(date);
          let newmonth=addZero(newdate.getMonth()+1);
          let newday=addZero(newdate.getDate());
          str+=`<p class="time">${newmonth}月${newday}日 <span></span></p>`
      }
      str+=`<ul>`;
      data.forEach((item,index)=>{
          let{author,id,des,img,title}=item;
          str+=`<li>
                <div class="leftlist" id="${id}">
                    <h2>${title}</h2>
                    <p>${author}·${des}分钟阅读</p>
                </div>
                <img src="${img}" alt="">
            </li>`
      })
      str+="</ul></div>";
      //console.log(str);
      $(".list")[0].innerHTML+=str;
      i++;
      loadmore();
    }
    //加载更多
    function loadmore(){
        count++;
        let ob=new IntersectionObserver((changes)=>{
            if(changes[0].isIntersecting){
                if(count>5){
                    ob.unobserve($(".loadmore")[0]);
                    $(".loadmore").html("加载完毕");
                    return;
                }
                getlistdata();
            }
        })
        ob.observe($(".loadmore")[0]);
    }
    //
})