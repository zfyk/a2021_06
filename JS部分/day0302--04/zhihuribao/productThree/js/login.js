$(function(){
    let seturl="setting.html";
    if(location.search.includes("dark")){
        $(".box").addClass("dark");
        seturl="setting.html?setid=setdark";
    }else{
        $(".box").removeClass("dark");
        seturl="setting.html";
    }
    //返回首页
    $(".icon-fanhui").click(function(){
        location.href="../index.html";
    })
    //点击知乎
    $("#zhihu").click(function(){
        if($("#agree").prop("checked")){
            alert("点击了知乎");
        }else{
            alert("请先阅读条款");
        }
    })
    
    //点击微博
    $(".icon-weibo").click(function(){
        if($("#agree").prop("checked")){
            alert("点击了微博");
        }else{
            alert("请先阅读条款");
        }
    })

    //夜间模式
    $("#yejian").click(function(){
        if($(".box").hasClass("dark")){
            $(".box").removeClass("dark");
            seturl="setting.html";
        }else{
            $(".box").addClass("dark");
            seturl="setting.html?setid=setdark";
        }
    })

    //跳转到设置页面
    $("#shezhi").click(function(){
        location.href=seturl;
    })
})