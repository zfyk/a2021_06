$(function(){
    let url="http://localhost:8888";
    let strlist="",i=0,beforedate=null;
    //日期加0
    function addZero(n){
      return n>9?n:"0"+n;
    }
    //头部渲染
    function changeheader(){
       let date=new Date();
       let month=date.getMonth();
       let day=date.getDate();
       let hours=date.getHours()>=12?"今日头条":"早上好";
       let arrmonth= ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月', '十二月'];
       $("#day").html(addZero(day));
       $("#month").html(arrmonth[month]);
       $("#des").html(hours);
    }
    //获取轮播图数据
    $.get(url+"/api/swiper",function(data){
      //console.log(data);
      renderimg(data);
    }) 
    //渲染轮播图
    function renderimg(data){
        let str="";
        data.forEach((item)=>{
            let {author,title,img,id}=item;
          str+=` <div class="swiper-slide" data-id="${id}">
                    <img src="${img}" alt="">
                    <div class="info">
                        <h1>${title}</h1>
                        <p>${author}</p>
                    </div>
                </div>`
        })
       $(".swiper-wrapper").html(str);
       active();
    }
    //启动轮播图
    function active(){
        var swiper = new Swiper('.swiper-container', {
            autoplay: {
                delay: 1000,
                stopOnLastSlide: false,
                disableOnInteraction: true,
            },
            loop:true,
            pagination: {
              el: '.swiper-pagination',
            },
          });
    }
    //获取列表数据
    function getlistData(){
        if(i>5){
            return;
        }
        let date=new Date();
        let textdate=`${date.getFullYear()}-${addZero(date.getMonth()+1)}-${addZero(date.getDate())}`;
        $.get(url+"/api/articleList?date=${textdate}",function(data){
            //console.log(data);
            renderlist(data);
        });
    }
    //设置日期是否显示
    function setdate(){
        let date=new Date().getTime()-(24*60*60*1000)*i;
        beforedate={
            month:(new Date(date).getMonth())+1,
            day:addZero(new Date(date).getDate())
        }
        i++;
    }
    //渲染列表数据
    function renderlist(data){
        setdate();
        if(i>1){
            strlist+=`<p class="listhr">${beforedate.month}月${beforedate.day}日<b></b></p>`;
        }
        data.forEach((item)=>{
            let {author,title,img,id,des}=item;
            strlist+=`<div class="item" data-id="${id}">
                      <div class="item_left">
                          <h3>${title}</h3>
                          <p>${author}·${des}分钟阅读</p>
                      </div>
                      <img src="${img}" alt="">
                  </div>`
        })
        $(".list").html(strlist);
        loadmore();
    }
    //加载更多
    function loadmore(){
       let ob=new IntersectionObserver((changes)=>{
         if(changes[0].isIntersecting){
            getlistData();
         }
       })
       ob.observe($(".loadMore")[0])
    }
    //跳转登录页
    $("#gologin").click(function(){
        location.href="pages/login.html";
    })
    //跳转详情页
    $(".list").delegate(".item","click",function(){
        let aid=$(this).attr("data-id");
        location.href="pages/article.html?id="+aid;
    });
    changeheader();
    getlistData(); 
})