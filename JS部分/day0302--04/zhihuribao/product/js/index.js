let indexProduct=(function(){
    let baseurl="http://localhost:8888";
    let i = 0, //存储现在文章显示的是几天前
    date = null;
    let str='';
    function dealDate() {
        // time是i天前的毫秒数
        let time = new Date().getTime() - 1000 * 60 * 60 * 24 * i;
        date = {
            year: new Date(time).getFullYear(),
            month: new Date(time).getMonth()+1,
            day: new Date(time).getDate(),
        };
        i++;
    }
    //日期补零
    function addZero(n){
        return n>9?n:"0"+n;
     }
   //渲染头部数据
   function renderHead(){
       //let date=new Date("2020/1/1 06:00:00");
       let date=new Date();
       let month=date.getMonth();
       let day=date.getDate()>9?date.getDate():"0"+date.getDate();
       let timedes=date.getHours()>=12?"今日头条":"早上好";
       let months = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月', '十二月'];
        $("#day").html(day);
        $("#month").html(months[month]);
        $("#des").html(timedes);
    }
    //轮播图数据获取
    function getlunData(){
       $.get(baseurl+"/api/swiper",function(data){
         // console.log(data);
           randerlun(data);
       })
    }
    //渲染轮播图
    function randerlun(data){
      let str="";
      data.forEach((item)=>{
          let {author,title,img,id}=item;
          str+=` <div class="swiper-slide" data-id="${id}">
                    <img src="${img}" alt="">
                    <div class="info">
                        <h1>${title}</h1>
                        <p>${author}</p>
                    </div>
                </div>`
      })
      $(".swiper-wrapper").html(str);
      activelun();
    }
    //轮播图动画
    function activelun(){
        var swiper = new Swiper('.swiper-container', {
            autoplay: {
                delay: 1000,
                stopOnLastSlide: false,
                disableOnInteraction: true,
            },
            loop:true,
            pagination: {
              el: '.swiper-pagination',
            },
          });
    }
    //获取列表数据
    function getlistData(){
        if(i>=5){
            return;
        }
        let date=new Date();
        let textdate=`${date.getFullYear()}-${addZero(date.getMonth()+1)}-${addZero(date.getDate())}`;
        $.get(baseurl+"/api/articleList?date="+textdate,function(data){
            //console.log(data);
            renderlist(data);
        })
    }
    //渲染列表
    function renderlist(data){
      dealDate();
      if(i>1){
        str+=` <p class="listhr">${date.month}月${date.day}日<b></b></p>`
      }
      data.forEach((item)=>{
        let {author,title,img,id,des}=item;
          str+=`<div class="item" data-id="${id}">
                    <div class="item_left">
                        <h3>${title}</h3>
                        <p>${author}·${des}分钟阅读</p>
                    </div>
                    <img src="${img}" alt="">
                </div>`
      })
      $(".list").html(str);
      i===1&&loadmore();
    }
    //列表加载更多
    function loadmore(){
        let ob=new IntersectionObserver((changes)=>{
            if(changes[0].isIntersecting){
                getlistData();
            }
        })
        ob.observe($(".loadMore")[0]);
    }
   //点击跳转页面
   function go(){
       $("#gologin").click(function(){
           location.href="pages/login.html";
       })
       //jQuery 事件委托
       $(".list").delegate(".item","click",function(){
           let aid=$(this).attr("data-id");
           location.href="pages/article.html?id="+aid;
       });

   }
   return{
      init(){
        renderHead();
        getlunData();
        getlistData();
        go();
      }
   }
})()
indexProduct.init()

