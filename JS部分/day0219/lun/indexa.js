(function(){
	let bannerBox=document.getElementById("bannerBox");
	let wrapper=document.querySelector(".wrapper");
	let spanlist=null;
	let pagination=document.querySelector(".pagination");
	let w=bannerBox.offsetWidth;//每个图片宽
	let step=0;
	let timer=null;
	let count=4;
	
	//获取数据 渲染页面
	function render(){
		//获取数据
		let data=null;
		let xhr=new XMLHttpRequest;
		xhr.open("GET","data.json",false);
		xhr.onreadystatechange=function(){
			if(xhr.readyState===4&&xhr.status===200){
				data=JSON.parse(xhr.response);
			}
		}
		xhr.send(null);
		
		//渲染页面
		let str="",strCircle="";
		data.forEach((item,index)=>{
			let {pic}=item;
			str+=`<div class="slide"><img src="${pic}" alt=""></div>`;
			strCircle+=`<span class="${index==0?'active':''}" index="${index}"></span>`;
		})
		str+=`<div class="slide"><img src="${data[0].pic}" alt=""></div>`;
		
		wrapper.innerHTML=str;
		pagination.innerHTML=strCircle;
		spanlist=document.querySelectorAll(".pagination span");
		count=data.length+1;
	}
	render();
	//自动轮播
	function autoplay(){
		step++;
		if(step>count-1){
			wrapper.style.transitionDuration="0s";
			wrapper.style.left="0px";
			wrapper.offsetHeight;
			step=1;
		}
		wrapper.style.transitionDuration="0.3s";
		wrapper.style.left=`-${w*step}px`;
		criclefocus();
	}
	timer=setInterval(autoplay,1000);
	
	//小圆点获取焦点
	function criclefocus(){
		//step: 0,1,2,3
		let temp=step;
		if(step==count-1){
			temp=0;
		}
		spanlist.forEach((item,index)=>{//0,1,2
			if(index==temp){
				item.className="active";
			}else{
				item.className="";
			}
		})
	}
	//点击---》事件委托
	bannerBox.onclick=function(e){
		let tar=e.target;
		if(tar.tagName=="SPAN"){//小圆点
		    let index=+tar.getAttribute("index");
			if(index==step||(index==0&&step==count-1)){return;}//index:0   step:3
			step=index;//step:0
			wrapper.style.left=`-${step*w}px`;//滑到第-幅
			criclefocus();
		}
		if(tar.tagName=="DIV"&&tar.className.includes("navigation")){//左右按钮
			if(tar.className.includes("prev")){//左
				//console.log("左");
				step--;
				if(step<0){
					wrapper.style.transitionDuration="0s";
					wrapper.style.left=`-${(count-1)*w}px`;
					wrapper.offsetHeight;
					step=count-2;
				}
				wrapper.style.transitionDuration="0.3s";
				wrapper.style.left=`-${step*w}px`;
				criclefocus();
			}else{//右
			    //console.log("右")
				autoplay();
			}
		}
	}
	
	//进入轮播图，停止
	bannerBox.onmouseenter=function(){
		clearInterval(timer);
		timer=null;
	}
	//离开轮播图，开始
	bannerBox.onmouseleave=function(){
		timer=setInterval(autoplay,1000);
	}
})()