(function(){
	let bannerBox=document.querySelector("#bannerBox")
	let wrapper=document.querySelector(".wrapper");
	let spanlist=null;
	let pagination=document.querySelector(".pagination");
	let step=0;
	let w=bannerBox.offsetWidth;
	let count=4;
	let timer=null;
	
	function render(){
		let data=null;
		let xhr=new XMLHttpRequest;
		xhr.open("GET","data.json",false)
		xhr.onreadystatechange=function(){
			if(xhr.readyState===4&&xhr.status===200){
				data=JSON.parse(xhr.response);
			}
		}
		xhr.send(null);
		console.log(data);
		
		let stra="",strb="";
		
		data.forEach((item,index)=>{
			stra+=`<div class="slide"><img src="${item.pic}" alt=""></div>`;
			strb+=`<span class="${index==0?'active':''}" index="0"></span>`;
		})
		stra+=`<div class="slide"><img src="${data[0].pic}" alt=""></div>`;
		
		wrapper.innerHTML=stra;
		pagination.innerHTML=strb;
		count=data.length+1;
		spanlist=document.querySelectorAll(".pagination span");
	}
	render();
	
	//自动轮播
	function autoplay(){
		step++;
		if(step>count-1){
			wrapper.style.transitionDuration="0s";
			wrapper.style.left=`0px`;
			wrapper.offsetHeight;
			step=1;
		}
		wrapper.style.transitionDuration="0.3s";
		wrapper.style.left=`-${step*w}px`;
		criclefocus();
	}
	
	timer=setInterval(autoplay,1000);
	
	//小圆点获取焦点
	function criclefocus(){
		let temp=step;
		if(step==count-1){//千万别直接改 step
			temp=0
		}
		spanlist.forEach((item,index)=>{
			//step  0 1 2 3
			//index 0 1 2
			//temp  0 1 2
			if(index==temp){
				item.className="active";
			}else{
				item.className="";
			}
		})
	}
	
	// 小圆点，左右按钮---》事件委托
	bannerBox.onclick=function(e){
		if(e.target.tagName=="SPAN"){//点击小圆点
			let index=+e.target.getAttribute("index");
			if(step==index||(index==0&&step==count-1)){return;}
			step=index;
			wrapper.style.left=`-${step*w}px`;
			criclefocus();
		}
		
		if(e.target.tagName=="DIV"&&e.target.className.includes("navigation")){
			if(e.target.className.includes("prev")){//左
				step--;
				if(step<0){
					wrapper.style.transitionDuration="0s";
					wrapper.style.left=`-${(count-1)*w}px`;
					wrapper.offsetHeight;
					step=count-2;
				}
				wrapper.style.transitionDuration="0.3s";
				wrapper.style.left=`-${step*w}px`;
				criclefocus();
			}else{//右
				//console.log("右");
				autoplay();
			}
		}
	}
	
	//移入轮播图，停止
	bannerBox.onmouseenter=function(){
		clearInterval(timer);
		timer=null;
	}
	//移出轮播图，开始
	bannerBox.onmouseleave=function(){
		timer=setInterval(autoplay,1000);
	}
})()