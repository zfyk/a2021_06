$(function(){
    let url="login.html";
    //判断夜间模式
    let query=location.search;
    if(query){
       $(".setHeader").addClass("dark");
       $("#neight").prop("checked",true);
       url+="?id=dark";
    }else{
        $(".setHeader").removeClass("dark");
        $("#neight").prop("checked",false);
        url="login.html";
    }

    //切换夜间模式
    $("#neight").change(function(){
        if($(this).prop("checked")){
            console.log("夜间模式");
            $(".setHeader").addClass("dark");
            url+="?id=dark";
        }else{
            console.log("正常模式");
            $(".setHeader").removeClass("dark");
            url="login.html";
        }
    })

    //返回login
    $("#back").click(function(){
        location.href=url;
     })
 

})