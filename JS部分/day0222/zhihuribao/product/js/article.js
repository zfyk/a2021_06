$(function(){
    let url='http://localhost:8888';
    let query=location.search;
    //获取数据
    $.get(url+'/api/article' + query,function(data){
       render(data);
    })
    //渲染页面
    function render(data){
      //console.log(data);
      let {author,img,title,content}=data;
      $("header img").attr("src",img);
      $(".title").html(title);
      $(".author").html(author);
      //console.log(content);
      let str=""
      content.forEach((item)=>{
        if(item.type=="text"){
            str+=`<p>${item.text}</P>`;
        }else{
            str+=`<p class="imgBox">
                <img src="${item.img}">
                <span class="des">${item.des}</span>
            </p>`
        }
      })
      $(".topcon").html(str);
    }
    //返回首页
    $(".icon-fanhui").click(function(){
        location.href="../index.html";
    })
    //遮罩层显示
    $("footer i:not(.icon-fanhui)").click(function(){
        $(".mask").show();
    })
    //遮罩隐藏
    $(".mask").click(function(){
        $(".mask").hide();
    })
})