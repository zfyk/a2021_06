$(function(){
    //返回首页
    $(".icon-fanhui").click(function(){
        location.href="../index.html"
    })
    //获取数据
    let search=location.search;//"?id=9736137"
    function getDate(){
        $.get("http://localhost:8888/api/article"+search,(data)=>{
            console.log(data);
            render(data);
        })
    }
    getDate();
    //渲染
    function render(data){
        let {img,title,author,content}=data;
        $(".con_top img").attr("src",img);
        $(".con_top h1").html(title);
        $(".title span").html(author);
        let str="";
        content.forEach(item=>{
            if(item.type=="text"){
                 str+=`<p class="text">${item.text}</p>`
            }else{
                 str+=`<div class="textimg">
                            <img src="${item.img}" alt="">
                            <span>${item.des}</span>
                   </div>`
            }
        })
        console.log(str);
        $(".mian_con").html(str);
        $(".mark").click(function(){
            $(".mark").hide();
        })
        $(".footer i:not(.icon-fanhui)").click(function(){
            $(".mark").show();
        })
    }
})