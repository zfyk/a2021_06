$(function(){
    //补零
    function addZero(n){
      return n>9?n:"0"+n;
    }
    //获取日期
    function getDate(){
       let date=new Date();
       let day=date.getDate();
       let month=date.getMonth();
       let montharr= ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月', '十二月'];
       $(".date h2").html(addZero(day));
       $(".date span").html(montharr[month]);
       let hour=date.getHours();
       if(hour>12){
           $(".h_left h1").html("知乎日报");
       }else{
           $(".h_left h1").html("早上好");
       }
    }
    getDate()
    //轮播图
    function getDateLun(){
       $.get("http://localhost:8888/api/swiper").then(data=>{
           //console.log(data);
           renderLun(data);
       })
    }
    getDateLun()
   
    function renderLun(data){
       let str="";
       data.forEach((item)=>{
           let {author,id,img,title}=item;
           str+=`<div class="swiper-slide" data-id="${id}">
                <img src="${img}" alt="">
                <div class="swiper_text">
                    <h2 class="title">${title}</h2>
                    <span class="author">${author}</span>
                </div>
            </div>`;
       })
       $(".swiper-wrapper").html(str);
       activelun();
    }

    function activelun(){
        var swiper = new Swiper(".mySwiper", {
            pagination: {
              el: ".swiper-pagination",
            },
            loop:true,
            autoplay: {
                delay: 1000,
                stopOnLastSlide: false,
                disableOnInteraction: true,
            },
          });
    }
    //列表渲染
    let i=-1,n=-1,date = null;;
    function dealDate() {
        // time是i天前的毫秒数
        let time = new Date().getTime() - 1000 * 60 * 60 * 24 * i;
        date = {
            year: new Date(time).getFullYear(),
            month: new Date(time).getMonth()+1,
            day: new Date(time).getDate(),
        };
        i++;
    }
    function getlistDate(){
        n++;
        let datea=new Date();
        let date=new Date(datea*1-(n*24*60*60*1000))
        let year=date.getFullYear();
        let day=date.getDate();
        let month=date.getMonth()+1;
        let time=`${year}-${addZero(month)}-${addZero(day)}`;
        console.log(time);
        $.get(`http://localhost:8888/api/articleList?date=${time}`).then(data=>{
            renderlist(data);
        })
    }
    getlistDate()
    let liststr="";
    function renderlist(data){
        dealDate();
        if(i>0){
          liststr+=`<p class="line">${addZero(date.month)}-${addZero(date.day)}<span></span></p>`;
        }
       data.forEach(item=>{
        let {author,title,img,id,des}=item;
        liststr+=` <div class="item" data-id="${id}">
                    <div class="item_left">
                        <h3>${title}</h3>
                        <span>${author}·${des}分钟阅读</span>
                    </div>
                    <img src="${img}" alt="">
                </div>`
       })
       $(".list").html(liststr);
       loadmore()
    }

    function loadmore(){
        let ob=new IntersectionObserver(changes=>{
            if(changes[0].isIntersecting){
                if(i>100){
                    ob.unobserve($("#loadmore")[0]);
                    $("#loadmore").html("全部加载完毕");
                    return;
                }
                getlistDate();
            }
        })
        ob.observe($("#loadmore")[0])
    }

    $(".list").delegate(".item","click",function(){
        let id=$(this).attr("data-id");
        location.href="pages/article.html?id="+id;
    })
    
})