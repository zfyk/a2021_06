console.log(Number(""));
console.log(Number("   "));
console.log(Number("123"));
console.log(Number("1a23"));

console.log(Number(true));
console.log(Number(false));

console.log(Number(undefined));

console.log(Number(null));

console.log(Number([]));
console.log(Number([1, 2, 3]));
console.log(Number([1]));
console.log(Number({}));
console.log(Number({ name: "lily" }));


console.log(parseInt(123));
console.log(parseInt("a123"));
console.log(parseInt("1a123"));
console.log(parseInt("10a23"));
console.log(parseInt("100px"));
console.log(parseInt(12.3));
console.log(parseInt("0xa"));

console.log(parseInt(null));
console.log(parseInt(true));

console.log(parseFloat(123));
console.log(parseFloat(12.3));
console.log(parseFloat("12.3.3"));
console.log(parseFloat("a12.1"));

