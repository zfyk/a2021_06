/* 
从输入URL地址到看到页面，中间经历的步骤
  第一步：URL解析
    https://www.baidu.com:443/index.html?from=wx&type=1#video
    + 传输协议：http、https、ftp
      + http 超文本传输协议「除了传输文本内容外，还可以传输富文本内容(图片、音视频...)」，最常用的
      + https http+SSL(加密证书)，比HTTP更安全的传输协议，一般涉及支付类的网站都使用https
      + ftp 文件上传和下载传输协议，一般用于把本地开发的代码传到服务器
      + webscoket  地址是这样的"ws://"
      + ...
    + 域名：给服务器外网IP取了一个好记忆的名字
      + 顶级域名 baidu.com 「需要购买的」
      + 一级域名 www.baidu.com
      + 二级域名 image.baidu.com
      + 三级域名 mx.image.baidu.com
      + ...
    + 端口号：用来区分同一台服务器上的不同服务(项目)的
      + 范围：0~65535
      + 默认端口号(我们自己没有写，浏览器自动帮我们加上的)
        + http -> 80
        + https -> 443
        + ftp -> 21
    + 请求资源的路径名称
      + 默认名称，例如：/default.html  /index.html  ... 可以自己发布的时候配置
    + 基于URL地址实现问号传参 ?xxx=xxx&xxx=xxx
      + 必须会:queryURLParams
      + 作用一:把信息传递给服务器
      + 作用二:客户端两个页面间跳转,可以基于问号传参把A页面中的某些信息传递给B页面「例如：列表->详情」
    + 哈希值(HASH值) #xxx
      + 作用一:锚点定位
      + 作用二:哈希路由,实现SPA单页面应用开发
    --------
    信息编码：如果一个URL地址中出现了“中文、特殊符号、其它的URL地址”等，为了防止传输过程中的信息错乱，我们需要对这些信息进行编码处理！！
      + encodeURI / decodeURI：应用于对整个URL的处理，它只会把“中文、空格”等少量信息进行编码，对于URL地址和相关的特殊符号是无法进行编码的！！
      + encodeURIComponent / decodeURIComponent：应用于对URL问号传参的信息进行编码，可以编码很多内容
      + escape / unescape：不是所有后台语言都有这个API，所以一般只用于客户端之间传输

  第二步：缓存检查
    从服务器获取资源文件(例如:html/css/js/图片...)如果设置了缓存机制，信息会存储在：
      + Memory Cache：内存缓存「虚拟内存/内存条」
      + Disk Cache：硬盘缓存「物理内存/硬盘」
    页面刷新(F5)，首先检查Memory Cache中的缓存，有就从这获取，没有则检查Disk Cache...
    页面关闭重新打开，直接检查Disk Cache中的缓存
    强制刷新(CTRL+F5)，这种情况下会清除本地缓存，强制从服务器重新获取
    -----对“静态资源文件”的缓存处理
    如果有强缓存机制，且还在生效，直接从强缓存中获取；如果没有或者失效了，再看协商缓存机制，如果依然是没有或者失效了，则会从服务器重新获取！！
    @1 强缓存
      + 强缓存是由服务器设置：第一次向服务器发送请求(本地没有缓存,从服务器获取)，服务器在返回资源信息的同时，会基于“响应头”携带个字段“Expires(HTTP1.0老版本)或者Cache-Control(HTTP1.1新版本)”;客户端浏览器在获取到响应信息的时候，如果发现相关字段，就会把信息和缓存标识在本地存储一份！！
        + expires: Fri, 25 Mar 2022 03:18:47 GMT  设置过期时间
        + cache-control: max-age=2592000  设置有效期(2592000秒=30天)
      + 强缓存虽然很好「可以让第二次及以后访问网站的速度非常快(前提在缓存有效期内)」，但是也带来的相关的问题：如果服务器资源更新了、而本地缓存的信息还在有效期，这样导致客户端无法及时获取服务器最新的内容！
        解决办法：HTML页面“坚决不能”设置强缓存机制！！
        + 每一次访问都从服务器获取最新的HTML页面信息
        + 如果某个其它资源有更新，我们可以控制生成新的文件名，而HTML中导入的也是新文件「webpack自动处理」

    @2 协商缓存
      + 协商缓存和强缓存不同的地方在于，不论本地缓存是否生效，都要和服务器进行“协商”：检查服务器端相关的资源信息是否更新过！！
      + 协商缓存也是由服务器设置的；第一次向服务器发送请求(本地没缓存)，服务器返回资源信息的同时，会基于“响应头”返回Last-Modified(HTTP1.0版本)或者ETag(HTTP1.1版本)；客户端把资源信息和标识存储起来！！
        + last-modified: Fri, 21 Jan 2022 07:12:09 GMT  当前资源在服务器最后一次更新的时间
        + etag: "61ea5cc9-3fe"  服务器资源更新，会产生一个唯一的标识
      + 第二次再向服务器发送请求，不论本地是否有缓存(强缓存失效了)，都要向服务器发送请求，并且基于“请求头”把存储的Last-Modified值基于If-Modified-Since、把存储的Etag值基于If-None-Match发送给服务器；服务器拿到信息后和当前资源最新的更新时间/标识进行对比：
        + 传递的时间/标识，与服务器最新的更新时间/标识一致，说明资源没有更新过，此时服务器返回“304”状态码，客户端获取状态码后，直接从本地缓存中获取信息渲染即可！！
        + 如果不一致，说明服务器这段时间对资源进行了更新，则服务器返回“200”状态码，再把最新的信息和更新时间/标识，基于Last-Modified/etag返回给客户端，客户端把最新的信息都存储起来以及进行渲染！！
    -------
    强缓存 VS 协商缓存
    + 都是对静态资源文件的缓存，而且都是由服务器设置，客户端浏览器自动配合完成
    + 如果两个缓存机制都设置了(一般都这么干)，一定是强缓存失效后，才会走协商缓存的机制
    + 强缓存只要本地缓存还生效，就不会再和服务器通信了；但是协商缓存不论如何都会向服务器发请求，去校验资源是否更新；所以，HTML不能做强缓存，防止“服务器资源更新，但是客户端还是按照之前缓存渲染”的问题，但是HTML可以做协商缓存！！
    + 强存是基于响应头中的cache-control和expires实现的，协商缓存是基于Last-Modified(If-Modified-Since)和Etag(If-None-Match)实现的；
    + 强缓存不论是读取本地缓存还是从服务器获取信息，状态码都是200；
      协商缓存的状态码，可能有200，还可能出现304；
    + 不论强缓存还是协商缓存，都是产品性能优化非常重要的手段！！
    
    @3 Ajax数据请求方面的缓存「此时需要前端开发者，基于“本地存储方案”，手动去编码实现」
      本地存储方案：
        + cookie 兼容性很好
        + localStorage H5新增的API，只兼容IE9及以上
        + sessionStorage H5新增的API，只兼容IE9及以上
        + 全局变量(或者vuex/redux)，把信息存储在栈内存中(虚拟内存中)
        + IndexDB & WebSQL 利用浏览器自带的小型数据库进行存储（基本不用）
        + ...
      可以按照存储的生命周期进行明显区分：
        排除手动清除或者卸载浏览器
        + cookie：页面刷新在、页面关闭也在，但是要设置过期时间，过了有效期就会被清除
        + localStorage：“持久化”存储，没有过期时间
        + sessionStorage：“会话”存储，页面刷新信息不释放，但是页面关闭，属于会话结束，信息都会清空
        + 全局变量(或者vuex/redux)：页面不论是刷新还是关闭，之前存储的信息都会消失
      cookie VS localStorage
        + 存储有效期：cookie具备过期时间；而localStorage没有，他是持久化存储；
        + 兼容性：cookie基本都兼容；localStorage不兼容IE6~8；
        + 存储大小：同源下cookie只能存储4KB内容；localStorage可以存储5MB；
        + 稳定性：浏览器清除历史记录或者安全卫士清除垃圾等操作，可能会把cookie干掉；但是localStorage是不受这些操作影响的！！
        + 限制性：浏览器的隐私模式/无痕模式下，是禁用cookie的！但是不禁用localStorage；
        + 和服务器的关系：只要本地存储着cookie，不管服务器需不需要，每次向服务器发送请求，在请求头中都会把cookie带着(在CROS跨域中如果禁止携带资源凭证，是不会传输cookie信息的)；localStorage和服务器没关系，但是可以自己手动把localStorage中的部分信息传递给服务器！
      不论哪一种存储方案：
        + 都是有源和浏览器的限制
        + 都是明文存储，而且控制台可以直接看到，所以：重要安全信息尽可能不要存储，非要存储一定要加密！！

    


  第三步：DNS解析

  第四步：TCP三次握手

  第五步：实现客户端和服务器之前的信息传输「请求&响应」

  第六步：TCP四次挥手

  第七步：浏览器渲染从服务器获取的内容
*/

/* let url = `http://www.xxx.com/index.html?from=${encodeURIComponent('http://www.baidu.com/')}&lx=${encodeURIComponent('搜索')}`;
console.log(url); */

// 数据缓存的思路：每一次向服务器发送请求之前，首先校验本地是否存储过(而且有没有在有效期内)
//  + 如果本地存储信息生效：直接从本地获取即可
//  + 如果本地存储信息不生效：从服务器重新获取(ajax)、获取后存储到本地(设定存储的时间)

const ajax = url => {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', url);
        xhr.onreadystatechange = () => {
            let { readyState, status, responseText } = xhr;
            if (readyState === 4 && status === 200) {
                resolve(JSON.parse(responseText));
            }
        };
        xhr.send();
    });
};

/* (async function () {
    // @1 首先检测本地存储的信息是否生效(假设有效期：1H「3600000ms」)
    let data = localStorage.getItem('data');
    if (data) {
        let { time, value } = JSON.parse(data);
        if (+new Date() - time <= 3600000) {
            // 本地存储生效
            console.log('成功:', value);
            return;
        }
    }
    // @2 不生效再发请求 & 存储到本地
    let value = await query('./package.json');
    localStorage.setItem('data', JSON.stringify({
        time: +new Date(),
        value
    }));
    console.log('成功:', value);
})(); */


/* 
 * cacheData:封装一个能够缓存数据请求的方法
 *  @params
 *    query:函数,数据请求的方法「要求：执行方法可以从服务器获取数据、而且是基于Promise管理的、获取的数据格式是JSON对象」,此参数是必传项！
 *    name:字符串,必传,本地存储数据设置的名字
 *    time:数字,非必传(默认值3600000ms),存储数据的有效期(单位毫秒)
 *  @return 
 *    promise实例,实例的值是需要的数据(可能是从服务器获取的,也可能是从本地缓存中获取的)
 */
const cacheData = function cacheData(query, name, time) {
    // 格式校验
    if (typeof query !== "function") throw new TypeError("query is not a function");
    if (typeof name !== "string") throw new TypeError("name is not a string");
    time = +time;
    if (isNaN(time)) time = 60 * 60 * 1000;

    return new Promise(async resolve => {
        // 校验本地缓存是否生效
        let data = localStorage.getItem(name);
        if (data) {
            data = JSON.parse(data);
            if (+new Date() - data.time <= time) {
                // 缓存有效
                resolve(data.value);
                return;
            }
        }
        // 缓存失效:从服务器获取
        let value = await query();
        localStorage.setItem(name, JSON.stringify({
            time: +new Date(),
            value
        }));
        resolve(value);
    });
};

cacheData(
    () => ajax('./package.json'),
    'data'
).then(value => {
    // 数据绑定
    console.log('成功:', value);
});

cacheData(
    () => ajax('./data.json'),
    'list',
    5000
).then(value => {
    // 数据绑定
    console.log('成功:', value);
});