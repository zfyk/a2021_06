import axios from 'axios';
import { createApp } from 'vue';
import App from './App.vue';

axios.get('/api/asimov/subscriptions/recommended_collections')
    .then(response => response.data)
    .then(value => {
        console.log('成功:', value);
    });

const app = createApp(App);
app.mount('#app');