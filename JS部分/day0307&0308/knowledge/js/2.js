/* 
Axios基础知识 & 二次配置 
  http://www.axios-js.com/
  Axios库就是对ajax操作的封装，是基于Promise进行管理，实现客户端和服务器之间的通信本质：XMLHttpRequest

  @1 Axios基础语法
    基于axios发送请求，返回结果是一个promise实例：当请求成功，实例是fulfilled，值是从服务器获取的成功结果；当请求失败，实例是rejected，值是失败的原因！
    ----
    基于axios发送请求的办法
    + axios([config]) 核心
    + axios([url],[config]) 提取请求地址，其余写在config配置项中
    + axios.request([config]) 和axios一样，所有配置都写在config中
    + axios.get/head/delete/options([url],[config]) 提取请求方式、请求地址，其余写在config配置项中
    + axios.post/put/patch([url],[data],[config]) 提取请求方式、请求地址、请求主体，其余的写在config配置项中
    ----
    CONFIG配置项
      + params:字符串/对象  URL问号传参的配置，如果值是个对象，Axios内部会把对象中的键值对变为“xxx=xxx&xxx=xxx”的字符串，拼接到URL末尾传递给服务器!
      + Axios内部处理的时候，只有服务器返回的状态码是以2开始，才被处理为请求成功，其余都认为是请求失败；但是我们可以基于一个配置项进行调整：
        validateStatus: function (status) {
            return status >= 200 && status < 300; //default
        }
      + responseType 预设服务器返回数据的类型(把服务器返回的结果变为自己指定的类型) 
        + json 默认值,把服务器返回的JSON字符串变为对象
        + text
        + arraybuffer 
        + blob
        + document
        + stream
      + timeout 设置超时时间，默认0(不设置)
      + ”只针对于POST系列请求“，把用户传递的请求主体信息(一般写的是普通对象)，转换为服务器需要的格式，最后函数返回啥，就把啥放在请求主体中传递给服务器
        transformRequest:(data,headers)=>{
            //data:用户传递的请求主体信息
            //headers:请求头信息
            return data;
        }
      + headers 设置请求头信息
        headers:{
            get:{ ... },
            post:{ ... },
            ...
        }
      + baseURL 请求地址的公共前缀，真正的请求地址是 baseURL+url
      + withCredentials 设置跨域请求中，是否允许携带资源凭证
      + auth:{...}  设置这个配置，则发送请求的时候，在请求头中自动加一个Authorization字段
      + onUploadProgress/onDownloadProgress:ev=>{} 监听上传/下载的进度
      + ...
 
  @2 Axios中断Ajax请求
    + 创建一个遥控器「一个控制取消的对象」 const source = axios.CancelToken.source();
    + 把信号器安装在请求上「给请求加一个配置」 cancelToken:source.token
    + 只要按下取消按钮就可以停止请求  source.cancel('取消原因')
*/

// 检测是否为纯粹的对象
const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};

axios.post(
    '/user/login',
    // 请求主体：我们传递的是普通对象，但是Axios内部默认会把对象变为JSON字符串「Content-Type: application/json」，作为请求主体传递给服务器！
    { account: '18310612838', password: md5('1234567890') },
    // 配置项
    {
        baseURL: 'http://127.0.0.1:9999',
        transformRequest: data => {
            // 如果用户传递的DATA是普通对象，我们按照服务器要求，把其变为urlencoded字符串
            if (isPlainObject(data)) return Qs.stringify(data);
            return data;
        },
        headers: {
            // 请求主体的格式被确定后，我们需要有配套的请求头 Content-Type，声明其MIME类型；Axios也懂这个事情，所以在内部处理的时候，会自动识别请求主体的数据格式，自动配置Content-Type！
            // 'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
).then(response => response.data).then(value => {
    console.log('成功:', value);
}).catch(reason => {
    console.log('失败:', reason);
});





/* 
const source = axios.CancelToken.source();

axios.get('http://127.0.0.1:9999/user/list', {
    params: {
        departmentId: 0,
        search: ''
    },
    validateStatus: status => {
        // status:服务器返回的HTTP状态码
        return status >= 200 && status < 400;
    },
    timeout: 10000,
    cancelToken: source.token
}).then(response => {
    /!* 
    请求成功：response就是从服务器获取的结果（response是个对象）
      + config 存储发送请求时，我们设置的配置项
      + data 存储服务器返回的响应主体信息「默认把信息变为JSON对象(服务器一般返回JSON字符串)」
      + headers 存储服务器返回的响应头信息
      + request 存储原生AJAX创建的XHR实例对象信息
      + status/statusText 状态码及其描述 
    *!/
    return response.data;
}).then(value => {
    console.log('请求成功(响应主体):', value);
}).catch(reason => {
    /!* 
    请求失败
      CASE1：服务器有响应，只不过HTTP状态码没有通过validateStatus的校验，此时reason是个对象
        + config
        + isAxiosError:true
        + request
        + response 等同于成功的response对象
        + toJSON
        + message
      CASE2：请求超时，reason是个对象
        + code: "ECONNABORTED"
        + response: undefined
        + ...
      CASE3：请求被中断，reason是个对象
        + message 存储中断时传递的原因
      OTHER：剩下情况，我们都可以认为是“网络出问题了”
    *!/
    let status = reason?.response?.status,
        code = reason?.code;
    if (status) {
        switch (status) {
            case 403:
                alert('请求参数或者权限出问题');
                break;
            case 404:
                alert('请求地址错误');
                break;
            // ...
        }
    } else if (code === "ECONNABORTED") {
        alert("请求超时~");
    } else if (reason) {
        alert("请求被中断~");
    } else {
        alert("当前网络繁忙，请稍后再试~");
    }
});

// source.cancel('老子愿意'); 
*/