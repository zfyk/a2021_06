/*
 Axios的二次封装/配置:把多个axios请求公共的配置项进行提取 
   提取出的公共配置信息适用于大部分请求；部分请求如果配置信息和其不一样，只需要在发送请求的时候单独设置即可「单独设置的优先级是最高的」；
   + axios.defaults.xxx = xxx
   + axios.interceptors.request/response 拦截器
   不论如何封装和提取，都是根据当前项目的需求来设定的！！
 */
const isPlainObject = function isPlainObject(obj) {
    let proto, Ctor;
    if (!obj || Object.prototype.toString.call(obj) !== "[object Object]") return false;
    proto = Object.getPrototypeOf(obj);
    if (!proto) return true;
    Ctor = proto.hasOwnProperty('constructor') && proto.constructor;
    return typeof Ctor === "function" && Ctor === Object;
};

// 基础配置
axios.defaults.baseURL = "http://127.0.0.1:9999";
axios.defaults.timeout = 60000;
axios.defaults.withCredentials = true;
axios.defaults.transformRequest = data => {
    if (isPlainObject(data)) return Qs.stringify(data);
    return data;
};
axios.defaults.validateStatus = status => {
    return status >= 200 && status < 400;
};

// 拦截器
axios.interceptors.request.use(config => {
    // 把本次存储的Token传递给服务器
    let token = localStorage.getItem('token');
    if (token) {
        config.headers.authorzation = token;
    }
    return config;
});

axios.interceptors.response.use(response => {
    // 如果请求成功:我们返回响应主体信息
    return response.data;
}, reason => {
    // 如果请求失败:我们做统一的提示处理
    let status = reason?.response?.status,
        code = reason?.code;
    if (status) {
        switch (status) {
            case 400:
            case 401:
            case 403:
                alert('请求参数或者权限出问题');
                break;
            case 404:
                alert('请求地址错误');
                break;
            case 500:
                alert('服务器出现未知错误');
                break;
        }
    } else if (code === "ECONNABORTED") {
        alert("请求超时~");
    } else if (reason) {
        alert("请求被中断~");
    } else {
        alert("当前网络繁忙，请稍后再试~");
    }
    return Promise.reject(reason);
});