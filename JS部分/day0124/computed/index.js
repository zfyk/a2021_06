let product=(function(){
	let data=null;
	let boxtop=document.getElementsByClassName("boxtop")[0];
	let boxbtm=document.getElementsByClassName("boxbtm")[0];
	//1.获取数据---》json
	function getData(){
		let xhr=new XMLHttpRequest();
		xhr.open("GET","data.json",false);
		xhr.onreadystatechange=function(){
			if(xhr.readyState===4&&xhr.status===200){
				data=JSON.parse(xhr.response);
				console.log(data);
			}
		}
		xhr.send(null);
	}
	//循环渲染
	function render(){
		//渲染上半部分
		let str="";
		let allcount=0,allprice=0,heighprice=[0];
		data.forEach(item=>{
			let {id,count,price}=item;
			str+=`<div class="row" id="${id}">
					<img src="img/add.png" alt="" class="add">
					<span>${count}</span>
					<img src="img/sub.png" alt="" class="sub">
					<p>单价：${price}元 小计：${(count*price).toFixed(2)}元</p>
				</div> `
			if(count>0){
				allcount+=count;
				allprice+=count*price;
				heighprice.push(price);
			}
		})
		boxtop.innerHTML=str;
		
		//渲染下半部分
		let str2=`<p>商品合计：<span>${allcount}</span> 件</p>
				<p>共花费了：<span>${allprice.toFixed(2)}</span> 元</p>
				<p>其中最贵的商品单价是：<span>${Math.max(...heighprice)}</span> 元</p>`;
		boxbtm.innerHTML=str2
	}
	//功能部分
	function handle(){
		//事件委托：把事件绑定到祖先上
		boxtop.onclick=function(e){
			// console.log(e.target);
			// console.log(e.target.tagName);
			if(e.target.tagName==="IMG"){
				//1.找到当前点击按钮是 哪个id(img的父元素身上)
				let aid=+e.target.parentNode.id;
				//2. 通过aid找数据，对数据修改
				let newdata=data.find(item=>{
					return item.id===aid;
				})
				//没找到，结束
				if(!newdata){
					return;
				}
				//找到，修改count
				if(e.target.className==="add"){
					newdata.count++;
				}else{
					newdata.count--;
					//count小于等于0----》0
					if(newdata.count<=0){
						newdata.count=0;
					}
				}
			}
			render();
		}
	}
	return {
		init(){
			getData();
			render();
			handle();
		}
	}
})()
product.init();