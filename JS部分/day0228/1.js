/*
  创建promise实例的办法：
    @1 new Promise((resolve,reject)=>{})
       + 首先看executor函数执行是否报错，如果报错了，返回失败状态的实例(值是报错原因)，如果没有报错
       + 再看是执行的resolve还是reject，resolve执行创建成功的实例，反之reject执行创建失败的实例(值是函数传递的实参)
    @2 执行THEN/CATCH/FINALLY会返回一个全新promise实例(@P)，且传递onfulfilled/onrejected函数
       不论是传递的onfulfilled还是onrejected执行
       + 如果执行报错，则新实例@P状态是失败(值是报错原因)
       + 如果方法执行返回另外一个promise实例，则这个实例的状态和值决定了@P的状态和值
       + 剩下的情况@P都是成功的(值是函数的返回值)
    @3 Promise.resolve/reject 基于这两个函数执行可以快速创建状态是成功/失败的实例(值是传递的内容)
    @4 Promise.all/any/race 执行这三个方法也会返回promise实例(@P)
       包含零到多个的promise实例集合 -> “promises”「如果某一项不是promise实例，则默认转换为：状态为成功，值是本身的实例」
       + all promises中的所有项都是成功，@P才是成功的(值是按照原定顺序依次存储的每一项成功结果)，只要有一项是失败的，则@P就是失败(值是失败原因)，后面还没处理的也不处理了！！
       + any promises中有一项是成功的，则@P就是成功的...「兼容性不好」
       + race 以promises中最先处理完成的那一项为主

  在分析出实例的状态和值后
     @1 我们可以基于.then和.catch分别管理成功和失败后做啥事 「THEN链」
       + 真实项目中，THEN中只传递onfulfilled、CATCH中只传递onrejected(一般放在最末尾)
     @2 穿透/顺延机制：如果onfulfilled或者onrejected没有传递，则顺延至下一个THEN中，相同状态要处理的方法(原理:默认设置一个函数,能够顺延相关的状态)
 */
const query = url => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url,
            success(value) {
                resolve(value);
            },
            error(reason) {
                reject(reason);
            }
        });
    });
};

//===============
/* 
async & await ：async是对函数的修饰  await是在函数中使用 「ES8：promise的语法“糖”」
  基于async修饰函数，函数默认的返回值会变为一个promise实例
    + 如过函数执行报错，则返回状态为失败、值是报错原因的实例
    + 如果本身返回就是一个新的promise实例，则以自己返回的为主
    + 剩下情况则返回状态为成功、值是RETURN返回的值的实例
  但是真实项目中，我们使用async修饰函数，主要是为了在函数中应用await
  ---
  let value = await [promise];
  ...
  + await后面会跟着一个promise实例{如果跟着的不是实例，则默认转换为实例(状态成功、值是本身)}
  + “等待”promise实例状态为成功
    + promise实例状态如果还是pending或者rejected，则当前上下文中，await下面的代码不会执行，也不会给value赋值！
    + 只有状态为成功，才会把成功的结果赋值给value，下面代码才会执行！！
  + await的异常捕获：当监听的实例状态是失败，因为我们啥都没有处理，所以控制台会抛出红色异常(不影响其他代码执行，但是不好看)，此时我们最好能够像CATCH一样，做异常捕获处理！！
    + try/catch
  ---
  基于async修饰的函数，如果函数体中遇到await
    + 在await没有处理完成之前，默认返回状态是pending、值是undefined的实例
    + 等待await处理结果
      + await后面实例是失败的，则函数返回的实例也是失败的，值是失败原因
      + await后面实例是成功的，则await下面代码继续执行，而函数返回实例的状态也要进一步判断...如果整个过程中没有出现报错和失败的实例，则最后函数返回的实例状态是fulfilled、值是函数RETURN的值！！
    --> async修饰函数的返回值，是要依赖函数中await的处理！！
 */

/* 
// 真实项目中，我们一般都是基于async/await实现Ajax的串行操作！！
(async () => {
    try {
        let value = await query('./package.json');
        console.log('第一个请求成功:', value);

        value = await query('./data.json');
        console.log('第二个请求成功:', value);
    } catch (_) { }
})(); 
*/

/* const fn = async function fn() {
    let value = await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(100);
        }, 2000);
    });
    console.log('函数内部 OK', value); //OK 100
    return value / 10;
};
fn().then(value => {
    console.log(value); //10
});
*/

/* const fn = async function fn() {
    try {
        let value = await Promise.reject(10);
        console.log('OK', value);
    } catch (err) {
        // err存储TRY中代码执行报错的原因「这个参数必须有」
    }
};
console.log(fn()); */

//===============
/* // 如果promise实例的状态是失败的，而我们没有基于CATCH/THEN去设置onrejected，则控制台会“报红”,不影响其他代码执行(Uncaught (in promise));所以为了避免这个问题，我们一般都在末尾设置CATCH，来处理失败实例的情况！
query('./package.json')
    .then(value => {
        console.log('第一个请求成功:', value);
        return query('./data.json');
    })
    .then(value => {
        console.log('第二个请求成功:', value);
    })
    .catch(reason => { }); */