/* setTimeout(() => {
    console.log(1);
}, 20);
console.log(2);
setTimeout(() => {
    console.log(3);
}, 10);
console.log(4);
for (let i = 0; i < 90000000; i++) {
    // do soming 79ms 左右
}
console.log(5);
setTimeout(() => {
    console.log(6);
}, 8);
console.log(7);
setTimeout(() => {
    console.log(8);
}, 15);
console.log(9); */


/* 
 promise.then(onfulfilled,onrejected)
   执行THEN方法的时候，promise实例的状态有两种情况：
   + 已知其状态是成功或者失败，但是也并没有立即把onfulfilled/onrejected执行，而是创建一个“异步的微任务”「先放入到WebAPI中监听，发现是可以执行的，再放如到EventQueue中排队等待」,等待同步代码都执行完，在去执行这个异步微任务！！
   + 实例的状态还是pending，则会把onfulfilled/onrejected先存储起来，当后期基于resolve/reject等方法修改实例状态之后，也不会立即通知onfulfilled/onrejected执行，而是创建“异步的微任务”，把当前上下文中，其余同步要做的事情做完，再去通知指定的方法执行！！

 await promise;
   + 当前上下文中，遇到await，会把其“下面”的代码设置为“异步微任务”，放入WebAPI中进行监听；再看await后面的promise实例状态是成功还是失败，如果是成功则把刚才的异步微任务放入EventQueue中排队等着；只有其余的同步代码都执行完，才会执行这个异步微任务！！
 */
let body = document.body;
body.addEventListener('click', function () {
    Promise.resolve()
        .then(() => {
            console.log(1);
        });
    console.log(2);
});
body.addEventListener('click', function () {
    Promise.resolve()
        .then(() => {
            console.log(3);
        });
    console.log(4);
});




/* async function async1() {
    console.log('async1 start');
    await async2();
    console.log('async1 end');
}
async function async2() {
    console.log('async2');
}
console.log('script start');
setTimeout(function () {
    console.log('setTimeout');
}, 0);
async1();
new Promise(function (resolve) {
    console.log('promise1');
    resolve();
}).then(function () {
    console.log('promise2');
});
console.log('script end'); */


/* console.log(1);
(async () => {
    console.log(2);
    let value = await Promise.resolve('OK'); 
    console.log('成功:', value);
    console.log(3);
})();
console.log(4); */

/* console.log(1);
let p1 = Promise.resolve(10);
let p2 = p1.then(value => {
    console.log('成功1:', value);
    return value * 10;
});
p2.then(value => {
    console.log('成功2:', value);
});
console.log(2); */

/* console.log(1);
let p1 = new Promise((resolve) => {
    console.log(2);
    setTimeout(() => {
        resolve('OK'); //立即修改状态，“异步”通知之前存储的方法执行
        console.log(3);
    }, 2000);
});
console.log(4);
p1.then(value => {
    console.log('成功:', value);
});
console.log(5); */

/* console.log(1);
let p1 = new Promise((resolve) => {
    console.log(2);
    resolve('OK'); //立即修改实例状态为fulfilled
    console.log(3);
});
console.log(4);
p1.then(value => {
    console.log('成功:', value);
});
console.log(5); */