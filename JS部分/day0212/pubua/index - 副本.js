let product=(function(){
	let cloumns=Array.from(document.querySelectorAll(".cloumn"));
	let allimg;
	let html=document.documentElement||document.body;
	let data=null;
	//节流函数
	const throttle = function throttle(func, wait) {
	    if (typeof func !== "function") throw new TypeError('func must be an function');
	    if (typeof wait !== "number") wait = 300;
	    let timer,
	        previous = 0;
	    return function proxy(...params) {
	        let now = +new Date(),
	            remaining = wait - (now - previous),
	            self = this,
	            result;
	        if (remaining <= 0) {
	            if (timer) {
	                clearTimeout(timer);
	                timer = null;
	            }
	            result = func.call(self, ...params);
	            previous = now;
	        } else if (!timer) {
	            timer = setTimeout(() => {
	                if (timer) {
	                    clearTimeout(timer);
	                    timer = null;
	                }
	                result = func.call(self, ...params);
	                previous = +new Date();
	            }, remaining);
	        }
	        return result;
	    };
	};
	//1.获取数据
	function getData(){
		let xhr=new XMLHttpRequest();
		xhr.open("GET","data.json",false);
		xhr.onreadystatechange=function(){
			if(xhr.readyState===4&&xhr.status===200){
				data=JSON.parse(xhr.response);
			}
		}
		xhr.send();
	}
	//2.循环渲染
	function render(){
		//1.数据统一处理  map:返回一个与原来数组长度一样的新数组
		data=data.map(item=>{
			let {width,height}=item;
			item.width=230;
			item.height=230*height/width;
			return item;
		})
		
		let group;
		for(let i=0;i<data.length;i+=3){//i=0，3，6...
			group=data.slice(i,i+3);//i=0 (0,3) [0,1,2]
			//group [小 中 大]
			group.sort((a,b)=>{//[0]
				return a.height-b.height;
			})
			//cloumns [大 中 小]
			cloumns.sort((a,b)=>{//[0]
				return b.offsetHeight-a.offsetHeight;
			})
			
			group.forEach((item,index)=>{
				let {id,pic,width,height,title,link}=item;
				let con=`<div class="item">
					<a href="${link}">
						<div class="imgbox" style="height:${height}px;">
							<img src="" alt="" data-src="${pic}" style="height:${height}px;">
						</div>
						<p>${title}</p>
					</a>
				</div> `
				cloumns[index].innerHTML+=con;
			})
		}
		allimg=Array.from(document.querySelectorAll("img"));
	}
	function showimg(img){
		let datasrc=img.getAttribute("data-src");
		let newimg=document.createElement("img");
		newimg.src=datasrc;
		newimg.onload=function(){
			img.src=datasrc;
			//img.removeAttribute("data-src");
			newimg=null;
			img.flag=true;
		}
	}
	//功能部分
	function handle(){
		  console.log("111");
		allimg.forEach((img)=>{
			let ch=html.clientHeight;
			let imgbox=img.parentNode;//粉色盒子
			let rectBox=imgbox.getBoundingClientRect();
			let boxh=rectBox.bottom;
			if(ch>=boxh){//加载图片
				if(img.flag){
					return;
				}
				showimg(img)
			}
		})
	}
	//加载更多
	let count=0;
	function loadmore(){
		let allh=html.scrollHeight;
		let h=html.clientHeight+html.scrollTop;
		if(h+100>=allh){
			count++;
			if(count>2){
				return;
			}
			getData();
			render();
			handle();
		}
	}
	return {
		init(){
			getData();
			render();
			handle();
			window.onscroll=throttle(function(){
				handle();
				loadmore();
			},1000)
		}
	}
})()
product.init();