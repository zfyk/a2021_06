let product=(function(){
	let data=null;
	let cloumns=Array.from(document.querySelectorAll(".con .cloumn"));
	let imglist=null;
	let html=document.documentElement||document.body;
	let con=document.querySelector(".con");
	let more=document.querySelector("#more");
	//获取数据
	function getData(){
		let xhr=new XMLHttpRequest();
		xhr.open("GET","data.json",false);
		xhr.onreadystatechange=function(){
			if(xhr.readyState===4&&xhr.status===200){
				data=JSON.parse(xhr.responseText);
				//console.log(data);
			}
		}
		xhr.send(null);
	}
	//循环渲染
	function render(){
		//数据统一处理
		let newdata=data.map((item)=>{
			let {height,width}=item;
			item.width=230;
			item.height=230*height/width;
			return item;
		})
		
		let group;
		//i=0,3,6,9....
		for(let i=0;i<newdata.length;i+=3){
			//i=0 (0-3) [0,1,2] //i=3 (3,6) [3,4,5]//i=6 (6,9) [6,7,8]
			group=newdata.slice(i,i+3);
			group.sort((a,b)=>{
				return a.height-b.height;
			})
			
			cloumns.sort((a,b)=>{
				return b.offsetHeight-a.offsetHeight;
			})
			
			group.forEach((item,index)=>{
	            let {pic,height,title,link}=item;
				let div=document.createElement("div");
				div.className="item";
				div.innerHTML=`<a href="${link}">
						<div class="item_img" style="height:${height}px">
							<img src="" data-src="${pic}" style="height:${height}px">
						</div>
						<p>${title}</p>
					</a>`;
				cloumns[index].appendChild(div);
			})
			
		}
		imglist=document.querySelectorAll(".con img");
	}
	//监控器
	let ob=new IntersectionObserver(changes=>{
		//console.log(changes);
		changes.forEach((change)=>{
			let {isIntersecting,target}=change;
			if(isIntersecting){
				loadimg(target.querySelector('img'));
				ob.unobserve(target);
			}
		})
	},{
		threshold:[1]
	})
	//功能部分---图片懒加载
	function loadimg(img){
		let src=img.getAttribute("data-src");
		let new_img=new Image();
		new_img.src=src;
		new_img.onload=function(){
			img.src=src;
			img.removeAttribute("data-src");
			img.isload=true;
			new_img=null;
		}
	}
	function handle(){
		imglist.forEach(item=>{//item----》img
		    if(item.isLoad) return;
			let box=item.parentNode.parentNode.parentNode;//白盒
			ob.observe(box);
		})
	}
	let count=0;
	function loadmore(){
		let ob2=new IntersectionObserver(changes=>{
			//console.log(changes);
			if(changes[0].isIntersecting){
				count++;
				if(count>3){
					ob2.unobserve(more);
					return;
				}
				getData();
				render();
				handle();
			}
		})
		ob2.observe(more);
	}
	return {
		init(){
			getData();    
			render();
			handle();
			window.onscroll=function(){
				handle();
				loadmore();
			}
		}
	}
})()
product.init();