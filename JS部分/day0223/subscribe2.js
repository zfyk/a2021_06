(function () {
    class Sub {
        pond = []; //给实例设置的私有属性：this.pond=[]
        on(func) {
            let pond = this.pond;
            if (pond.includes(func)) return;
            pond.push(func);
        }
        off(func) {
            let pond = this.pond,
                i = 0,
                item;
            for (; i < pond.length; i++) {
                item = pond[i];
                if (item === func) {
                    pond[i] = null;
                    return;
                }
            }
        }
        fire(...params) {
            let pond = this.pond,
                i = 0,
                item;
            for (; i < pond.length; i++) {
                item = pond[i];
                if (typeof item === 'function') {
                    item(...params);
                    continue;
                }
                pond.splice(i, 1);
                i--;
            }
        }
    }

    /* 暴露API */
    if (typeof window !== 'undefined') window.Sub = Sub;
    if (typeof module === 'object' && typeof module.exports === 'object') module.exports = Sub;
})();
