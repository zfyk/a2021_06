let utils=(function(){
	const offset=function offset(ele){
		let top=ele.offsetTop;
		let left=ele.offsetLeft;
		let parent=ele.offsetParent;//div
		while(parent){
			top=top+parent.offsetTop+parent.clientTop;
			left=left+parent.offsetLeft+parent.clientLeft;
			parent=parent.offsetParent;//div.offsetParent--->h1
		}
		return{
			top,
			left
		}
	}
	const getCss=function getCss(ele,attr){
		let result;
		if("getComputedStyle" in window){//不是
			result=getComputedStyle(ele)[attr]
		}else{//IE
			result=ele.currentStyle[attr]
		}
		return result;
	}
	const setCss=function setCss(ele, obj) {
		for (let key in obj) {
			if (obj.hasOwnProperty(key)) {//只循环自己私有的属性
	            ele.style[key] = obj[key];
			}
		}
	}
	return {
		 offset,
		 getCss,
		 setCss
	}
})()