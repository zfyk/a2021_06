/*
 Promise:ES6+中新增的API，基于承诺者模式，实现对异步编程的管理，避免出现“回调地狱”；在当前前端开发中，遇到异步编程(尤其是Ajax请求)，“必然”会基于Promise(async/await)进行管理！！

 异步操作：
   + 异步微任务
     + promise.then/resolve&reject
     + async/await
     + queueMicrotask
     + requestAnimationFrame
     + IntersectionObserver
     + ...
   + 异步宏任务
     + 定时器
     + 事件绑定
     + ajax
     + ...

  -----------
  Promise是一个内置构造函数(类)
    @1 如何创建实例&实例的特征
      let p1 = new Promise([executor]);
      + Promise只能被“new”执行，不能作为普通函数执行
      + 传递的[executor]必须是一个函数
      + 实例对象具备两个内置的私有属性
        + [[PromiseState]] promise实例的状态：pending准备(默认)、fulfilled成功、rejected失败
        + [[PromiseResult]] promise实例的值：undefined、成功的结果、失败的原因

    @2 Promise.prototype为实例提供的公共属性方法
      + then
      + catch
      + finally
      + ...

    @3 把Promise作为普通对象,查看其自己静态私有属性方法
      + all
      + any
      + race
      + resolve：Promise.resolve('OK') 立即创建一个状态为成功，值是OK的实例
      + reject：Promise.reject('NO') 立即创建一个状态为失败，值是NO的实例
      + ...
      ---
      Promise.resolve('OK') 等价于 new Promise((resolve)=>{ resolve('OK') })
 */

/*
new Promise的时候会把executor函数立即执行(同步)，在这个函数中我们一般都是处理异步编程的代码
 + 并且传递给executor函数两个实参，我们用resolve/reject形参变量接收，传递进来的值是两个函数
   + resolve('OK') 把实例的状态修改为fulfilled成功态，实例的值变为'OK'
   + reject('NO') 把实例的状态修改为rejected失败态，实例的值变为'NO'
   + 一但把状态从pending改为fulfilled/rejected，再次修改状态就没有用了！
 + 如果executor函数在执行过程中出现了错误，则也会把实例的状态修改为rejected失败态，值是报错原因

------
let p2 = p1.then([onfulfilled],[onrejected])
  + [onfulfilled]和[onrejected]都是函数「如果我们不传，有默认的值，实现THEN的穿透效果?」
  + 如果p1实例的状态是pending，我们先把函数存储起来，等待实例状态确定是成功或者失败后，再去执行
  + 如果p1实例的状态是fulfilled，则把[onfulfilled]执行
  + 如果p1实例的状态是rejected，则把[onrejected]执行
  + 不仅执行对应的方法，还会把实例的值作为参数传递给方法

执行then方法，会返回一个全新的promise实例“p2”，如何确定它的状态和值呢？
  + 和p1本身没有必然的关系，和p1.then中传递的onfulfilled或者onrejected执行有关，不论是二者中的哪个方法执行：
  + 首先看函数执行是否报错，如果报错了，则p2是rejected失败态，值是报错原因；如果没有报错，则继续下一步
  + 其次再看方法的返回值，是否为一个新的promise实例
    + 如果返回的是新的实例，则新实例的成功和失败以及值，决定了p2的状态和值
    + 如果没有返回新的实例，则p2的状态是fulfilled，值是函数的返回结果
执行THEN返回新实例的目的是：这样可以继续.THEN下去，实现了Promise中的“THEN链机制”

THEN的穿透/顺延进制:我们不设置的onfulfilled或者onrejected，内部会帮我们默认设置对应的函数；设置的函数会“继承/顺延”上一个实例的状态和值，这样保证虽然我们没有写，也可以顺延值下一个then中相同状态执行的方法

-------
p.catch([onrejected])：p.then(null,[onrejected])
p.finally([onfinally])：不论上述对应的实例是成功还是失败，都会执行onfinally函数

-------
let p = Promise.all([promise1,promise2,...]):返回一个全新的promise实例“p”，传递的是一个包含多个promsie实例的集合；all方法会等待集合中的每一个实例状态都变为成功，这样p就是成功的，值是按照原始集合中顺序，存储每一个实例成功结果的集合；但是如果其中某一个实例状态失败，则p就是失败的，值是失败原因，后续还未处理的也不再处理了！！
+ any 是其中一个成功整体就是成功，只有所有的都失败，整体才是失败「ES12中加的，兼容的浏览器不多」
+ race 谁快听谁的，不论成功还是失败
*/

/* 
let p1 = new Promise(resolve => {
    setTimeout(() => {
        resolve(1);
    }, 2000);
});
let p2 = Promise.reject(2);
let p3 = 3; //如果集合中某一项不是promise实例，默认会变为状态是fulfilled，值是“本身”的实例  => Promise.resolve(3)

Promise.all([p1, p2, p3])
    .then(values => {
        console.log('成功', values);
    })
    .catch(reason => {
        console.log('失败', reason);
    }); 
*/



/*
// 真实项目中，THEN值传递onfulfilled，CATCH中只传递onrejected，而且写在最末尾；不论上面那一次处理，返回失败的实例，最后都会顺延至最后一个CATCH中进行处理！！
Promise.resolve(100)
    .then(value => {
        console.log('成功', value);
        return 10;
    })
    .then(value => {
        console.log('成功', value);
        console.log(a);
    })
    .catch(reason => {
        console.log('失败', reason);
    });
*/

/*
Promise.reject(0)
/!*p1=*!/.then(value => {
        console.log('成功', value);
        return 10;
    }/!*, reason=>{
        throw reason;
    }*!/)
/!*p2=*!/.then(null/!* value=>{
            return value;
        } *!/, reason => {
        console.log('失败', reason); //失败 0
        return -10;
    })
    .then(value => {
        console.log('成功', value); //成功 -10
    });
*/

/* Promise.resolve(10)
    .then(value => {
        console.log('成功', value); //成功 10
        return Promise.reject(0);
    }, reason => {
        console.log('失败', reason);
        return 100;
    })
    .then(value => {
        console.log('成功', value);
        return 200;
    }, reason => {
        console.log('失败', reason);
        return 1;
    })
    .then(value => {
        console.log('成功', value, a);
    }, reason => {
        console.log('失败', reason);
    })
    .then(value => {
        console.log('成功', value);
    }, reason => {
        console.log('失败', reason);
    }); */



/* let p1 = new Promise((resolve, reject) => {
    resolve('OK');
});
let p2 = p1.then(value => {
    console.log('成功:', value);
    return 100;
}, reason => {
    console.log('失败:', reason);
});
p2.then(value => {
    console.log('成功:', value);
}, reason => {
    console.log('失败:', reason);
});
*/

/* let p = new Promise(function executor(resolve, reject) {
    resolve('OK');
});
p.then(value => {
    console.log('成功:', value);
}, reason => {
    console.log('失败:', reason);
}); */

/* let p = new Promise(function executor(resolve, reject) {
    setTimeout(() => {
        resolve('OK');
    }, 2000);
});
p.then(value => {
    console.log('成功:', value);
}, reason => {
    console.log('失败:', reason);
}); */


//=====================
//  基于Promise封装Ajax异步请求的操作「真实项目中我们使用Axios库」
/* const ajax = function ajax(url) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest;
        xhr.open('GET', url);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                resolve(xhr.responseText);
            }
        };
        xhr.send();
    });
}; */

/*
// 需求一：Ajax串行，基于Promise可以有效解决回调地狱问题
ajax('./data1.json')
    .then(value => {
        console.log('第一个请求成功:', value);
        return ajax('./data2.json');
    })
    .then(value => {
        console.log('第二个请求成功:', value);
        return ajax('./data3.json');
    })
    .then(value => {
        console.log('第三个请求成功:', value);
    });
*/
/*
// 需求一：Ajax串行，基于Promise的语法糖async/await，用起来更爽
(async function () {
    let value = await ajax('./data1.json');
    console.log('第一个请求成功:', value);

    value = await ajax('./data2.json');
    console.log('第二个请求成功:', value);

    value = await ajax('./data3.json');
    console.log('第三个请求成功:', value);
})();
*/

/*
// 需求二：Ajax并行，多个请求都完成后，统一处理啥事情
Promise.all([
    ajax('./data1.json'),
    ajax('./data2.json'),
    ajax('./data3.json')
]).then(values => {
    // values:按照顺序依次存储三个请求的结果
});
*/


//=====================
/* 
// 真实项目中基于Ajax实现和服务器之间的数据通信，一般都是采用异步请求
//  + ajax并行：多个请求之间不存在相互依赖，可以同时发送，谁先请求回来先处理谁即可「往往可能会附加一个额外的需求：都请求成功后统一干点啥事」
//  + ajax串行：多个请求之间存在依赖(如:下一个请求的发送依赖上一个请求的结果)，这样只能等待上一个请求成功，才能发送下一个请求...
// 如果当前的Ajax请求是“基于回调函数的方式”管理的(JQ库:$.ajax)，在实现异步请求&串行的程序中，很容易出现“回调地狱”的问题(回调嵌套回调)，这样不仅代码看起来很恶心，而且不利于后期的维护！！
const ajax = function ajax(url, callback) {
    let xhr = new XMLHttpRequest;
    xhr.open('GET', url);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback && callback(xhr.responseText);
        }
    };
    xhr.send();
};
ajax('./data1.json', (value) => {
    console.log('第一个请求成功:', value);
    ajax('./data2.json', (value) => {
        console.log('第二个请求成功:', value);
        ajax('./data3.json', (value) => {
            console.log('第三个请求成功:', value);
        });
    });
});

let n = 0;
const complete = () => { 
    // 三个都成功了
};
ajax('./data1.json', (value) => {
    console.log('第1个请求成功:', value);
    n++;
    if (n >= 3) complete();
});
ajax('./data2.json', (value) => {
    console.log('第2个请求成功:', value);
    n++;
    if (n >= 3) complete();
});
ajax('./data3.json', (value) => {
    console.log('第3个请求成功:', value);
    n++;
    if (n >= 3) complete();
}); 
*/




