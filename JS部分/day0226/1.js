/* 
 curring:代指柯理化函数
   柯理化函数是一种思想：本质就是利用闭包的“保存”作用，在闭包中存储一些信息，这些信息供其下级上下文中使用；也可以说是把后面，下级上下文中需要使用的内容，基于闭包“预先”存储起来，我们把这种“预先存储”的思想称之为柯理化函数思想！！
 */
/* 
const curring = function curring() {
    // arr:用来存储每一次add执行传递进来的实参
    let arr = [];
    const add = (...params) => {
        // params:存储的是每一次传递进来的实参
        arr = arr.concat(params);
        return add;
    };
    add[Symbol.toPrimitive] = () => {
        // 最后“+res”进行输出的时候，会调用此方法：把每一次add传递进来的实参值进行求和
        return arr.reduce((res, item) => res + item);
    };
    return add;
};
let add = curring();
let res = add(1)(2)(3);
console.log(+res); //->6 

add = curring();
res = add(1, 2, 3)(4);
console.log(+res); //->10 

add = curring();
res = add(1)(2)(3)(4)(5);
console.log(+res); //->15 
*/


/* 
// i++/++i永远进行的都是数学运算
// +i 就是把值变为数字类型，和 Number(i) 是一样的
let i = '10';
+i;
// i++;
// console.log(i); //11
// i += 1;
// console.log(i); //'101'
// i = i + 1;
// console.log(i); //'101' 
*/

/* 
// 数组求和
//  + 循环
//  + eval(arr.join('+')) 不推荐使用:eval消耗性能、在代码压缩的时候可能导致不可控的代码错误...
//  + reduce/reduceRight 是ES5新增的方法：依次迭代数组每一项，把每一项进行相关的处理，可以实现结果的累计(可以获取上一次回调函数处理的结果)
let arr = [10, 20, 30, 40, 50];
/!* // 把数组第一项作为res初始值，从数组第二项开始迭代
let total = arr.reduce((res, item, index) => {
    // 第一次：res=10(数组第一项作为初始值)  item=20  index=1
    // 第二次：res=30(获取上一次回调函数处理的结果) item=30 index=2
    // 第三次：res=60 item=40 index=3
    // 第四次：res=100 item=50 index=4  返回值是150
    return res + item;
}); *!/
// 如果reduce设置初始值(res=0)，那么则从数组第一项开始迭代
let total = arr.reduce((res, item, index) => {
    return res + item;
}, 0);
console.log(total); 
*/

//=======================
/* 
 类(构造函数)：函数数据类型，大部分函数都具备prototype原型对象属性
 实例：对象数据类型，具备__proto__这个属性  “实例.__proto__===所属类.prototype”
 实例一般是被“new”出来的
 ----
 实例具备私有的属性方法「可以保证同一个类的不同实例间互不干扰」；实例可以按照原型链，访问到类原型对象上，为其提供的公共属性方法；构造函数本身可以看做一个普通对象，设置静态私有属性方法(和实例没关系)；

 N是原始值的数字，例如：10
   N.plus() 执行的时候，浏览器内部会对N进行“装箱处理” => 把其转换为对象 new Number(N)或Object(N)
 N是非标准特殊对象，例如：new Number(10)
   +N 把对象转换为原始值数字，这个过程叫做“拆箱”
 */
/* const checkNum = num => {
    num = +num;
    return isNaN(num) ? 0 : num;
};
Number.prototype.plus = function plus(num) {
    // num->10  this->Object(n)
    num = checkNum(num);
    return this + num;
};
Number.prototype.minus = function minus(num) {
    // num->5  this->Object(20)
    num = checkNum(num);
    return this - num;
};
let n = 10;
let m = n.plus(10).minus(5);
console.log(m); //=>15（10+10-5） */

//==================
/* function Dog(name) {
    this.name = name;
}
Dog.prototype.bark = function () { console.log('wangwang'); }
Dog.prototype.sayName = function () { console.log('my name is ' + this.name); } */

/* // 重写内置NEW
//   + Ctor:想创建对应实例的构造函数
//   + params:执行Ctor时候,为其传递的所有实参值
function _new(Ctor, ...params) {
    // 1.创建Ctor的实例对象「对象.__proto__===Ctor.prototype」
    let obj = Object.create(Ctor.prototype);

    // 2.把Ctor像普通一样执行，只不过需要把函数中的this指向实例对象
    let result = Ctor.call(obj, ...params);

    // 3.监测函数返回值
    if (result !== null && /^(object|function)$/i.test(typeof result)) return result;
    return obj;
} */

/* function _new(Ctor, ...params) {
    // 格式校验:Ctor必须是函数、具备prototype、不能是Symbol/BigInt
    if (typeof Ctor !== "function" || !Ctor.prototype || Ctor === Symbol || Ctor === BigInt) throw new TypeError('Ctor is not a constructor');
    let obj, result;
    obj = Object.create(Ctor.prototype);
    result = Ctor.call(obj, ...params);
    if (result !== null && /^(object|function)$/i.test(typeof result)) return result;
    return obj;
}
let sanmao = _new(Dog, '三⽑');
sanmao.bark(); //=>"wangwang" 
sanmao.sayName(); //=>"my name is 三⽑" 
console.log(sanmao instanceof Dog); //=>true */

//======================
// “鸭子类型”：有一种动物长的很像鸭子，但是本质不是鸭子，这样是不具备鸭子的特性的，但是我们想，既然和鸭子这么像，我如何能让他像鸭子一样生活？
// 例如：类数组想用数组原型上的方法
//  + 把类数组转换为数组  Array.from/循环处理
//  + 直接借用
//    + 基于call改变this处理，例如：[].forEach.call(obj类数组,(item,index)=>...)
//    + 把需要借用的方法赋值给类数组的私有属性即可

/* 
let obj = {
    0: 10,
    1: 20,
    2: 30,
    length: 3
};
Array.prototype.forEach.call(obj, item => {
    console.log(item);
});
// obj.forEach((item, index) => {
//     console.log(item, index);
// }); //Uncaught TypeError: obj.forEach is not a function 
*/

/* Array.prototype.push = function push(val) {
    // this->arr val->100
    this[this.length] = val;
    this.length++;
    return this.length;
}; */
let obj = {
    2: 3, //1
    3: 4, //2
    length: 2, //(3)4
    push: Array.prototype.push
};
obj.push(1);
// this->obj  val->1
// obj[obj.length] = 1;  -> obj[2]=1
// obj.length++;
obj.push(2);
// this->obj  val->2
// obj[obj.length] = 2; -> obj[3] = 2;
// obj.length++;
console.log(obj);