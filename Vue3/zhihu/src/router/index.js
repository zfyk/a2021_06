import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes'
import store from '../store'
import { Toast } from 'vant'

// 写一个全局提示的方法
const Loading = function Loading() {
    // 创建遮着层
    const markDiv = document.createElement('div'),
        body = document.body
    markDiv.innerText = '小主，奴家正在努力处理中...'
    markDiv.style.cssText = `
        display:flex;
        justify-content:center;
        align-items:center;
        position:fixed;
        top:0;
        left:0;
        z-index:99999;
        width:100%;
        height:100%;
        background:rgba(0,0,0,.8);
        font-size:.4rem;
        color:rgb(0, 148, 255);
    `
    body.appendChild(markDiv)
    // 禁用掉页面滚动
    const forbidden = ev => ev.preventDefault()
    body.addEventListener('scroll', forbidden)
    body.addEventListener('touchmove', forbidden)
    body.style.overflowY = 'hidden'
    body.style.height = '100vh'

    return function unLoading() {
        // 移除遮罩层
        body.removeChild(markDiv)
        body.style.overflowY = 'auto'
        body.style.height = 'auto'
        body.removeEventListener('scroll', forbidden)
        body.removeEventListener('touchmove', forbidden)
    }
}

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

// 导航守卫
let unLoading
router.beforeEach(async (to, from, next) => {
    let checkList = ['/person', '/store', '/update']
    if (checkList.includes(to.path)) {
        // 需要做登录校验
        let { info } = store.state
        if (!info) {
            unLoading = Loading()
            info = await store.dispatch('queryInfoAsync')
            if (!info) {
                // 确定是没登录
                Toast.fail('您还未登录，请先登录')
                next(`/login?from=${to.path}`)
                return
            }
        }
    }
    next()
})
router.afterEach(() => {
    if (unLoading) unLoading()
})

export default router