import Home from '../views/Home.vue'
const routes = [{
    path: '/',
    name: 'home',
    meta: {},
    component: Home
}, {
    path: '/detail/:id',
    name: 'detail',
    meta: {},
    component: () => import('../views/Detail.vue')
}, {
    path: '/login',
    name: 'login',
    meta: {},
    component: () => import('../views/Login.vue')
}, {
    path: '/person',
    name: 'person',
    meta: {},
    component: () => import('../views/Person.vue')
}, {
    path: '/store',
    name: 'store',
    meta: {},
    component: () => import('../views/Store.vue')
}, {
    path: '/update',
    name: 'update',
    meta: {},
    component: () => import('../views/Update.vue')
}, {
    path: '/:pathMatch(.*)*',
    redirect: '/'
}]
export default routes