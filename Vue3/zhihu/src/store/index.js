import { createStore, createLogger } from 'vuex'
import api from '../api'
const env = process.env.NODE_ENV
const store = createStore({
    state: {
        info: null,
        storeList: null
    },
    mutations: {
        queryInfo(state, payload) {
            state.info = payload
        },
        queryStoreList(state, payload) {
            state.storeList = payload
        }
    },
    actions: {
        async queryInfoAsync({ commit }) {
            let { code, data } = await api.userInfo()
            if (+code !== 0) return null
            commit('queryInfo', data)
            return data
        },
        async queryStoreListAsync({ commit }) {
            let { code, data } = await api.storeList()
            if (+code !== 0) return null
            commit('queryStoreList', data)
            return data
        },
    },
    plugins: env !== 'production' ? [createLogger()] : []
})
export default store