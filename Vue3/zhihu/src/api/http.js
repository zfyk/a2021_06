import axios from "axios";
import qs from 'qs';
import { isPlainObject, store } from '../assets/utils';
import { Notify } from 'vant';

const http = axios.create({
    baseURL: '',
    timeout: 60000,
    transformRequest: data => {
        if (isPlainObject(data)) return qs.stringify(data);
        return data;
    }
});
http.interceptors.request.use(config => {
    let token = store.get('tk');
    if (token) {
        config.headers['authorzation'] = token;
    }
    return config;
});
http.interceptors.response.use(response => {
    return response.data;
}, reason => {
    Notify({
        type: 'danger',
        message: '小主,当前网络繁忙,请稍后再试!'
    });
    return Promise.reject(reason);
});
export default http;