const { defineConfig } = require('@vue/cli-service')
const env = process.env.NODE_ENV
module.exports = defineConfig({
  publicPath: './',
  lintOnSave: env !== 'production',
  productionSourceMap: false,
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:7100',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
})